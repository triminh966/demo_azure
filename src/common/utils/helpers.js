import moment from 'moment';
import * as _ from 'lodash';
import {
  MaleIcon,
  FemaleIcon,
  Logo
} from 'modules/shared/images';

function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

export function getSuggestions(suggestions, value, propName) {
  let escapedValue = escapeRegexCharacters(value.trim());
  if (escapedValue === '') {
    return [];
  }
  let list = suggestions.filter((val) => {
    let arrName = [];
    let condition;
    if (propName) {
      arrName = val[propName].split(' ');
      condition = val[propName].toLowerCase().startsWith(escapedValue.toLowerCase());
    } else {
      arrName = val.name.split(' ');
      condition = val.name.toLowerCase().startsWith(escapedValue.toLowerCase());
    }
    for (let i = 0; i < arrName.length; i++) {
      if (arrName[i].toLowerCase().startsWith(escapedValue.toLowerCase()) ||
        condition) {
        return true;
      }
    }

    return false;
  });
  return list;
}
export function splitSpace(arrTitle) {
  let result;
  if (arrTitle) {
    result = arrTitle.replace(/ /g, '');
  }
  return result;
}

export function findMatchesItem(list, value, propName) {
  let result;
  if (value) {
    let escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
      return [];
    }

    for (let i = 0; i < list.length; i++) {
      let s = list[i];
      if (s[propName] === value) {
        result = s;
        break;
      }
    }
  }

  return result;
}


export function findMatchesHightlight(text, query) {
  let result = [];
  let queries = query.split(' ');
  for (let i = 0; i < queries.length; i++) {
    let q = queries[i];
    let c = q.length;
    let index = text.toLowerCase().indexOf(q.toLowerCase());
    if (index > -1) {
      result.push([index, index + c]);
    }
  }

  return result;
}

export function mobileAndTabletCheck() {
  let check = false;
  (function (a) {
    // eslint-disable-next-line max-len
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw-(n|u)|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do(c|p)o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(-|_)|g1 u|g560|gene|gf-5|g-mo|go(.w|od)|gr(ad|un)|haie|hcit|hd-(m|p|t)|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c(-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac( |-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c(-|0|1)|47|mc|nd|ri)|sgh-|shar|sie(-|m)|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel(i|m)|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test(a.substr(0, 4))) {
      check = true;
    }
  }(navigator.userAgent || navigator.vendor || window.opera));
  return check;
}


/**
 * Build query url based on passed parameters
 * @param {object} parameters Specify the params to build query url
 */
export function buildUrlQuery(parameters) {
  let queryUrl = '';
  let qs = '';

  /* eslint-disable guard-for-in */
  for (let key in parameters) {
    let value = parameters[key];

    if (value !== undefined) {
      qs += `${encodeURIComponent(key)}=${encodeURIComponent(value)}&`;
    }
  }
  /* eslint-enable */
  if (qs.length > 0) {
    qs = qs.substring(0, qs.length - 1); // chop off last "&"
    queryUrl = `${queryUrl}?${qs}`;
  }

  return queryUrl;
}

/**
 *
 * Exit fullscreen function
 *
 */
export function exitFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}

/**
 * Expand fullscreen
 */
export function expandFullscreen() {
  // If screen has already been fullscreen then return
  if (window.innerHeight === screen.height) {
    return;
  }
  // Otherwise request fullscreen
  let el = document.documentElement,
    rfs = el.requestFullscreen ||
    el.webkitRequestFullScreen ||
    el.mozRequestFullScreen ||
    el.msRequestFullscreen;

  rfs.call(el);
}

/**
 * Create uuid
 */
export function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return `${s4() + s4()}-${s4()}-${s4()}-${
    s4()}-${s4()}${s4()}${s4()}`;
}

export function getEnumDescription(object, value) {
  for (let prop in object) {
    if (object[prop] === value) {
      return prop;
    }
  }

  return '';
}

/**
 *
 * Delay input handler
 *
 */
export const delay = (function () {
  let timer = 0;
  return function (callback, ms) {
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
  };
}());

/**
 * Format member name as "Fist name concat with initial character of Last name"
 * Ex: John Cornor => John C
 * @param {string} firstName Specify the first name of member
 * @param {string} lastName Specify the last name of member
 */
export function formatMemberName(firstName, lastName) {
  if (!firstName) {
    return undefined;
  }

  // Trim name before concatination
  if (lastName) {
    lastName = `${lastName}`.trim();
  }
  return `${firstName.trim()} ${`${lastName || ''}`.charAt(0).toUpperCase()}.`;
}

/**
 * Render email to following format	Emi*****@xxx.com
 * @param {String} email The email need format
 */
export function renderEmail(email) {
  if (!email) {
    return undefined;
  }

  let arrs = email.split('@');
  let hash = arrs[0].substring(arrs[0].length - 6, arrs[0].length - 2);

  return email.replace(hash, '**');
}

/**
 * Return a javascript object containing the URL parameters
 * Read a page's GET URL variables and return them as an associative array.
 */
export function getUrlVars() {
  let vars = {};
  let hash;
  let hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

  for (let i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    vars[hash[0]] = hash[1];
  }

  return vars;
}

/**
 * Converts the given date with the format pattern
 * @param {String}  date The date to be converted
 * @param {String}  date The format pattern to be used
 * @param {boolean} ignoreConvert
 * @return {String} The formatted date
 */
export function formatDate(date, format, ignoreConvert = false) {
  if (date === null || date === '') {
    return '';
  }

  // Remove Z character from the date value to ignore convert to local time
  if (ignoreConvert && typeof date === 'string') {
    date = date.replace(new RegExp('Z', 'g'), '');
  }
  // TBD
  // return moment(date).format('LT');
  if (format) {
    return moment(date).format(format);
  }
  return moment(date).format('h:mm A');
}

export function updateLocalStorage(key, value, type = 'string') {
  if (type === 'array') {
    const list = JSON.parse(localStorage.getItem(key)) || [];
    if (!list.includes(value)) {
      list.push(value);
      localStorage.setItem(key, JSON.stringify(list));
    }
  } else {
    localStorage.setItem(key, value);
  }
}

export function renderGenericAvatar(gender) {
  if (gender === 'Male') {
    return MaleIcon;
  }

  if (gender === 'Female') {
    return FemaleIcon;
  }

  return Logo;
}

export function getDataInLocalStorage(key) {
  let data = localStorage.getItem(key);
  if (data && !_.isNull(data) && !_.isEqual(data, 'null')) {
    data = JSON.parse(localStorage.getItem(key));
  } else {
    data = [];
  }
  return data;
}
/**
 * Get start time duration in minutes
 * @param {String} classStartTime The date time value to be diff with current datetitme
 * @return {Number} The diff time in minutes
 */
export function getStartTimeDurationInMinutes(classStartTime) {
  const momentCurrentDateTime = moment();
  const momentStartDateTime = moment(classStartTime);

  return momentStartDateTime.diff(momentCurrentDateTime, 'minutes');
}

/**
 * Format phone number
 * @param {String} value
 */
export function formatPhoneNumber(value) {
  let phoneFormat = value ? value.replace(/[^0-9]/g, '') : undefined;
  phoneFormat = phoneFormat && phoneFormat.length < 10 ? undefined : phoneFormat;
  return phoneFormat;
}
export function checkSignedCI(member, studio) {
  const {memberStudioAgreements} = member;
  for (let i = 0; i < memberStudioAgreements.length; i++) {
    if (memberStudioAgreements[i]
      && memberStudioAgreements[i].studioAgreement.studio.studioUUId === studio.StudioUUId) {
      return true;
    }
  }
  return false;
}
export function calculateAge(birthday) {
  let ageDifMs = Date.now() - birthday.getTime();
  let ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}
export function saveState(state) {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
    return true;
  } catch (err) {
    return undefined;
  }
}
export function loadState() {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
}

export default {
  getUrlVars,
  formatMemberName,
  delay: delay,
  getEnumDescription,
  guid: guid,
  exitFullscreen,
  buildUrlQuery,
  mobileAndTabletCheck,
  findMatchesHightlight,
  findMatchesItem,
  getSuggestions,
  splitSpace,
  formatDate,
  renderEmail,
  updateLocalStorage,
  renderGenericAvatar,
  expandFullscreen,
  getDataInLocalStorage,
  getStartTimeDurationInMinutes,
  formatPhoneNumber,
  checkSignedCI,
  calculateAge,
  saveState,
  loadState
};

class AppConfig {
  _config;

  constructor() {
    this.load(process.env.APP_ENV);
  }

  /**
   * Load the environment configuration with specific environment name such as production
   */
  load(env) {
    this._config = require(`./env/${env}.json`);
  }

  /**
   * Get the current environment configuration object which is loaded in the load() function
   * @return {object}
   */
  getConfig() {
    return this._config;
  }

  /**
   * Get the root url of OTUM Franchise API
   * @return {String} The url of OTUM Franchise API
   */
  getOtumFranchiseApiRootUrl() {
    return this._config.otumFranchiseApiRootUrl;
  }
  getUnauthorizeEndpoint() {
    return this._config.unauthorizeEndpoint;
  }

  getAuthorizeEndpoint() {
    return this._config.authorizeEndpoint;
  }
  getEdenApiRootUrl() {
    return this._config.edenRootUrl;
  }
  /**
   * Get the cognito user pool id
   * @return {String} the cognito user pool id configured in the environment file
   */
  getCognitoUserPoolId() {
    return this._config.cognito.userPoolId;
  }

  /**
   * Get the cognito identity pool id
   * @return {String} the cognito identity pool id configured in the environment file
   */
  getCognitoIdentityPoolId() {
    return this._config.cognito.identityPoolId;
  }

  /**
     * Get the aws region
     * @return {String} the aws region configured in the environment file
     */
  getAwsRegion() {
    return this._config.awsRegion;
  }

  /**
   * Get the cognito user pool id
   * @return {String} the cognito user pool id configured in the environment file
   */
  getCognitoAppClientId() {
    return this._config.cognito.appClientId;
  }
}

export default new AppConfig();

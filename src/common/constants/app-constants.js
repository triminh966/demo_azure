const AppConstants = {
  appNameCheckIn: 'Class Check-in',
  appNameProduct: 'Product selection guide',
  appNameCI: 'Client Intake',
  defaultPagePath: {
    app: '/login',
    homePage: '/home'
  },
  api: {
    devRoot: 'http://ec2-34-227-193-251.compute-1.amazonaws.com/api-fca-dev/api',
    testRoot: 'http://ec2-34-227-193-251.compute-1.amazonaws.com/api-fca-dev/api',
    timeOut: 300000
  },
  allowedGroups: ['ST_User', 'ST_Super_User']
};

export default AppConstants;

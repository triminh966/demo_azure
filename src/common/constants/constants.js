const Constants = {
  CoefficientUnitMeasurement: 0.45359237,
  CognitoStatus: {
    Success: 'SUCCESS',
    NewPasswordRequired: 'NEW_PASSWORD_REQUIRED',
    SetNewPasswordSuccess: 'SET_NEW_PASSWORD_SUCCESSFULLY',
    ForgotPasswordSuccess: 'FORGOT_PASSWORD_SUCCESS',
    VerifyCodeSendSuccess: 'VERIFY_CODE_SEND_SUCCESS'
  },
  DefaultDayFormat: 'MM/DD/YYYY',
  PreCheckinPeriod: 30,
  LateCheckinPeriod: 10,
  PreUndoCheckinPeriod: 30,
  LateUndoCheckinPeriod: 10,
  GenderType: {
    Male: 'Male',
    Female: 'Female'
  },
  DateProfileFormat: 'YYYY-MM-DD',
  ProfileStorageKey: 'PROFILE_VALIDATED',
  SelectedDateKey: 'SELECTED_DATE',
  WearableType: {
    Arm: 0,
    Chest: 1
  },
  Relationship: {
    Parent: 'parent',
    Spouse: 'spouse',
    Friend: 'friend',
    Other: 'other'
  },
  MaritalStatusType: {
    Single: 'Single',
    Married: 'Married'
  },
  WeightMeasurement: {
    Imperial: 'LB',
    Metric: 'KG'
  },
  HeightMeasurement: {
    Imperial: 'IN',
    Metric: 'CM'
  },
  GoalCategory: {
    WeightLose: 1,
    IncMuscleTone: 2,
    IncMuscleMass: 3,
    InEnergy: 4,
    OverallHealth: 5,
    IncreaseRunningDistance: 6,
    Strength: 7,
    Endurance: 8,
    ExpFamily: 9,
    DoPositive: 10,
    OtherFitnessGoal: 11
  },
  GoalDistance: {
    Run5k: 1,
    Run10k: 2,
    RunHalfMarathon: 3,
    RunMarathon: 4
  },
  MemberStatus: {
    Booked: 'Booked',
    CheckedIn: 'Checked In',
    LateCancelled: 'Late Cancelled'
  },
   //language config
  languageConfig:{
    default:{
      languages:[
        {
          value: 'en',
          label: 'English'
        },
        {
          value: 'es',
          label: 'Español'
        },
        {
          value: 'fr',
          label: 'Français'
        }
      ]
    }
  },
  classMilestones: [1, 5, 10 ,25 ,50 , 75, 100, 150, 200, 250, 300, 350, 400, 450, 500, 1000, 2000, 3000, 4000, 5000]
};

export const SelectOptionsConstants = {
  state: [
    { value: 'AL', label: 'AL' },
    { value: 'AK', label: 'AK' },
    { value: 'AZ', label: 'AZ' },
    { value: 'AR', label: 'AR' },
    { value: 'CA', label: 'CA' },
    { value: 'CO', label: 'CO' },
    { value: 'CT', label: 'CT' },
    { value: 'DE', label: 'DE' },
    { value: 'DC', label: 'DC' },
    { value: 'FL', label: 'FL' },
    { value: 'GA', label: 'GA' },
    { value: 'HI', label: 'HI' },
    { value: 'ID', label: 'ID' },
    { value: 'IL', label: 'IL' },
    { value: 'IN', label: 'IN' },
    { value: 'IA', label: 'IA' },
    { value: 'KS', label: 'KS' },
    { value: 'KY', label: 'KY' },
    { value: 'LA', label: 'LA' },
    { value: 'ME', label: 'ME' },
    { value: 'MD', label: 'MD' },
    { value: 'MA', label: 'MA' },
    { value: 'MI', label: 'MI' },
    { value: 'MN', label: 'MN' },
    { value: 'MS', label: 'MS' },
    { value: 'MO', label: 'MO' },
    { value: 'MT', label: 'MT' },
    { value: 'NE', label: 'NE' },
    { value: 'NV', label: 'NV' },
    { value: 'NH', label: 'NH' },
    { value: 'NJ', label: 'NJ' },
    { value: 'NM', label: 'NM' },
    { value: 'NY', label: 'NY' },
    { value: 'NC', label: 'NC' },
    { value: 'ND', label: 'ND' },
    { value: 'OH', label: 'OH' },
    { value: 'OK', label: 'OK' },
    { value: 'OR', label: 'OR' },
    { value: 'PA', label: 'PA' },
    { value: 'RI', label: 'RI' },
    { value: 'SC', label: 'SC' },
    { value: 'SD', label: 'SD' },
    { value: 'TN', label: 'TN' },
    { value: 'TX', label: 'TX' },
    { value: 'UT', label: 'UT' },
    { value: 'VT', label: 'VT' },
    { value: 'VA', label: 'VA' },
    { value: 'WA', label: 'WA' },
    { value: 'WV', label: 'WV' },
    { value: 'WI', label: 'WI' },
    { value: 'WY', label: 'WY' },
    { value: 'American Samoa', label: 'American Samoa' },
    { value: 'Guam', label: 'Guam' },
    { value: 'Puerto Rico', label: 'Puerto Rico' },
    { value: 'Virgin Islands', label: 'Virgin Islands' }
  ],
  relationship: [
    { value: Constants.Relationship.Parent, label: Constants.Relationship.Parent },
    { value: Constants.Relationship.Spouse, label: Constants.Relationship.Spouse },
    { value: Constants.Relationship.Friend, label: Constants.Relationship.Friend },
    { value: Constants.Relationship.Other, label: Constants.Relationship.Other }
  ],
  isYesNo: ['Yes', 'No'],
  isYesNoPregnant: ['Yes', 'No', 'Not Applicable'],
  unitMeasurement: ['LB', 'KG'],
  achievingGoals: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
  weeklyExercise: ['N/A', '1x', '2x', '3x', '4x'],
  runningDistance: ['Run a 5K', 'Run a 10K', 'Run a half marathon', 'Run a marathon'],
  timeAchieveArr: ['1-3 months', '4-7 months', '8-12 months', '12+ months', 'Other'],
  arrChekedIn: ['Booked', 'CheckIn', 'All'],
  listMemberGoals: [
    {value: Constants.GoalCategory.WeightLose, name: 'isWeightLose'},
    {value: Constants.GoalCategory.IncMuscleMass, name: 'isIncMuscleMass'},
    {value: Constants.GoalCategory.IncMuscleTone, name: 'isIncMuscleTone'},
    {value: Constants.GoalCategory.DoPositive, name: 'isDoPositive'},
    {value: Constants.GoalCategory.Endurance, name: 'isEndurance'},
    {value: Constants.GoalCategory.ExpFamily, name: 'isExpFamily'},
    {value: Constants.GoalCategory.IncreaseRunningDistance, name: 'isIncreaseRunningDistance'},
    {value: Constants.GoalCategory.OtherFitnessGoal, name: 'isOtherFitnessGoal'},
    {value: Constants.GoalCategory.OverallHealth, name: 'isOverallHealth'},
    {value: Constants.GoalCategory.Strength, name: 'isStrength'},
    {value: Constants.GoalCategory.InEnergy, name: 'isInEnergy'}
  ],
  listGoalIds: [
    {value: Constants.GoalDistance.Run5k, name: 'Run a 5K'},
    {value: Constants.GoalDistance.Run10k, name: 'Run a 10K'},
    {value: Constants.GoalDistance.RunHalfMarathon, name: 'Run a half marathon'},
    {value: Constants.GoalDistance.RunMarathon, name: 'Run a marathon'}
  ]
};

export const LocaleSessionConstant = 'current_locale';

export default Constants;

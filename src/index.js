import React from 'react';
import ReactDOM from 'react-dom';
import { hashHistory } from 'react-router';
import { IntlProvider } from 'react-intl';

import './theme/sass/app.scss';
import 'bootstrap';
import { App } from 'modules/core/containers';
import { configureStore, makeAppRoutes } from './common';
import { setApiActions } from './services';

import { language, messages } from './localization';

const initialState = {};
const { store, actions, history } =
  configureStore({ initialState, historyType: hashHistory });

setApiActions(actions);
let render = (routerKey = null) => {
  const routes = makeAppRoutes(store);

  const mountNode = document.querySelector('#root');
  ReactDOM.render(
    <IntlProvider locale={language} messages={messages}>
      <App history={history}
        store={store}
        actions={actions}
        routes={routes}
        routerKey={routerKey} />
    </IntlProvider>, mountNode);
};

if (module.hot) {
  module.hot.accept(() => {
    render();
  });
}

render();

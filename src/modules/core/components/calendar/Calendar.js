import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// Currently, the challenge tracker app is supporting localization (fr and es)
// So that we should add fr and es locales for calendar
import 'moment/locale/fr';
import 'moment/locale/es';

import DateTimePicker from './DateTimePicker';
import './styles.scss';

const defaultFormat = 'dddd, MM/DD/YYYY';

class Calendar extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount () {
    const options = this.props.options || {};
    const { selectedDate } = options;
    const { resetDefaultDate } = this.props.inputProps || {};
    if (!resetDefaultDate) {
      this.handleOnCalendarChange(selectedDate || new Date());
    }
  }

  /**
   * Handle on calendar is changed
   * Handle for two cases: Redux form and custom onChange function
   */
  handleOnCalendarChange(newDate) {
    // Get props from Field Redux Form
    const { input } = this.props;
    const onChange = (input || {}).onChange;

    if (typeof onChange === 'function') {
      onChange(newDate);
    }

    // Emmit the onChange function that is passed from component
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newDate);
    }
  }

  render() {
    const options = this.props.options || {};
    const { inline, multiDate, format, mode, selectedDate, minDate, maxDate } = options;
    return (
      <div className='date-time-picker'>
        {!multiDate ?
          (<DateTimePicker
            options={{
              date: selectedDate || new Date(),
              locale: this.props.locale,
              inline: inline || false,
              viewMode: mode || 'days',
              format: format || defaultFormat,
              minDate,
              maxDate
            }}
            onChange={this.handleOnCalendarChange.bind(this)}
            inputProps={this.props.inputProps} />) :
          (<div/>)}
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
Calendar.propTypes = {
  input: PropTypes.object, // The props under the input key are what connects your input component to Redux
  inputProps: PropTypes.object,
  locale: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.object
};

export default connect(state => ({
  locale: state.core_language.locale
}))(Calendar);

import React from 'react';
import PropTypes from 'prop-types';
import SignaturePad from 'signature_pad';

import { Helpers } from 'common';
import './styles.scss';

export class AppSignaturePad extends React.Component {
  constructor(props) {
    super(props);

    this.guid = Helpers.guid();
  }

  componentDidMount() {
    const canvas = document.getElementById(this.guid);
    const wrap = document.getElementById(`wrap-${this.guid}`);
    canvas.width = wrap.offsetWidth;
    canvas.height = wrap.offsetHeight;
    window.addEventListener('resize', this.resizeCanvas);
    this.resizeCanvas();
    this.signaturePad = new SignaturePad(canvas, {
      onEnd: this.onEnd.bind(this)
    });

    if (this.props.data) {
      this.signaturePad.fromDataURL(this.props.data);
    }
  }

  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.renderSignature(nextProps.data);
    }
  }
  resizeCanvas() {
    const canvas = document.getElementById(this.guid);
    const wrap = document.getElementById(`wrap-${this.guid}`);
    let ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = wrap.offsetWidth * ratio;
    canvas.height = wrap.offsetHeight * ratio;
    canvas.getContext('2d').scale(ratio, ratio);
  }
  renderSignature(data) {
    this.signaturePad.fromDataURL(data);
    this.signaturePad.on();
  }

  onEnd() {
    if (this.props.onEnd && typeof this.props.onEnd === 'function') {
      this.props.onEnd(this.signaturePad);
    }
  }
  onClear() {
    if (this.props.onClear && typeof this.props.onClear === 'function') {
      this.signaturePad.clear();
      this.props.onClear(this.signaturePad);
    }
  }
  render() {
    return (
      <div id={`wrap-${this.guid}`} className='app-signature-pad full-width'>
        <canvas id={this.guid}/>
        <button className={'btn btn-prev'} onClick={this.onClear.bind(this)}>{'Clear'}</button>
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
AppSignaturePad.propTypes = {
  data: PropTypes.string,
  height: PropTypes.number,
  onClear: PropTypes.func,
  onEnd: PropTypes.func,
  width: PropTypes.number
};

export default AppSignaturePad;

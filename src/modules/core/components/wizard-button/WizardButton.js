import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import './styles.scss';

class WizardButton extends React.Component {
  constructor(props) {
    super(props);

    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
  }

  async next() {
    const { actions, isAbleNext, onCreateEvent, checkIsValidate } = this.props;
    const numberSteps = this.props.steps ? this.props.steps.length : 3;
    let currentStep = this.props.currentStep;
    if (!isAbleNext) {
      if (onCreateEvent && typeof onCreateEvent === 'function') {
        currentStep = 1;
        onCreateEvent();
        checkIsValidate();
      }
      return;
    }
    await this.props.onSubmit(currentStep);

    if (currentStep >= numberSteps) {
      currentStep = numberSteps;
    } else {
      currentStep += 1;
    }
    actions.core_wizardForm.setCurrentStep(currentStep);
  }

  prev() {
    const { actions } = this.props;
    let currentStep = this.props.currentStep;
    if (currentStep <= 1) {
      currentStep = 1;
    } else {
      currentStep -= 1;
    }
    actions.core_wizardForm.setCurrentStep(currentStep);
  }

  complete() {
    const { complete, currentStep } = this.props;
    if (complete && typeof complete === 'function') {
      this.props.complete();
    }
    this.props.onSubmit(currentStep);
  }

  render() {
    const { clientIntake, isAbleSubmit } = this.props;
    const numberSteps = 3;
    return (
      <div className={'wizard-button'}>
        {this.props.currentStep > 1 &&
          <button type='button' className='btn btn-prev' onClick={this.prev}>{'PREVIOUS'}</button>}
        {this.props.currentStep < numberSteps &&
          <button type='button' className='btn btn-primary btn-next' onClick={this.next}>{'NEXT'}</button>}
        {this.props.currentStep >= numberSteps &&
          <button type='button' className='btn btn-primary btn-next' disabled={clientIntake.code === 'SUCCESS' || !isAbleSubmit}
            onClick={this.complete.bind(this)}>{'SUBMIT'}</button>}
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
WizardButton.propTypes = {
  actions: PropTypes.any,
  checkIsValidate: PropTypes.func,
  clientIntake: PropTypes.object,
  complete: PropTypes.func,
  currentStep: PropTypes.number,
  isAbleNext: PropTypes.bool,
  isAbleSubmit: PropTypes.bool,
  onCreateEvent: PropTypes.func,
  onSubmit: PropTypes.func,
  steps: PropTypes.number
};

export default connect(state => ({
  currentStep: state.core_wizardForm.currentStep
}))(WizardButton);

import React from 'react';
import PropTypes from 'prop-types';
import { intlShape, injectIntl } from 'react-intl';

import './styles.scss';

class WizardProgressBar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { steps, stepIndex, isComplete } = this.props;

    return (
      <div className='wizard-progress'>
        <div className='progress'>
          <div className='progress-steps'>
            {steps && steps.map((step, index) => {
              let status;
              if (stepIndex > index + 1 || isComplete) {
                status = 'complete';
              } else if (stepIndex === index + 1) {
                status = 'in-progress';
              } else {
                status = '';
              }
              return (<div key={index} className={`step ${status}`}>
                <span>{step.title}</span>
                <div className='node' />
              </div>);
            })}
          </div>
        </div>
      </div>);
  }
}

WizardProgressBar.propTypes = {
  actions: PropTypes.any,
  intl: intlShape.isRequired,
  isComplete: PropTypes.bool,
  stepIndex: PropTypes.number,
  steps: PropTypes.array
};

export default injectIntl(WizardProgressBar);

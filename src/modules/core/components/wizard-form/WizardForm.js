import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import WizardProgressBar from '../wizard-progress-bar/WizardProgressBar';
import './styles.scss';

class WizardForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { className, renderSteps, steps, currentStep, clientIntake } = this.props;
    return (
      <div className={`wizard-form ${className}`}>
        <WizardProgressBar stepIndex={currentStep} isComplete={clientIntake.code === 'SUCCESS'} steps={steps}/>
        {renderSteps ? renderSteps(currentStep || 1) : ''}
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
WizardForm.propTypes = {
  className: PropTypes.string,
  clientIntake: PropTypes.object,
  complete: PropTypes.func,
  currentStep: PropTypes.number,
  isAbleSubmit: PropTypes.bool,
  onSubmit: PropTypes.func,
  renderSteps: PropTypes.func,
  steps: PropTypes.array
};

export default connect(state => ({
  currentStep: state.core_wizardForm.currentStep
}))(WizardForm);

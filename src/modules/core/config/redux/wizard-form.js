import { createConstants, createReducer } from 'redux-module-builder';

/**
 * Define wizard form action constants
 */
export const types = createConstants('wizard')(
  'SET_CURRENT_STEP',
  'RESET_CURRENT_STEP'
);

export const actions = {
  /**
   * Set current step for wizard form
   * @param {number} step current step
   */
  setCurrentStep: (step) => (dispatch) => {
    dispatch({ type: types.SET_CURRENT_STEP, payload: step});
  },
  resetCurrentStep: () => (dispatch) => {
    dispatch({ type: types.RESET_CURRENT_STEP });
  }
};

export const reducer = createReducer({
  [types.SET_CURRENT_STEP]: (state, { payload }) => {
    return {
      ...state,
      currentStep: payload
    };
  },
  [types.RESET_CURRENT_STEP]: (state) => {
    return {
      ...state,
      currentStep: 1
    };
  }
});

export const initialState = {
  currentStep: 1
};

import { createConstants, createReducer } from 'redux-module-builder';

import { alertConstants } from './alert';

/**
 * Define errors action constants
 */
export const errorHandlerConstants = createConstants('errors')(
  'HANDLE_ERROR',
  'DESTROY_ERROR'
);

export const actions = {
  /**
   * Handle error exception
   * @param {any} error The error object
   */
  handleError: (error) => (dispatch, getState) => {
    let standardizedError;
    let errorMessage;

    // Standardize error
    if (typeof error === 'string') {
      standardizedError = new Error(error);
    } else if (error instanceof Error) {
      standardizedError = error;
    } else if (error.message && error.code) {
      // If error is returned from server
      if (typeof error.message === 'string') {
        standardizedError = new Error(error.message);
      }
    }

    if (!standardizedError) {
      standardizedError = new Error('Something wrong happened!');
    }

    // Try get error message from response
    if (error.response && error.response.data) {
      errorMessage = error.response.data.errorMessage ? error.response.data.errorMessage : error.response.data;
      if (error.response.data.errorCode === 201 && error.response.data.statusCode === 5) {
        errorMessage = 'The last sign-in was not accepted. The client is already scheduled at this time';
      }
    } else if (error.details) {
      errorMessage = '';

      for (let errorItem of error.details) {
        if (typeof errorItem === 'object') {
          errorMessage = errorMessage ? `${errorMessage}<br/><p>${errorItem.message}</p>` : `<p>${errorItem.message}</p>`;
        } else if (typeof errorItem === 'string') {
          errorMessage = errorMessage ? `${errorMessage}<br/><p>${errorItem}</p>` : `<p>${errorItem}</p>`;
        }
      }
    } else {
      errorMessage = error.message;
    }

    // Log error / send stack trace error to remote server
    // Or using Sentry (Raven.js), OverOps for error tracking

    // If this value is false, shouldn't show alert/notification
    if (!getState().core_errorHandler.isSilence) {
      const alertOptions = getState().core_errorHandler.alertOptions;

      // We can using modal or toast for notify/alert to end user
      dispatch({ type: alertConstants.SHOW_ERROR, payload: { message: errorMessage, options: alertOptions } });
    } else {
      dispatch({ type: errorHandlerConstants.DESTROY_ERROR });
    }
  },

  /**
   * Destroy error
   */
  destroyError: () => (dispatch) => {
    dispatch({ type: errorHandlerConstants.DESTROY_ERROR });
  }
};

export const reducer = createReducer({
  [errorHandlerConstants.HANDLE_ERROR]: (state, { payload }) => {
    let isSilence = payload.isSilence === undefined || payload.isSilence === null ?
      false :
      payload.isSilence;

    return {
      ...state,
      alertOptions: payload.alertOptions,
      error: (payload.error.response) ? payload.error.response.data : payload.error,
      isSilence
    };
  },
  [errorHandlerConstants.DESTROY_ERROR]: (state) => {
    return {
      ...state,
      error: undefined,
      isSilence: false
    };
  }
});

export const initialState = {
  error: undefined,
  isSilence: false,
  alertOptions: undefined
};

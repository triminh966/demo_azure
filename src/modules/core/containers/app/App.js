import React from 'react';
import PropTypes from 'prop-types';
import { Router } from 'react-router';
import { connect, Provider } from 'react-redux';

import {
  LoadingIndicator,
  Modal,
  ErrorHandler,
  Alert
} from 'modules/core/components';
import { LanguageProvider } from '..';
import { messages } from 'localization';

import './app.scss';

class App extends React.Component {
  componentDidMount() {
    const { actions } = this.props;
    const self = this;

    $(document).on('click', function () {
      if (self.canExpandSreen()) {
        actions.core_screen.firstScreenLoad();
        actions.core_screen.expand();
      }
    });
  }

  canExpandSreen() {
    return window.innerHeight !== screen.height && !this.props.firstScreenLoad;
  }

  get content() {
    const { history, routes, routerKey, actions } = this.props;
    let newProps = {
      actions,
      ...this.props
    };

    const createElement = (Component, props) => {
      return <Component {...newProps} {...props} />;
    };

    return (
      <Router
        key={routerKey}
        routes={routes}
        createElement={createElement}
        history={history} />
    );
  }

  renderAppContent() {
    return (
      <div className={'orange-app-container'}>
        {this.content}
        <Modal actions={this.props.actions} />
        <Alert history={this.props.history} actions={this.props.actions} />
        <ErrorHandler actions={this.props.actions} />
        <LoadingIndicator />
      </div>
    );
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <LanguageProvider messages={messages}>
          <div>
            {this.renderAppContent()}
          </div>
        </LanguageProvider>
      </Provider>
    );
  }
}

App.propTypes = {
  actions: PropTypes.object,
  firstScreenLoad: PropTypes.bool,
  history: PropTypes.object.isRequired,
  router: PropTypes.object,
  routerKey: PropTypes.number,
  routes: PropTypes.element.isRequired,
  store: PropTypes.any
};

export default connect(state => ({
  firstScreenLoad: state.core_screen.firstScreenLoad
}))(App);

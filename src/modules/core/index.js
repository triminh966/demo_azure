import * as LoadingIndicatorRedux from './config/redux/loading-indicator';
import * as ModalRedux from './config/redux/modal';
import * as ErrorHandlerRedux from './config/redux/error-handler';
import * as AlertRedux from './config/redux/alert';
import * as FullscreenRedux from './config/redux/fullscreen';
import * as LanguageRedux from './config/redux/language';
import * as WizardFormRedux from './config/redux/wizard-form';

const CoreModule = {
  makeRoutes: () => {},
  redux: {
    loading: LoadingIndicatorRedux,
    modal: ModalRedux,
    errorHandler: ErrorHandlerRedux,
    alert: AlertRedux,
    screen: FullscreenRedux,
    language: LanguageRedux,
    wizardForm: WizardFormRedux
  }
};

export default CoreModule;

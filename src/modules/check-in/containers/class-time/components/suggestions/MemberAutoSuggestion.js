import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import _ from 'lodash';

import { AutoSuggestion } from 'modules/core/components';
import { Helpers } from 'common';

import './styles.scss';

export class MemberAutoSuggestion extends React.Component {
  constructor(props) {
    super(props);
    this.actions = this.props.actions.clientTake_members;
    this.state = {
      member: ''
    };
  }

  onChange(event, newValue) {
    this.actions.updateCurrentMember(newValue);
    this.setState({ member: newValue });
  }

  onSuggestionsFetchRequested(value) {
    const { loggedInStudioId, filterStudioId } = this.props;
    if (value) {
      this.actions.searchMembersSuggestion(value, loggedInStudioId, filterStudioId, true);
    } else {
      this.actions.clearMemberSuggestion();
    }
  }

  onSuggestionsClearRequested() {
    this.actions.clearMemberSuggestion();
  }

  renderSuggestion(suggestion, query) {
    const suggestionText = `${suggestion.fullName}`;
    const matches = match(suggestionText, query);
    const parts = parse(suggestionText, matches);
    const displayEmail = Helpers.renderEmail(suggestion.email);
    return (
      <div className={'no-underline'}>
        <div className={`suggestion-content ${suggestion.gender.toLowerCase()}`}>
          <p className='name member'>
            {parts.map((part, index) => {
              const className = part.highlight ? 'highlight' : null;
              return <span className={className} key={index}>{part.text}</span>;
            })}
          </p>
        </div>
        <p className='email'>{displayEmail}</p>
      </div>
    );
  }

  render() {
    let value = this.state.member;
    let suggestions = this.props.members;
    const inputProps = {
      id: 'autosuggestInput',
      name: this.props.name,
      className: 'i-alKeyboard',
      value,
      onKeyPress: this.props.onKeyPress,
      autoComplete: 'off'
    };
    const getSuggestionValue = (suggestion) => {
      this.props.selectMember(suggestion);
      return `${suggestion.firstname} ${suggestion.lastname}`;
    };

    return (
      <AutoSuggestion
        input={inputProps}
        isServerFetching={this.props.fetching}
        onChange={this.onChange.bind(this)}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested.bind(this)}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested.bind(this)}
        renderSuggestion={this.renderSuggestion.bind(this)}
        getSuggestionValue={getSuggestionValue}
        suggestions={suggestions}
        selectedValue={this.props.member} />
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
MemberAutoSuggestion.propTypes = {
  actions: PropTypes.any,
  currentStudio: PropTypes.object,
  fetching: PropTypes.bool,
  filterStudioId: PropTypes.number,
  loggedInStudioId: PropTypes.number,
  member: PropTypes.any,
  members: PropTypes.any,
  name: PropTypes.string,
  onKeyPress: PropTypes.func,
  selectMember: PropTypes.func
};

export default connect(state => ({
  fetching: state.clientTake_members.fetching,
  members: state.clientTake_members.suggestions,
  member: state.clientTake_members.member,
  currentStudios: state.auth_users.currentStudio
}))(MemberAutoSuggestion);

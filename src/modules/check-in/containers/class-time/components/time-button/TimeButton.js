import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const TimeButton = (props)=>{
  return (
    <div className={`time-button ${props.active}`} onClick={props.handleClick}>
      <div className='time'>{props.time}</div>
      <div className='group'>
        <p className='coach'>{props.coach.Name}</p>
        <p className='name'>{props.name} </p>
      </div>
      {/* TBD
      <div className='number'>{props.totalMembers}
        <i className='fa fa-angle-right' />
      </div> */}
    </div>
  );
};

TimeButton.propTypes = {
  active: PropTypes.any,
  coach: PropTypes.any,
  end: PropTypes.string,
  handleClick: PropTypes.func,
  name: PropTypes.string,
  time: PropTypes.string,
  totalIntroCount: PropTypes.any,
  totalMembers: PropTypes.any
};
export default TimeButton;

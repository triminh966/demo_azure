import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import './styles.scss';

const DateCheckIn = (props) =>{
  return (
    <div className='date-checkin'>
      <h1>{moment(props.selectedDate).format('dddd MMM, D')}</h1>
    </div>
  );
};
DateCheckIn.propTypes = {
  selectedDate: PropTypes.any
};
export default DateCheckIn;

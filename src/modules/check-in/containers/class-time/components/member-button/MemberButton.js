import React from 'react';
import PropTypes from 'prop-types';
import { intlShape } from 'react-intl';

import { Helpers, Constants } from 'common';
import moment from 'moment';
import { MaleIcon, FemaleIcon, Logo, IntroMember, IntroMemberGrey } from 'modules/shared/images';
import './styles.scss';
import SplatIcon from './images/splat.svg';

export default class MemberButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelect: false,
      today: new Date(),
      isActive: false
    };
  }

  renderHighlight(isBefore, isAfter) {
    if (isBefore > 2 || isBefore === 0 || isAfter > 2 || isAfter === 0) {
      return false;
    }
    return true;
  }
  getMemberId = () => {
    this.setState({
      isActive: !this.state.isActive
    });
  }
  renderAvatarDefault() {
    return (<img src={SplatIcon} className="avatar-default" />)
  }
  renderMemberPhoto(member) {
    if (!member.profilePictureUrl) {
      return this.renderAvatarDefault();
    }
    return (<img src={member.profilePictureUrl}
      ref={img => { this.img = img; }} onError={() => { this.renderAvatarDefault() }}
      className='avatar' />)
  }

  handleCheck() {
    this.setState({
      isSelect: !this.state.isSelect
    });
  }

  notifyCIRequired(CIRequired) {
    if (CIRequired) {
      return (<span className='notificator'>{'CI Form Required'}</span>);
    }
    return null;
  }

  notifyNumberOfClasses(totalClassesAttended) {
    if (totalClassesAttended === 0) {
      return (<span><img src={IntroMember} className='intro-member-noti' /></span>);
    }
    if (totalClassesAttended <= 5) {
      return (<span><img src={IntroMemberGrey} className='intro-member-noti' /></span>);
    }
    return null;
  }

  notifyBirthday(isShowDOB, birthDate) {
    if (isShowDOB && birthDate) {
      return (<span className='notificator'> {`Birthday ${birthDate}`}</span>);
    }
    return null;
  }

  notifyCrossRegional(isCrossRegional) {
    if (isCrossRegional) {
      return (<span className='notificator'>{'Cross Regional'}</span>);
    }
    return null;
  }

  /**
  * Show notification if the current class is a milestone class
  * @param any totalClassesAttended - member total number of classes attended, including the current class
  * @return http element - class milestone description
  */
  notifyClassMilestone(totalClassesAttended) {
    if (totalClassesAttended === 1) {
      return (<span className='notificator'>{'1st Class'}</span>);
    }
    if (Constants.classMilestones.includes(totalClassesAttended)) {
      return (<span className='notificator'>{`${totalClassesAttended}th Class`}</span>);
    }
    return null;
  }

  render() {
    let { member, intl, isDisabled, isLateCancel } = this.props;
    member = member || {};
    const memberName = Helpers.formatMemberName(member.firstname, member.lastname);
    const CIRequired = member.requiredCI === true || member.requiredCI === 'true';
    const displayEmail = Helpers.renderEmail(member.email);
    const canCheckIn = member.status === Constants.MemberStatus.Booked || member.status === 'false';
    const totalClassesAttended = member.totalClassesAttended;
    const isCrossRegional = member.isCrossRegional === true || member.isCrossRegional === 'true';

    let allowCheckIn = null;
    if (!isDisabled) {
      allowCheckIn = canCheckIn ? this.props.onClick : this.props.onUndoCheckIn;
    }
    const birthDate = (typeof member.birthday === 'undefined') ? '' : moment(member.birthday).format('MM/DD');
    const today = moment(new Date()).format('MM/DD');
    const showDOB = birthDate === today;
    const isShowDOB = showDOB ? moment(birthDate).format('DD') === moment(this.props.selectedDate).format('DD') : false;
    const isBefore = showDOB ? Math.abs(parseInt(moment(this.props.selectedDate).format('DD'), 10) - parseInt(moment(member.birthday).format('DD'), 10)) : 0;
    const isAfter = showDOB ? Math.abs(parseInt(moment(birthDate).format('DD'), 10) - parseInt(moment(this.props.selectedDate).format('DD'), 10)) : 0;
    const activeHighlight = this.renderHighlight(isBefore, isAfter);
    return (
      <div className={`${this.props.className} member-button`}>
        <div className={`member-info ${isDisabled ? 'disabled' : ''} ${(activeHighlight && !this.state.isActive) ? 'active' : ''}`} onClick={this.getMemberId}>
          <div className='icon-info'>
            <div className='avatar-icon'>
              {this.renderMemberPhoto(member)}
            </div>
          </div>
          <div className='middle-info'>
            <span className='name'>{memberName}</span>
            {this.notifyNumberOfClasses(totalClassesAttended)}
            {this.notifyCIRequired(CIRequired)}
            {this.notifyBirthday(isShowDOB, birthDate)}
            {this.notifyCrossRegional(isCrossRegional)}
            {this.notifyClassMilestone(totalClassesAttended + 1)}
            <p className='email'>{displayEmail}</p>
          </div>
          <div className='congrations' />
          <div className='intro-member' />
          {!isLateCancel && <div className='submit-check-in'>
            {!isDisabled && <button
              className={`check-in-btn ${!canCheckIn ? 'signed' : ''}`}
              onClick={allowCheckIn}>{`${canCheckIn ? intl.formatMessage({ id: 'ClassRoster.Member.CheckInBtn' })
                : intl.formatMessage({ id: 'ClassRoster.Member.UndoCheckInBtn' })}`}</button>}
          </div>}

        </div>
      </div>
    );
  }
}

MemberButton.propTypes = {
  className: PropTypes.string,
  intl: intlShape.isRequired,
  isDisabled: PropTypes.bool,
  isLateCancel: PropTypes.any,
  member: PropTypes.object,
  onClick: PropTypes.func,
  onUndoCheckIn: PropTypes.func,
  selectedDate: PropTypes.any
};


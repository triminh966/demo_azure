import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {StartTime} from '..';
import './styles.scss';
export default class CountMember extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      selectedClass,
      checkedInCount,
      totalBooked,
      checkInRemain,
      checkLateCancelled,
      selectedDate
    } = this.props;
    const classStartDateTime = selectedClass.classStartDateTime;
    return (
      <div className='count-member'>
        <div className='count-booked'>
          <p className='number'>{totalBooked || 0}</p>
          <p className='title'>{'Total'}</p>
        </div>
        <div className='late-cancel'>
          <p className='number'>{checkLateCancelled || 0}</p>
          <p className='title'>{'Late Cancelled' }</p>
        </div>
        <div className='count-checkin'>
          <p className='number'>{checkedInCount || 0}</p>
          <p className='title'>{'Checked In'}</p>
        </div>
        <div className='count-remaining'>
          <p className='number'>{checkInRemain || 0}</p>
          <p className='title'>{'Remaining'}</p>
        </div>
        {classStartDateTime &&
        <StartTime
          selectedDate={selectedDate}
          classStartTime={moment(classStartDateTime).format('YYYY-MM-DDTHH:mm:ss')}/>}
      </div>
    );
  }
}

CountMember.propTypes = {
  checkInRemain: PropTypes.any,
  checkLateCancelled: PropTypes.any,
  checkedInCount: PropTypes.any,
  isRosterActive: PropTypes.bool,
  location: PropTypes.any,
  routing: PropTypes.any,
  selectedClass: PropTypes.object,
  selectedDate: PropTypes.any,
  totalBooked: PropTypes.any
};

/**
 * React / Redux dependencies
 */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { intlShape, injectIntl } from 'react-intl';
import Highlighter from 'react-highlight-words';
import _ from 'lodash';

import { languageWithoutRegionCode as defaultLocale } from 'localization';
import { Calendar, FormSelect } from 'modules/core/components';
import { MemberAutoSuggestion } from '..';

import './styles.scss';
import { UserAPI } from 'services';

export class AddMemberForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classUUId: props.classUUId,
      classId: props.classId,
      mboClassId: props.mboClassId,
      mboStudioId: props.mboStudioId,
      currentTime: props.currentTime,
      selectedDate: props.selectedDate,
      studioId: props.studioId,
      startDateTimeUTC: moment.utc(_.get(props, 'selectedClass.classStartDateTime')),
      endDateTimeUTC: moment.utc(_.get(props, 'selectedClass.classStartDateTime')).add(1, 'h')
    };
  }
  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(nextProps) {
    const { member, ok } = this.props;
    if (nextProps.member && nextProps.member !== member) {
      this.setState({ showResult: true, triggerSearch: false });
    }
    if (nextProps.isAddMemberSuccessfully) {
      ok();
    }
  }

  componentWillUnmount() {
    this.props.actions.checkIn_roster.clearLatestVisitForMember();
    this.props.actions.checkIn_roster.clearSelectedProfile();
    this.props.actions.checkIn_roster.clearStatusAddMember();
  }

  renderClassName() {
    const {selectedClass} = this.props;

    if (!selectedClass) { return undefined; }
    let className = selectedClass.className;

    return `${className}`;
  }

  renderCoachName() {
    const {selectedClass} = this.props;
    return selectedClass ? selectedClass.coachName : undefined;
  }
  selectClass(selectedClass) {
    this.setState((prevState) => {
      return {
        ...prevState,
        classUUId: selectedClass.ClassUUId,
        classId: selectedClass.ClassId,
        mboClassId: selectedClass.MBOClassId,
        mboStudioId: selectedClass.MBOStudioId,
        currentTime: moment(selectedClass.StartTime).locale(defaultLocale).format('hh:mm A'),
        className: selectedClass.ClassName,
        coachName: `${selectedClass.Coach.FirstName} ${selectedClass.Coach.LastName}`,
        startDateTimeUTC: moment.utc(selectedClass.StartTime),
        endDateTimeUTC: moment.utc(selectedClass.EndTime)
      };
    });
  }

  changeCalendar(newDate) {
    this.setState({
      selectedDate: newDate
    }, () => {
      if (this.props.currentStudio) {
        let currStudio = this.props.currentStudio;
        this.props.actions.checkIn_classes.getAllSchedule(currStudio.StudioId, moment(newDate).format('MM/DD/YYYY'));
      }
      this.props.actions.checkIn_classes.selectDate(this.state.selectedDate);
    });
  }
  selectMember(member) {
    this.setState((prevState) => {
      return { ...prevState, selectedMember: member };
    });
  }

  onAddMember() {
    const { classId, selectedMember, mboClassId, mboStudioId, studioId, startDateTimeUTC, endDateTimeUTC } = this.state;
    this.props.actions.checkIn_roster.addMember(
      classId,
      {
        memberId: selectedMember.memberId,
        mboStudioId,
        mboClassId,
        studioId,
        waitlist: true,
        moveFromWaitlist: false,
        startDateTimeUTC,
        endDateTimeUTC,
        crossRegionalBooking: selectedMember.isCrossRegional
      });
  }
  /**
   * Handle select change event
   * @param {string} name The name of select
   */
  handleSelectChange(name) {
    return function (newValue) {
      this.setState((state) => {
        return {
          ...state,
          [name]: newValue.value,
          selectedMember: undefined
        };
      }, () => {
        $('[name=\'member-name\']').val('');
      });
    }.bind(this);
  }
  /**
   * Customize the studio option
   */
  studioOptionRenderer(option) {
    return (
      <div>
        <Highlighter
          highlightClassName='highlight-select'
          searchWords={[this.studioSelectValue]}
          textToHighlight={option.label} />
        <span>{` / ${option.address}`}</span>
      </div>
    );
  }
  loadStudioOptions() {
    if (!this.studioSelectValue || this.studioSelectValue === '') {
      return Promise.resolve({
        options: [
          { value: this.props.currentStudio.StudioId,
            label: this.props.currentStudio.StudioName,
            address: this.props.currentStudio.StudioAddress
          }]
      });
    }
    let complete = false;
    return UserAPI.getStudioSuggestions(this.studioSelectValue, { isUndergroundReq: true })
      .then(res => {
        if (res) {
          let options = res.data.map((item) => {
            return {
              value: item.StudioId,
              label: item.StudioName,
              address: item.StudioAddress
            };
          });
          return { options, complete };
        }
        return { options: [], complete };
      }, () => {
        // Silent handle error
        return { options: [], complete };
      });
  }
  render() {
    const { classes, close, selectedClass, actions, currentStudio } = this.props;
    const {selectedDate, currentTime, coachName, className, studioId, selectedMember} = this.state;
    let classTime = classes || [];
    let classTimeAvailable = _.filter(classTime, function(ct) {
      return !ct.isCancelled;
    });
    let allowAdd = _.find(classTimeAvailable, (obj) => {
      return obj.ClassId === this.state.classId;
    });
    let tds = [];
    let trs = [];
    let self = this;
    for (let i = 0; i < classTimeAvailable.length; i++) {
      let time = moment(classTimeAvailable[i].StartTime).locale(defaultLocale).format('hh:mm A');
      let classId = classTimeAvailable[i].ClassId;
      if (tds.length < 3) {
        tds.push(<td className='td-time'>
          <div className={`time-btn${self.state.classId === classId ? ' selected' : ''}`} onClick={() => this.selectClass.bind(this)(classTimeAvailable[i])}>
            {time}
          </div>
        </td>);
      } else {
        trs.push(<tr key={i}>{tds}</tr>);
        tds = [];
        tds.push(<td className='td-time'>
          <div className={`time-btn${self.state.classId === classId ? ' selected' : ''}`} onClick={() => this.selectClass.bind(this)(classTimeAvailable[i])}>
            {time}
          </div>
        </td>);
      }
      if ((i + 1) >= classTimeAvailable.length) {
        trs.push(<tr key={i + 1}>{tds}</tr>);
        break;
      }
    }
    return (
      <div className='add-member-form'>
        <div className='col-md-5 left-content'>
          <Calendar
            options={{
              inline: true, selectedDate,
              maxDate: moment(new Date().setDate(new Date().getDate() + 29)),
              minDate: moment(new Date().setDate(new Date().getDate() - 30))
            }}
            onChange={this.changeCalendar.bind(this)}/>
          <div className='class-time'>
            <table>
              <tbody>
                {trs}
              </tbody>
            </table>
          </div>
        </div>
        <div className='col-md-7 right-content'>
          <p className='close-btn' onClick={close}>{'X'}</p>
          <p className='title'>{'Add to class'}</p>
          {allowAdd && <React.Fragment>
            <p className='time'>{`${moment(selectedDate).format('dddd, MMMM Do')} • ${currentTime}`}</p>
            <p className='class-info'>{`${className || this.renderClassName()} • ${coachName || this.renderCoachName()}`}</p>
          </React.Fragment>}
          <FormSelect
            name='individual'
            value={studioId}
            placeholder={'Select Studio...'}
            async={true}
            optionRenderer={this.studioOptionRenderer.bind(this)}
            className='select-country'
            loadOptions={this.loadStudioOptions.bind(this)}
            onInputChange={(value) => { this.studioSelectValue = value; }}
            onChange={this.handleSelectChange('studioId')} />
          <div className='search-member row'>
            <div className='search'>
              <p className='ph-name'>{'Name'}</p>
              <MemberAutoSuggestion
                name={'member-name'}
                filterStudioId={studioId}
                loggedInStudioId={currentStudio.StudioId}
                actions={actions}
                selectMember={this.selectMember.bind(this)}
                selectedClass={selectedClass} />
            </div>
          </div>
          <button className='add-btn' disabled={!(allowAdd && selectedMember)} onClick={this.onAddMember.bind(this)}>{'Add'}</button>
        </div>
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
AddMemberForm.propTypes = {
  actions: PropTypes.any,
  allStudios: PropTypes.array,
  classId: PropTypes.number,
  classUUId: PropTypes.string,
  classes: PropTypes.array,
  close: PropTypes.func,
  currentStudio: PropTypes.object,
  currentTime: PropTypes.string,
  intl: intlShape.isRequired,
  isAddMemberSuccessfully: PropTypes.string,
  mboClassId: PropTypes.string,
  mboStudioId: PropTypes.number,
  member: PropTypes.string,
  ok: PropTypes.func,
  selectedClass: PropTypes.object,
  selectedDate: PropTypes.any,
  studioId: PropTypes.number
};

const addMemberForm = connect(state => ({
  currentStudio: state.auth_users.currentStudio,
  selectedClass: state.checkIn_roster.selectedClass,
  isAddMemberSuccessfully: state.checkIn_roster.isAddMemberSuccessfully,
  members: state.clientTake_members.members,
  member: state.clientTake_members.member,
  studio: state.auth_users.currentStudio,
  classes: state.checkIn_classes.classes,
  allStudios: state.auth_users.allStudios
}))(AddMemberForm);

export default injectIntl(addMemberForm);

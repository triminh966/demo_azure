import React, { Component } from 'react';
import moment from 'moment';
import DateCheckIn from '../date-checkin/DateCheckIn';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { intlShape, injectIntl, FormattedMessage } from 'react-intl';

import './styles.scss';
class CalendarCheckIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentDate: new Date(),
      today: new Date(),
      selectedDate: new Date()
    };
    this.nextMonth = this.nextMonth.bind(this);
    this.prevMonth = this.prevMonth.bind(this);
    this.onDateClick = this.onDateClick.bind(this);
  }

  componentDidMount() {
    let date = moment(this.state.today);
    this.props.actions.checkIn_classes.selectDate(date);
  }
  nextMonth(dates) {
    let nextFrom = moment(new Date()).add(29, 'day').format('YYYY/MM/DD');
    for (let i = 0; i < dates.length; i++) {
      if (dates[i] === nextFrom) {
        return;
      }
    }
    this.setState({
      today: moment(this.state.today).add(1, 'day')
    });
  }

  prevMonth(dates) {
    let prevFrom = moment(new Date()).subtract(29, 'day').format('YYYY/MM/DD');
    for (let i = 0; i < dates.length; i++) {
      if (dates[i] === prevFrom) {
        return;
      }
    }
    this.setState({
      today: moment(this.state.today).add(-1, 'day')
    });
  }

  async onDateClick(date, disablePrve, disableNext) {
    if (disablePrve || disableNext) {return;}
    const { StudioId } = this.props.currentStudio;
    let selectedDate = moment(date);
    this.setState({
      selectedDate,
      today: selectedDate
    });
    await this.props.actions.checkIn_classes.selectDate(selectedDate);
    await this.props.actions.checkIn_classes.getAllSchedule(StudioId, selectedDate);
  }

  generateDateRange(
    from,
    to,
    step = 1,
    modifier = 'day',
    format = 'YYYY/MM/DD'
  ) {
    const dates = [];
    let currentDate = moment(from);
    const lastDate = moment(to);
    while (currentDate <= lastDate) {
      dates.push(currentDate.format(format));
      currentDate = currentDate.add(step, modifier);
    }
    return dates;
  }

  renderDays(dates) {
    let days = [];
    let prevFrom = moment(this.state.currentDate).subtract(30, 'day');
    let nextFrom = moment(this.state.currentDate).add(29, 'day');
    const isOpacityDate = moment(new Date()).add(-1, 'day');
    dates.map((day, i) => {
      let cls = moment(day).isSame(this.state.currentDate, 'day') ? 'rbc-today' : '';
      let result = isOpacityDate.isBefore(day);
      let disablePrve = prevFrom.isBefore(day);
      let disableNext = nextFrom.isAfter(day);
      if (this.props.selectedDate && moment(day).isSame(this.props.selectedDate, 'day')) {
        cls += ' active';
      }
      days.push(
        <div
          className={`day ${cls} ${result ? '' : 'opacity-date'} ${disablePrve ? '' : 'disable-date'}${disableNext ? '' : 'disable-date'} `}
          onClick={() => this.onDateClick(day, !disablePrve, !disableNext)}
          key={i}>
          <span className='number'> {moment(day).format('DD')}</span>
        </div>
      );
    });

    return <div className='col-center'>{days}</div>;
  }


  render() {
    const start = moment(this.state.today).add(-2, 'day');
    const stop = moment(this.state.today).add(2, 'day');
    const dates = this.generateDateRange(start, stop, 1, 'day');
    let renderDays = this.renderDays(dates);
    return (
      <div className='calendar-checkin'>
        <div className='weekly-checkin'>
          <div className='col col-start' onClick={() => this.prevMonth(dates)} >
            <span className={'glyphicon glyphicon-chevron-left '} />
          </div>
          {renderDays}
          <div className='col col-end' onClick={() => this.nextMonth(dates)}>
            <span className='glyphicon glyphicon-chevron-right' />
          </div>
        </div>
        <DateCheckIn selectedDate={this.props.selectedDate} />
      </div>
    );
  }
}
CalendarCheckIn.propTypes = {
  actions: PropTypes.any,
  currentStudio: PropTypes.any,
  selectedDate: PropTypes.any
};
export default injectIntl(
  connect(state => ({
    classes: state.checkIn_classes.classes,
    selectedDate: state.checkIn_classes.selectedDate,
    errors: state.checkIn_classes.errors,
    currentStudio: state.auth_users.currentStudio
  }))(CalendarCheckIn)
);

import React from 'react';
import PropTypes from 'prop-types';

import {
  Helpers,
  Constants
} from 'common';
import Splat from '../member-button/images/splat.svg';
import { IntroMember, IntroMemberGrey } from 'modules/shared/images';
import './styles.scss';
import moment from 'moment';

export default class MultiMemberButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelect: false
    };
  }

  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.isSelect) {
      this.setState({
        isSelect: true
      });
    }
  }

  renderAvatarDefault() {
    return (<img src={Splat} className="avatar-default" />)
  }

  renderMemberPhoto(member) {
    if (!member.profilePictureUrl) {
      return this.renderAvatarDefault();
    }
    return (<img src={member.profilePictureUrl}
      ref={img => { this.img = img; }} onError={() => { this.renderAvatarDefault() }}
      className='avatar' />)
  }

  handleCheck(e) {
    this.props.select(e.target.checked);
    this.setState({
      isSelect: e.target.checked
    });
  }

  notifyCIRequired(CIRequired) {
    if (CIRequired) {
      return (<span className='notificator'>{'CI Form Required'}</span>);
    }
    return null;
  }

  notifyNumberOfClasses(totalClassesAttended) {
    if (totalClassesAttended === 0) {
      return (<span><img src={IntroMember} className='intro-member-noti' /></span>);
    }
    if (totalClassesAttended <= 5) {
      return (<span><img src={IntroMemberGrey} className='intro-member-noti' /></span>);
    }
    return null;
  }

  notifyBirthday(isShowDOB, birthDate) {
    if (isShowDOB && birthDate) {
      return (<span className='notificator'> {`Birthday ${birthDate}`}</span>);
    }
    return null;
  }

  notifyCrossRegional(isCrossRegional) {
    if (isCrossRegional) {
      return (<span className='notificator'>{'Cross Regional'}</span>);
    }
    return null;
  }

  /**
  * Show notification if the current class is a milestone class
  * @param any totalClassesAttended - member total number of classes attended, including the current class
  * @return http element - class milestone description
  */
  notifyClassMilestone(totalClassesAttended) {
    if (totalClassesAttended === 1) {
      return (<span className='notificator'>{'1st Class'}</span>);
    }
    if (Constants.classMilestones.includes(totalClassesAttended)) {
      return (<span className='notificator'>{`${totalClassesAttended}th Class`}</span>);
    }
    return null;
  }

  render() {
    let {
      member,
      isDisabled, id
    } = this.props;
    member = member || {};
    const memberName = Helpers.formatMemberName(member.firstname, member.lastname);
    const displayEmail = Helpers.renderEmail(member.email);
    const CIRequired = member.requiredCI === true || member.requiredCI === 'true';
    const totalClassesAttended = member.totalClassesAttended;
    const isCrossRegional = member.isCrossRegional === true || member.isCrossRegional === 'true';
    const birthDate = (typeof member.birthday === 'undefined') ? '' : moment(member.birthday).format('MM/DD');
    const today = moment(new Date()).format('MM/DD');
    const showDOB = birthDate === today;
    const isShowDOB = showDOB ? moment(birthDate).format('DD') === moment(this.props.selectedDate).format('DD') : false;

    return (
      <div className={`${this.props.className} multi-member-button`}>
        <div className={`member-info ${isDisabled ? 'disabled' : ''}`}>
          <div className='icon-info'>
            <div className='avatar-icon'>
              {this.renderMemberPhoto(member)}
            </div>
          </div>
          <div className='middle-info' >
            <span className='name'>{memberName}</span>
            {this.notifyNumberOfClasses(totalClassesAttended)}
            {this.notifyCIRequired(CIRequired)}
            {this.notifyBirthday(isShowDOB, birthDate)}
            {this.notifyCrossRegional(isCrossRegional)}
            {this.notifyClassMilestone(totalClassesAttended + 1)}
            <p className='email'>{displayEmail}</p>
          </div>
          <div className='intro-member' />
          <div className='check-in checkbox-checkin'>{!isDisabled && <input
            id={id}
            type={'checkbox'}
            name={'checkbox'}
            checked={this.state.isSelect}
            onChange={this.handleCheck.bind(this)} />} {!isDisabled && <label htmlFor={id}> <span />
            </label>} </div>
        </div>
      </div >
    );
  }
}

MultiMemberButton.propTypes = {
  className: PropTypes.string,
  id: PropTypes.any,
  isClassAvailable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isSelect: PropTypes.bool,
  member: PropTypes.object,
  onClick: PropTypes.func,
  onUndoCheckIn: PropTypes.func,
  select: PropTypes.func
};

import React from 'react';
import PropTypes from 'prop-types';
import { intlShape, injectIntl } from 'react-intl';

import moment from 'moment';

class StartTime extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      today: new Date(),
      remainingTime: this.getRemainingTime(this.props.classStartTime)
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({
        remainingTime: this.getRemainingTime(this.props.classStartTime)
      });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  /**
   * Get remaining time of a class with start time
   * @param {String} classStartTime The class start time to be checked
   * @return {String}               The remaining time with format mm:ss
   */
  getRemainingTime(classStartTime) {
    const momentCurrentDateTime = moment();
    const momentStartDateTime = moment(classStartTime);
    return moment.utc(momentStartDateTime.diff(momentCurrentDateTime)).format('HH:mm:ss');
  }

  render() {
    const { intl, selectedDate } = this.props;

    const { remainingTime } = this.state;
    const timeToday = moment(this.state.today).format('YYYY-MM-DD');
    const selectedDatea = moment(selectedDate).format('YYYY-MM-DD');
    const momentStartDateTime = moment(this.props.classStartTime);
    const isAfterStartTime = moment(new Date()).isAfter(momentStartDateTime);
    let displayStartTime = moment(timeToday).isSame(moment(selectedDatea));
    return (
      !isAfterStartTime && displayStartTime ?
        (<div className='start-time'>
          <p className='time'>{remainingTime}</p>
          <p className='class-start-in'>{intl.formatMessage({ id: 'ClassTime.StartIn' })}</p>
        </div>) :
        (
          <div />
        )
    );
  }
}

StartTime.propTypes = {
  classStartTime: PropTypes.string,
  intl: intlShape.isRequired,
  selectedDate: PropTypes.any
};

export default injectIntl(StartTime);

import React, { Component } from 'react';
import moment from 'moment';
import { intlShape, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import { Helpers } from 'common';
import { Constants } from 'common';
import { CountMember, MemberButton, MultiMemberButton } from '..';
import './styles.scss';
class ListMemberCheckIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelectAll: false,
      selectedMembers: [],
      memberId: ''
    };
  }


  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      if (nextProps !== 'isBulkCheckIn') {
        this.setState({
          isSelectAll: false,
          selectedMembers: []
        });
      }
    }
    if ((nextProps.undoCheckIn && nextProps.undoCheckIn !== this.props.undoCheckIn
      && nextProps.undoCheckIn.toUpperCase() === 'SUCCESS')
      || (nextProps.checkedIn && nextProps.checkedIn !== this.props.checkedIn
        && nextProps.checkedIn.toUpperCase() === 'SUCCESS')) {
      this.setState({
        selectedMembers: []
      });
    }
    if (nextProps.members && nextProps.members.length > 0 && !_.isEqual(nextProps.members, this.props.members)) {
      let emails = [];
      nextProps.members.forEach((member) => {
        emails.push(member.email);
      });
    }
  }
  sortClassRoster(rosters) {
    if (rosters.length > 0) {
      rosters.sort(function (ros1, ros2) {
        // ignore upper and lowercase
        const firstName1 = ros1.firstName ? ros1.firstName.toLowerCase() : '';
        const firstName2 = ros2.firstName ? ros2.firstName.toLowerCase() : '';
        const lastName1 = ros1.lastName ? ros1.lastName.toLowerCase() : '';
        const lastName2 = ros2.lastName ? ros2.lastName.toLowerCase() : '';

        // Sort by firstName
        if (firstName1 < firstName2) {
          return -1;
        } else if (firstName1 > firstName2) {
          return 1;
        }

        // Compare lastName when first name the same
        if (lastName1 < lastName2) {
          return -1;
        } else if (lastName1 > lastName2) {
          return 1;
        }

        // names must be equal
        return 0;
      });
    }
    return rosters;
  }
  checkClassIsAvailabe(selectedClass) {
    if (selectedClass) {
      const periodTime = Helpers.getStartTimeDurationInMinutes(
        selectedClass.classStartDateTime
      );
      return (
        periodTime >= -Constants.LateCheckinPeriod &&
        periodTime <= Constants.PreCheckinPeriod
      );
    }
    return false;
  }

  async onMemberClick(member) {
    const email = member.email;
    const { classUUId } = this.props;
    const { selectedClass } = this.props;
    const mboVisitId = member. mboVisitId;
    const mboMemberId = member.memberUuid;
    await this.props.actions.checkIn_roster.updateRosterStatus(selectedClass, member);
    await this.props.actions.checkIn_roster.checkin(classUUId, mboMemberId, mboVisitId, email);
  }

  /**
   * undo checkin logic will be goes here
   */
  async undoCheckIn(member) {
    const { email } = member;
    const { selectedClass } = this.props;
    const { classUUId } = this.props;
    const mboVisitId = member. mboVisitId;
    const mboMemberId = member.memberUuid;
    await this.props.actions.checkIn_roster.updateRosterStatus(selectedClass, member);
    await this.props.actions.checkIn_roster.undoCheckin(
      classUUId,
      mboMemberId,
      mboVisitId,
      email
    );
  }

  /**
  * Handle Check or uncheck the check box in multiple member button
  * @param {*} status Checkbox's status
  * @param {*} member
  */
  selectMember(status, member) {
    let { selectedMembers } = this.state;
    if (status) {
      selectedMembers.push({
        mboMemberId: member.memberUuid,
        mboVisitId: member.mboVisitId,
        email: member.email
      });
    } else {
      selectedMembers = _.filter(selectedMembers, function (obj) {
        return obj.mboMemberId !== member.memberUuid;
      });
      this.setState({
        isSelectAll: false
      });
    }
    this.setState({
      selectedMembers
    });
  }


  /**
   * Handle check all member
   */
  selectAll() {
    let selectedMembers = [];
    const classRoster = this.sortClassRoster(
      this.props.members.filter(
        value =>
          value && (value.status === Constants.MemberStatus.Booked || value.status === 'false')
      )
    );

    classRoster.forEach(member => {
      selectedMembers.push({
        mboMemberId: member.memberUuid,
        mboVisitId: member.mboVisitId,
        email: member.email
      });
    });
    this.setState({
      isSelectAll: true,
      selectedMembers
    });
  }

  handleMultiCheckIn() {
    const { classUUId } = this.props;
    this.props.actions.checkIn_roster.multiCheckIn(
      classUUId,
      this.state.selectedMembers
    );
  }
  /**
   * The checkin button is enabled if classStartTimeDuration time in the checkin rules, otherwise checkin button is disabled
   * @param {*} classStartTime The start time of selected class
   */
  isAbleCheckIn(classStartTime) {
    const classStartTimeDuration = Helpers.getStartTimeDurationInMinutes(
      classStartTime
    );
    return (
      classStartTimeDuration >= -Constants.LateCheckinPeriod &&
      classStartTimeDuration <= Constants.PreCheckinPeriod &&
      this.state.selectedMembers.length > 0
    );
  }

  renderMemberList() {
    let classRosterRow = [];
    let classCheckInRow = [];
    let classRosterElement = [];
    let classCheckInElement = [];
    let checkedInMember = [];
    let classCancelElement = [];
    let classCancelRow = [];
    let checkInCancel = [];
    let classRoster = [];
    let isDisabled;

    if (this.props.selectedClass) {
      const momentCurrentDateTime = moment();
      const momentEndDateTime = moment(this.props.selectedClass.classStartDateTime).add(1, 'h');
      const visits = this.props.members;
      const prevSelected = this.props.selectedMember;
      if (prevSelected) {
        visits.map(member => {
          if (member.email === prevSelected.client.email.toLowerCase()) {
            member.status = prevSelected.signedIn;
          }
        });
      }

      isDisabled = momentEndDateTime.isBefore(momentCurrentDateTime, 'minutes');
      checkedInMember = this.sortClassRoster(this.props.members.filter((value) =>
        value && (value.status === Constants.MemberStatus.CheckedIn || value.status === 'true')));

      classRoster = this.sortClassRoster(this.props.members.filter((value) =>
        value && (value.status === Constants.MemberStatus.Booked || value.status === 'false')));

      checkInCancel = this.sortClassRoster(this.props.members.filter((value) => {
        return value.status === Constants.MemberStatus.LateCancelled;
      }));

      for (let i = 0; i < classRoster.length; i++) {
        classRosterElement.push(
          <MemberButton key={i} {...this.props} member={classRoster[i]}
            id={i}
            className='item-member'
            onUndoCheckIn={() => this.undoCheckIn.bind(this)(classRoster[i])}
            onClick={() => this.onMemberClick.bind(this)(classRoster[i])}
            selectedDate={this.props.selectedDate}
            isDisabled={isDisabled} />
        );
        if ((i + 1) % 2 === 0 || (i + 1) === classRoster.length) {
          classRosterRow.push(classRosterElement);
          classRosterElement = [];
        }
      }
    }
    for (let i = 0; i < checkedInMember.length; i++) {
      classCheckInElement.push(
        <MemberButton key={i} {...this.props} member={checkedInMember[i]}
          id={i}
          className='item-member'
          onUndoCheckIn={() => this.undoCheckIn.bind(this)(checkedInMember[i])}
          onClick={() => this.onMemberClick.bind(this)(checkedInMember[i])}
          selectedDate={this.props.selectedDate}
          isDisabled={isDisabled} />
      );

      if ((i + 1) % 2 === 0 || (i + 1) === checkedInMember.length) {
        classCheckInRow.push(classCheckInElement);
        classCheckInElement = [];
      }
    }

    for (let i = 0; i < checkInCancel.length; i++) {
      classCancelElement.push(
        <MemberButton key={i} {...this.props} member={checkInCancel[i]}
          id={i}
          className='item-member'
          isLateCancel={true}
          isDisabled={isDisabled} />
      );

      if ((i + 1) % 2 === 0 || (i + 1) === checkInCancel.length) {
        classCancelRow.push(classCancelElement);
        classCancelElement = [];
      }
    }

    return {
      classRosterRow,
      classCheckInRow,
      classRosterElement,
      classCancelRow,
      checkedInMember,
      classCancelElement,
      isDisabled
    };
  }

  renderMemberBulkCheckIn() {
    let { isSelectAll } = this.state;

    let memberBulkCheckIn = [];
    let memberRowBulkCheckIn = [];

    const isClassAvailable = this.checkClassIsAvailabe(
      this.props.selectedClass
    );
    const { bulkCheckedInMembers } = this.props;
    const prevSelected = this.props.selectedMember;
    const visits = this.props.members;

    if (prevSelected) {
      visits.map(member => {
        if (member.email === prevSelected.client.email.toLowerCase()) {
          member.status = prevSelected.signedIn;
        }
      });
    } else if (bulkCheckedInMembers) {
      visits.map(member => {
        const bulkMember = _.filter(
          bulkCheckedInMembers,
          m => m && m.client.email.toLowerCase() === member.email
        );
        if (bulkMember.length > 0) {
          member.status = bulkMember[0].signedIn;
        }
      });
    }
    if (this.props.selectedClass) {
      const momentEndDateTime = moment(this.props.selectedClass.classStartDateTime).add(1, 'h');
      const momentCurrentDateTime = moment();
      let isDisabled = momentEndDateTime.isBefore(momentCurrentDateTime, 'minutes');

      const classRosterMultiCheckIn = this.sortClassRoster(this.props.members.filter(
        (value) => value && value.status !== Constants.MemberStatus.LateCancelled
      ));

      for (let i = 0; i < classRosterMultiCheckIn.length; i++) {
        memberBulkCheckIn.push(
          <MultiMemberButton key={i} {...this.props} member={classRosterMultiCheckIn[i]}
            className='item-member'
            id={classRosterMultiCheckIn[i].memberUuid}
            isSelect={isSelectAll}
            isDisabled={!isClassAvailable || isDisabled || (classRosterMultiCheckIn[i].status === Constants.MemberStatus.CheckedIn || classRosterMultiCheckIn[i].status === 'true')}
            select={(status) => this.selectMember.bind(this)(status, classRosterMultiCheckIn[i])} />
        );
        if ((i + 1) % 2 === 0 || (i + 1) === classRosterMultiCheckIn.length) {
          memberRowBulkCheckIn.push(memberBulkCheckIn);
          memberBulkCheckIn = [];
        }
      }
    }

    return {
      memberRowBulkCheckIn
    };
  }

  render() {
    const { isBooked, isCheckIn, isAll, intl, selectedClass, selectedDate, isBulkCheckIn, members } = this.props;
    const {
      classRosterRow,
      checkedInMember,
      classCancelRow,
      classCheckInRow
    } = this.renderMemberList();
    const { memberRowBulkCheckIn } = this.renderMemberBulkCheckIn();
    let checkedInCount = members.filter((value) => value && (value.status === Constants.MemberStatus.CheckedIn || value.status === 'true')).length;
    const totalBooked = selectedClass ? selectedClass.membersTotal : 0;
    const checkInRemain = members.filter((value) => value &&
      (value.status === Constants.MemberStatus.Booked || value.status === 'false')).length;
    const checkLateCancelled = members.filter((value) => value && value.status === Constants.MemberStatus.LateCancelled).length
      || (selectedClass && selectedClass.lateCancelledCount);

    return (
      <React.Fragment>
        <CountMember
          checkedInCount={checkedInCount}
          totalBooked={totalBooked}
          checkInRemain={checkInRemain}
          checkLateCancelled={checkLateCancelled}
          selectedDate={selectedDate}
          selectedClass={selectedClass || {}} />
        {isBooked === 'isBooked' && <div className='list-member-checkin' id='style-scroll'>{classRosterRow}</div>}
        {isCheckIn === 'isCheckIn' && <div className='list-member-checkin' id='style-scroll'>{classCheckInRow}</div>}
        {isBulkCheckIn === 'isBulkCheckIn' &&
          <div className='list-member-bulk-checkin' id='style-scroll'>
            <div className='header-bulk-checkin'>
              <div className='title-bulk-chekin'><p>{'Bulk Check-In'}</p></div>
              <div className='select-bulk-checkin'>
                <button className='btn-select-all' onClick={this.selectAll.bind(this)}><p>{'Select All'}</p></button>
                <button className='btn-check-in'
                  onClick={this.handleMultiCheckIn.bind(this)}
                  disabled={!this.isAbleCheckIn.bind(this)(
                    _.get(selectedClass, 'classStartDateTime'))}><p>{'Check-In'}</p></button>
              </div>
            </div>
            {memberRowBulkCheckIn}
          </div>}
        {isAll === 'isAll' && <div className='list-member-checkin' id='style-scroll'>{classRosterRow}
          {checkedInMember && checkedInMember.length > 0 &&
            <div className='checked-in'>
              <div className='checked-in-text'>
                {intl.formatMessage({
                  id: 'ClassRoster.CheckedIn.Title'
                })}
              </div>
            </div>}
          {classCheckInRow && <div className='list-member-uncheckin'>{classCheckInRow}</div>}
          {classCancelRow && classCancelRow.length > 0 &&
            <div className='checked-in'>
              <div className='checked-in-text'>
                {intl.formatMessage({
                  id: 'ClassRoster.Cancel.Title'
                })}
              </div>
            </div>}
          {classCancelRow && <div className='list-member-late-cancel'>{classCancelRow}</div>}
        </div>}
      </React.Fragment>
    );
  }
}
ListMemberCheckIn.propTypes = {
  actions: PropTypes.any,
  autoNextTime: PropTypes.number,
  bulkCheckedInMembers: PropTypes.any,
  checkInRemain: PropTypes.any,
  checkLateCancelled: PropTypes.number,
  checkedIn: PropTypes.any,
  checkedInCount: PropTypes.any,
  classUUId: PropTypes.any,
  classes: PropTypes.array,
  currentStudio: PropTypes.object,
  intl: intlShape.isRequired,
  isAll: PropTypes.any,
  isBooked: PropTypes.any,
  isCheckIn: PropTypes.any,
  isUpsertSucessfully: PropTypes.bool,
  location: PropTypes.any,
  members: PropTypes.array,
  params: PropTypes.object,
  profile: PropTypes.object,
  routing: PropTypes.object,
  selectedClass: PropTypes.object,
  selectedDate: PropTypes.any,
  selectedMember: PropTypes.object,
  totalBooked: PropTypes.number,
  undoCheckIn: PropTypes.any
};
export default injectIntl(
  connect(state => ({
    classes: state.checkIn_classes.classes,
    selectedClass: state.checkIn_roster.selectedClass,
    selectedDate: state.checkIn_classes.selectedDate,
    selectedMember: state.checkIn_roster.selectedMember,
    bulkCheckedInMembers: state.checkIn_roster.bulkCheckedInMembers,
    currentStudio: state.auth_users.currentStudio,
    checkedIn: state.checkIn_roster.checkedIn,
    autoNextTime: state.checkIn_roster.autoNextTime,
    undoCheckIn: state.checkIn_roster.undoCheckIn,
    profile: state.checkIn_roster.profile,
    isUpsertSucessfully: state.checkIn_roster.isUpsertSucessfully,
    routing: state.routing
  }))(ListMemberCheckIn)
);

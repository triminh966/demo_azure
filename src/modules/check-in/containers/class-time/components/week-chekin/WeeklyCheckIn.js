import React, { Component } from 'react';
import moment from 'moment';
import './styles.scss';

export default class WeeklyCheckIn extends Component {
  constructor(props) {
    super(props);

    this.isActive = this.isActive.bind(this);
  }


  renderDays(dates) {
    let days = [];
    dates.map((day, i) => {
      days.push(
        <div
          className={`day ${this.isActive(day) ? 'active' : ''}`}
          key={i}>
          <span className='number' onClick={this.props.onDateClick.bind(day)}> {moment(day).format('DD')}</span>
        </div>
      );
    });

    return <div className='col-center'>{days}</div>;
  }

  isActive(day) {
    let currentDate = new Date();
    if ((moment(day).format('YYYY-MM-DD') === moment(currentDate).format('YYYY-MM-DD'))) {
      return true;
    }
    if ((moment(day).format('YYYY-MM-DD') === this.props.selectedDate)) {
      return true;
    }
    return false;
  }

  render() {
    const { dates } = this.props;
    let renderDays = this.renderDays(dates);
    return (
      <div className='weekly-checkin'>
        <div className='col col-start'>
          <span
            className='glyphicon glyphicon-chevron-left'
            onClick={this.props.prevMonth}/>
        </div>
        {renderDays}
        <div className='col col-end'>
          <span
            className='glyphicon glyphicon-chevron-right'
            onClick={this.props.nextMonth}/>
        </div>
      </div>
    );
  }
}

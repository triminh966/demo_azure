import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListMemberCheckIn from '../list-member-checkin/ListMemberCheckIn';
import { AddMemberForm } from '..';
import { ModalType } from 'modules/core/components';

import './styles.scss';
class ListDetailCheckIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBooked: '',
      isCheckIn: '',
      isBulkCheckIn: '',
      isAll: 'isAll'
    };
  }

  onChangeCheckBox = e => {
    this.setState({
      isBooked: '',
      isCheckIn: '',
      isAll: '',
      isBulkCheckIn: '',
      [e.target.name]: e.target.name
    });
  };

  addMember() {
    this.props.actions.core_modal.show({
      modalType: ModalType.Custom,
      size: 'lg',
      component: AddMemberForm,
      props: {
        actions: this.props.actions,
        classUUId: this.props.classUUId,
        classId: this.props.classId,
        mboClassId: this.props.mboClassId,
        mboStudioId: this.props.mboStudioId,
        studioId: this.props.studioId,
        currentTime: this.props.currentTime,
        selectedClass: this.props.selectedClass,
        selectedDate: this.props.selectedDate
      },
      headerClass: 'add-member-form-header',
      className: 'add-member-form-modal',
      onClose: () => {
        if (this.props.isAddMemberSuccessfully) {
          this.props.actions.checkIn_classes.getAllSchedule(
            this.props.currentStudio.StudioId,
            this.props.selectedDate
          );
          this.props.actions.checkIn_roster.selectClass({
            mboClassId: this.props.mboClassId,
            mboStudioId: this.props.mboStudioId,
            classId: this.props.classId
          });
        }
      }
    });
  }

  render() {
    const { isBooked, isCheckIn, isAll, isBulkCheckIn } = this.state;
    const { members, classUUId } = this.props;

    return (
      <React.Fragment>
        <div className='header-member'>
          <div className='bulk-checkin'>
            <li className='box'>
              <input
                id='bulk-checkin'
                type='checkbox'
                value={isBulkCheckIn}
                name='isBulkCheckIn'
                checked={isBulkCheckIn === 'isBulkCheckIn' ? true : false}
                onChange={this.onChangeCheckBox} />
              <label className='number' htmlFor='bulk-checkin'>
                {'Bulk Check-In'}
              </label>
            </li>
          </div>
          <div className='middle-member'>
            <li className='box'>
              <input
                id='check1'
                type='checkbox'
                value={isBooked}
                name='isBooked'
                checked={isBooked === 'isBooked' ? true : false}
                onChange={this.onChangeCheckBox} />
              <label className='number' htmlFor='check1'>
                {'Booked'}
              </label>
            </li>
            <li className='box'>
              <input
                id='check2'
                type='checkbox'
                value={isCheckIn}
                name='isCheckIn'
                checked={isCheckIn === 'isCheckIn' ? true : false}
                onChange={this.onChangeCheckBox} />
              <label className='number' htmlFor='check2'>
                {'Checked In'}
              </label>
            </li>
            <li className='box'>
              <input
                id='check3'
                type='checkbox'
                value={isAll}
                name='isAll'
                checked={isAll === 'isAll' ? true : false}
                onChange={this.onChangeCheckBox} />
              <label className='number' htmlFor='check3'>
                {'All'}
              </label>
            </li>
          </div>
          <div className='add-member'>
            <span
              className='plus-ic'
              onClick={this.addMember.bind(this)}/>
          </div>
        </div>
        <ListMemberCheckIn
          isAll={isAll}
          isCheckIn={isCheckIn}
          isBooked={isBooked}
          isBulkCheckIn={isBulkCheckIn}
          classUUId={classUUId}
          actions={this.props.actions}
          members={members} />
      </React.Fragment>
    );
  }
}
ListDetailCheckIn.propTypes = {
  actions: PropTypes.any,
  classId: PropTypes.string,
  classUUId: PropTypes.string,
  currentStudio: PropTypes.object,
  currentTime: PropTypes.string,
  isAddMemberSuccessfully: PropTypes.bool,
  mboClassId: PropTypes.string,
  mboStudioId: PropTypes.string,
  members: PropTypes.any,
  selectedClass: PropTypes.object,
  selectedDate: PropTypes.any,
  studioId: PropTypes.string
};
export default (
  connect(state => ({
    members: state.checkIn_roster.members,
    isAddMemberSuccessfully: state.checkIn_roster.isAddMemberSuccessfully
  }))(ListDetailCheckIn)
);


import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { intlShape, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { Constants } from 'common';

import {default as CalendarCheckIn} from './components/calendar-checkin/CalendarCheckIn';
import {default as ListDetailCheckIn} from './components/list-detail-checkin/ListDetailCheckIn';
import TimeButton from './components/time-button/TimeButton';
import { languageWithoutRegionCode as defaultLocale } from 'localization';

import './styles.scss';

export class ClassTime extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      beginTime: props.classes && props.classes.length > 0 ? moment(props.classes[0].StartTime).format('YYYY hh:mm a') : '',
      classUUId: props.classes && props.classes.length > 0 ? props.classes[0].ClassUUId : '',
      mboClassId: props.classes && props.classes.length > 0 ? props.classes[0].MBOClassId : '',
      mboStudioId: props.classes && props.classes.length > 0 ? props.classes[0].MBOStudioId : '',
      studioId: props.currentStudio.StudioId || '',
      classId: props.classes && props.classes.length > 0 ? props.classes[0].ClassId : '',
      coachId: props.classes && props.classes.length > 0 ? props.classes[0].CoachId : ''
    };
  }

  /* eslint-disable react/no-deprecated */
  componentWillMount() {
    let date = this.props.location.query.date;
    if (date) {
      this.setState({ selectedDate: moment(date, Constants.DefaultDayFormat).toDate() });
    }
  }
  componentDidMount() {
    try {
      let selectedDate = new Date();
      this.props.actions.checkIn_classes.getAllSchedule(
        this.props.currentStudio.StudioId,
        selectedDate
      );
      if (this.props.classes && this.props.classes.length > 0) {
        const classPayload = {
          mboStudioId: this.props.classes[0].MBOStudioId,
          mboClassId: this.props.classes[0].MBOClassId,
          classId: this.props.classes[0].ClassId,
          studioId: this.props.currentStudio.StudioId
        };
        this.props.actions.checkIn_roster.selectClass(classPayload);
      }
    } catch (error) {
      this.props.actions.core_alert.showError(
        error
      );
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.classes && nextProps.classes.length !== this.props.classes.length) {
      if (!nextProps.classes || _.isEqual(nextProps.classes, [])) {
        return;
      }
      let beginTime = nextProps.classes[0] ? moment(nextProps.classes[0].StartTime).format('YYYY hh:mm a') : moment().format('YYYY hh:mm a');
      let classUUId = nextProps.classes[0] ? nextProps.classes[0].ClassUUId : null;
      let mboClassId = nextProps.classes[0] ? nextProps.classes[0].MBOClassId : null;
      let mboStudioId = nextProps.classes[0] ? nextProps.classes[0].MBOStudioId : null;
      let classId = nextProps.classes[0] ? nextProps.classes[0].ClassId : null;
      let coachId = nextProps.classes[0] ? nextProps.classes[0].CoachId : null;
      this.setState({
        beginTime,
        mboClassId,
        classUUId,
        classId,
        coachId,
        mboStudioId
      }, () => {
        this.props.actions.checkIn_roster.selectClass({classId, mboClassId, mboStudioId});
      });
    }
  }

  selectTime(obj) {
    const beginTime = moment(obj.StartTime).format('YYYY hh:mm a');
    const coachId = obj.CoachId;
    this.setState({
      beginTime,
      coachId,
      mboClassId: obj.MBOClassId,
      classUUId: obj.ClassUUId,
      mboStudioId: obj.MBOStudioId,
      classId: obj.ClassId
    }, () => {
      this.props.actions.checkIn_roster.selectClass({
        mboClassId: obj.MBOClassId,
        classId: obj.ClassId,
        mboStudioId: obj.MBOStudioId
      });
      this.props.actions.checkIn_roster.clearSelectedMember();
    });
  }
  dataToggle() {
    const isExpand = $('.container-checkin').hasClass('show-left-checkin');
    if (isExpand) {
      $('.container-checkin').addClass('hidden-left-checkin').removeClass('show-left-checkin');
      $('#btnShow').removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-right');
    }
    else {
      $('.container-checkin').addClass('show-left-checkin').removeClass('hidden-left-checkin');
      $('#btnShow').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-left');
    }
  }


  render() {
    let classTime = this.props.classes || [];
    const { selectedClass } = this.props;
    let timeButton;
    let classTimeAvailable = _.filter(classTime, function (ct) {
      return !ct.isCancelled;
    });
    if (!selectedClass) {
      timeButton = <div/>;
    }
    timeButton = (classTimeAvailable || []).map((v, i) => {
      let active = (moment(this.state.beginTime).isSame(moment(v.StartTime).format('YYYY hh:mm a')) && (this.state.coachId === v.CoachId)) ? 'active' : '';
      return (
        <div className='td-time' key={`${classTimeAvailable[i].ClassId}_col `}>
          <TimeButton value={classTimeAvailable[i].ClassId}
            time={moment(classTimeAvailable[i].StartTime).locale(defaultLocale).format('LT')}
            handleClick={this.selectTime.bind(this, classTimeAvailable[i])}
            name={classTimeAvailable[i].ClassName} end={classTimeAvailable[i].EndTime}
            coach={classTimeAvailable[i].Coach}
            totalBooked={classTimeAvailable[i].TotalBooked}
            totalMembers={classTimeAvailable[i].TotalMembers}
            totalIntroCount={classTimeAvailable[i].TotalIntroCount}
            active={active} />
        </div>
      );
    });

    return (
      <div className='container-checkin show-left-checkin'>
        <div className='left-checkin'>
          <div className='wrapper'>
            <div className='tag-div'>
              <div className='toggle'>
                <button type='button' className='btn-icon' onClick={this.dataToggle}>
                  <span id='btnShow' className='icon glyphicon glyphicon-chevron-left' />
                </button>
              </div>
            </div>
            <CalendarCheckIn actions={this.props.actions} />
            <div className='time-input'>
              <div className='scrollbar' id='style-4'>
                {timeButton}
              </div>
            </div>
          </div>
        </div>
        <div className='right-checkin'>
          <div className='wrapper-right'>
            <ListDetailCheckIn
              classUUId={this.state.classUUId}
              classId={this.state.classId}
              studioId={this.state.studioId}
              mboClassId={this.state.mboClassId}
              mboStudioId={this.state.mboStudioId}
              currentTime={moment(this.state.beginTime).format('hh:mm a')}
              {...this.props}/>
          </div>
        </div>
      </div>
    );
  }
}

ClassTime.propTypes = {
  actions: PropTypes.any,
  classes: PropTypes.any,
  currentStudio: PropTypes.object,
  intl: intlShape.isRequired,
  isRosterActive: PropTypes.bool,
  location: PropTypes.any,
  routing: PropTypes.any,
  selectedClass: PropTypes.object,
  selectedDate: PropTypes.any
};

export default injectIntl(connect(state => ({
  classes: state.checkIn_classes.classes,
  selectedDate: state.checkIn_classes.selectedDate,
  errors: state.checkIn_classes.errors,
  currentStudio: state.auth_users.currentStudio,
  isRosterActive: state.checkIn_classes.isRosterActive,
  selectedClass: state.checkIn_roster.selectedClass
}))(ClassTime));

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { intlShape, injectIntl } from 'react-intl';
import { FormSelect } from 'modules/core/components';
import { Constants } from 'common';
import './styles.scss';

export class LanguageSetting extends React.Component {
  constructor(props) {
    super(props);
  }

  handleChange(e) {
    const { actions, onSelectLanguage } = this.props;
    actions.core_language.changeLocale(e.value);
    if (typeof onSelectLanguage === 'function') {
      onSelectLanguage(e.value);
    }
    const checkClass = $('.wrapper-center-login').hasClass('wrapper-language');
    if (checkClass) {
      $('.wrapper-center-login').removeClass('wrapper-language');
    }
  }
  render() {
    const { actions, locale, intl } = this.props;

    return (
      <div className='language-setting-wrapper'>
        <p className='label-page'>
          {intl.formatMessage({ id: 'Login.SelectLanguage' })}
        </p>
        <div className='dropdown'>
          <FormSelect
            async={false}
            className='select-language'
            searchable={false}
            options={Constants.languageConfig.default.languages}
            value={locale}
            onChange={this.handleChange.bind(this)}
          />
        </div>
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
LanguageSetting.propTypes = {
  actions: PropTypes.any,
  intl: intlShape.isRequired,
  locale: PropTypes.string
};

const languageSetting = connect(state => ({
  locale: state.core_language.locale
}))(LanguageSetting);

export default injectIntl(languageSetting);

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { AppConstants } from 'common';
import { Header } from '../../components';

import './styles.scss';

class Container extends React.Component {
  constructor(props) {
    super(props);
  }

  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(newProps) {
    if (newProps.currentUser && newProps.currentUser !== this.props.currentUser) {
      const { actions } = this.props;
      actions.routing.navigateTo(AppConstants.defaultPagePath.homePage);
    }
  }

  render() {
    return (
      <div id='studioView' className='app-background'>
        {
          (this.props.currentUser) &&
          (
            <div>
              <Header actions={this.props.actions} />
            </div>
          )
        }
        <div className={`app-container ${this.props.currentUser ? 'auth' : ''}`}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
Container.propTypes = {
  actions: PropTypes.any,
  children: PropTypes.any,
  currentUser: PropTypes.any
};


export default connect(state => ({
  currentUser: state.auth_users.currentUser
}))(Container);

import React from 'react';
import { hashHistory } from 'react-router';

import { BackIcon } from 'modules/shared/images';
import './styles.scss';

export class Back extends React.Component {
  constructor(props) {
    super(props);
  }

  goBack() {
    hashHistory.goBack();
  }

  render() {
    return (
      <div className='back-button' onClick={this.goBack}>
        <img src={BackIcon} />
      </div>
    );
  }
}

export default Back;

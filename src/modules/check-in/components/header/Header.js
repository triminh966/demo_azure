import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { intlShape, injectIntl } from 'react-intl';

import { AppConstants } from 'common';
import Home from '../home-button/Home';
import { Clock } from '../clock/Clock';
import { Menu } from '../menu/Menu';
import Back from '../back-button/Back';

import { LogoHome, ArrowProfileIcon, ExpandIcon, CollapseIcon } from 'modules/shared/images';

import './styles.scss';

export class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillUnmount() {
    this.props.actions.auth_users.destroyStudio();
  }
  /**
   * Switch screen state
   * @param {bool} isFullscreen
   */
  onChangeScreen(isFullscreen) {
    if (isFullscreen) {
      this.props.actions.core_screen.collapse();
    } else {
      this.props.actions.core_screen.expand();
    }
  }
  renderAppName() {
    if (this.props.routing.pathname.includes('check-in')) {
      return AppConstants.appNameCheckIn;
    } else if (this.props.routing.pathname.includes('client')) {
      return AppConstants.appNameCI;
    }
    return AppConstants.appNameProduct;
  }
  render() {
    const isBack = this.props.routing && this.props.routing.pathname !== '/product-selection/home' && this.props.routing.pathname !== '/check-in/class-time' ? <Back /> : '';
    const headerBtn = this.props.routing && (this.props.routing.pathname !== AppConstants.defaultPagePath.homePage)
      ? <div>{isBack}<Home actions={this.props.actions} /></div>
      : '';
    const appName = this.renderAppName();
    const { isFullscreen, intl } = this.props;
    if (!this.props.studio) {
      return '';
    }
    return (
      <div className='header'>
        <div className='left'>
          {headerBtn}
          <div className='logo-challenge'>
            <img src={LogoHome} />
            <div className='title'>{this.props.routing.pathname !== AppConstants.defaultPagePath.homePage ? appName : ''}</div>
          </div>
        </div>
        <div className='right'>
          <Clock />
          <div className='seperator-line' />
          <div className='dropdown studio-menu'>
            <a className='current-studio'>
              <span className='studio-name'>{this.props.studio.StudioName || ''}</span>
              <span className='studio-number'>{'#'}{this.props.studio.StudioNumber || ''}</span>
            </a>

            <img className='icon-screen' src={isFullscreen ? CollapseIcon : ExpandIcon}
              onClick={() => this.onChangeScreen.bind(this)(isFullscreen)} />

            <a data-toggle='dropdown'>
              <img src={ArrowProfileIcon} />
            </a>
            <Menu actions={this.props.actions} intl={intl} />
          </div>
        </div>
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
Header.propTypes = {
  actions: PropTypes.any,
  intl: intlShape.isRequired,
  isFullscreen: PropTypes.bool,
  routing: PropTypes.object,
  studio: PropTypes.object
};

export default connect(state => ({
  routing: state.routing.locationBeforeTransitions,
  studio: state.auth_users.currentStudio,
  isFullscreen: state.core_screen.isExpand
}))(injectIntl(Header));

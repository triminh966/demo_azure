/**
 * Define list of menu item for profile dropdown menu on Header component
 */
export const MenuItems = [
  {
    name: 'Settings',
    i18nKey: 'Menu.Studio.Settings',
    className: 'settings',
    subItem: [
      {
        name: 'Languages',
        i18nKey: 'Menu.Studio.Setting.Languages',
        to: '/check-in/settings/languages',
        className: ''
      },
      {
        name: 'Change Password',
        i18nKey: 'Menu.Studio.Change.Password',
        funcName: 'changePassword',
        className: ''
      },
      {
        name: 'Manage Announcements',
        i18nKey: 'Menu.Studio.Manage.Annoucements',
        to: '/manage-announcements',
        className: ''
      }
    ]
  },
  {
    name: 'Logout',
    i18nKey: 'Menu.Studio.Logout',
    funcName: 'logout',
    className: 'log-out'
  }
];

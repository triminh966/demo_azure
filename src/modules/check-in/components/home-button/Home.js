import React from 'react';
import { AppConstants } from 'common';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { HomeIcon } from 'modules/shared/images';
import './styles.scss';

export class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  goHome() {
    this.props.actions.routing.navigateTo(AppConstants.defaultPagePath.homePage);
  }

  render() {
    return (
      <div className='home-button' onClick={this.goHome.bind(this)}>
        <img src={HomeIcon} />
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
Home.propTypes = {
  actions: PropTypes.any,
  selectedDate: PropTypes.any
};

export default connect(state => ({
  selectedDate: state.checkIn_classes.selectedDate
}))(Home);

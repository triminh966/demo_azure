import React from 'react';
import moment from 'moment';

import './styles.scss';

export class Clock extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      // TBD
      // currentTime: moment(new Date()).format('LT')
      currentTime: new Date()
    };
  }

  componentDidMount() {
    const that = this;

    this.interval = setInterval(() => {
      that.setState({
        // TBD
        // currentTime: moment(new Date()).format('LT')
        currentTime: new Date()
      }, ()=>{
        if (moment(that.state.currentTime).format('h:mm:ss A') === '2:00:00 AM') {
          window.location.reload(true);
        }
      });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { currentTime } = this.state;

    return (
      <div className='header-clock'>
        {`${moment(currentTime).format('h:mm A')} `}
      </div>
    );
  }
}

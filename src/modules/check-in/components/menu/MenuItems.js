/**
 * Define list of menu item for profile dropdown menu on Header component
 */
export const MenuItems = [
  {
    name: 'Settings',
    i18nKey: 'Menu.Studio.Settings',
    key: 'settings',
    className: 'settings',
    childMenus: [
      {
        name: 'Languages',
        i18nKey: 'Menu.Studio.Setting.Languages',
        key: 'languages',
        to: '/check-in/settings/languages'
      },
      {
        name: 'Change Password',
        i18nKey: 'Menu.Studio.Change.Password',
        funcName: 'changePassword',
        key: 'changePassword',
        className: ''
      }
      // The functionality is not built out yet we do not want the UI in the first release.
      // {
      //   name: 'Manage Announcements',
      //   i18nKey: 'Menu.Studio.Manage.Annoucements',
      //   to: '/manage-announcements',
      //   key: 'manageAnnouncements',
      //   className: ''
      // }
    ]
  },
  {
    name: 'Logout',
    i18nKey: 'Menu.Studio.Logout',
    key: 'logout',
    funcName: 'logout',
    className: 'log-out'
  }
];

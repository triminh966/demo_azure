import { makeRoutes } from './config/routes';

import * as ClassRedux from './config/redux/class';
import * as RosterRedux from './config/redux/roster';

const CheckInModule = {
  makeRoutes: makeRoutes,
  redux: {
    classes: ClassRedux,
    roster: RosterRedux
  }
};

export default CheckInModule;

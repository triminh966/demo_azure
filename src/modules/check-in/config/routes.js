import React from 'react';
import { Route } from 'react-router';

import { NotFound, Unauthorized } from 'modules/core/containers';
import { CognitoService } from 'services';

import { Container, LanguageSetting } from '../containers';
import ClassTime from '../containers/class-time/ClassTime';

export const makeRoutes = (getState) => {
  const requireAuth = (nextState, replace, callback) => {
    const { auth_users } = getState();
    const { currentUser } = auth_users;

    if (!currentUser) {
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
      return callback();
    } else if (!CognitoService.getCurrentUser()) {
      CognitoService.refreshToken().then(result => {
        let currentToken = localStorage.getItem('session_token');
        if (result !== currentToken) {
          localStorage.setItem('session_token', result);
        }
        // Re-authorize the route after refresh token
        replace({
          pathname: '/login',
          state: { nextPathname: nextState.location.pathname }
        });

        return callback();
      }).catch(() => { callback(); });
    }
    return callback();
  };
  /**
   * Return route define
   */
  return (
    <Route key='check-in' path='/check-in' component={Container}>
      <Route path='class-time' onEnter={requireAuth} component={ClassTime} />
      <Route onEnter={requireAuth} key='language-setting' path='settings/languages'
        component={LanguageSetting} />
      <Route path='unauthorized' component={Unauthorized} status={401} />
      { /* Catch all route */}
      <Route path='*' component={NotFound} status={404} />
    </Route>
  );
};


import { createConstants, createReducer } from 'redux-module-builder';
import moment from 'moment';
import * as _ from 'lodash';

import { ClassAPI, MemberAPI } from 'services';
import { errorHandlerConstants, actions as errorAction} from 'modules/core/config/redux/error-handler';
import { messages } from 'localization';

export const types = createConstants('roster')(
  'SELECTED_CLASS',
  'CLEAR_SELECTED_CLASS',
  'MEMBER_CHECK_IN',
  'MEMBER_BULK_CHECK_IN',
  'CLEAR_MEMBER_CHECK_IN',
  'GET_LATEST_VISIT_FOR_MEMBER',
  'CLEAR_LATEST_VISIT_FOR_MEMBER',
  'UPDATE_MEMBER_PROFILE',
  'CLEAR_SELECTED_PROFILE',
  'GET_MEMBER_PROFILE',
  'SKIP_UPDATE_PROFILE',
  'MEMBER_UNDO_CHECK_IN',
  'CLEAR_MEMBER_UNDO_CHECK_IN',
  'SELECT_MEMBER',
  'CLEAR_SELECTED_MEMBER',
  'ERROR_GET_DATA',
  'GET_TERMS_CONDITIONS_ROSTER',
  'RESET_UPSERT',
  'UPSERT_SUCCESSFULLY',
  'GET_MEMBERSHIP_TYPES',
  'CLEAR_MEMBERSHIP_TYPES',
  'ADD_MEMBER_SUCCESSFULLY',
  'CLEAR_STATUS_ADD_MEMBER'
);

export function handleError(err) {
  return {
    type: errorHandlerConstants.HANDLE_ERROR,
    payload: { error: err }
  };
}

export const actions = {
  /**
   * Get all class roster for a specify class
   * @param {String}  classUUId The class uuid to be requested
   */
  selectClass: (classPayload) => async (dispatch) => {
    return ClassAPI.getClassRoster(classPayload)
      .then((result) => {
        let data = {...result.data, classStartDateTime: result.data.classStartDateTime.split('+')[0]};
        dispatch({
          type: types.SELECTED_CLASS,
          payload: data
        });
      }).catch((err) => {
        if (_.get(err, 'response.data.message') && err.response.data.message.includes('ClassID') && err.response.data.message.includes('does not exist.')) {
          err.response.data.message = messages[moment.locale()]['ClassTime.Error.IsCanceled'];
        }
        dispatch(handleError(err));
        dispatch({
          type: types.CLEAR_SELECTED_CLASS
        });
      });
  },

  /**
   * Update roster status accoding to new roster
   * @param {String}  currentSelectedClass The current class entity contains roster to be updated
   * @param {String}  roster The roster to be updated
   */
  updateRosterStatus: (currentSelectedClass, roster) => (dispatch) => {
    if (currentSelectedClass.visits && roster) {
      const currentRoster = currentSelectedClass.visits.find(item => item.id === roster.id);
      if (currentRoster) {
        currentRoster.status = roster.signedIn;
        dispatch({
          type: types.SELECTED_CLASS,
          payload: currentSelectedClass
        });
      }
    }
  },

  /**
   * Clear seleted class in redux
   */
  clearSelectedClass: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_SELECTED_CLASS
    });
  },

  /**
   * Checkin a member into a class
   * @param {String}  classUUId   The class uuid that member will checkin
   * @param {Number}  mboMemberId The mbo member id will be checked in
   * @param {Number}  mboVivistId The mbo visit id to be checked in
   * @param {String}  email       The member email
   */
  checkin: (classUUId, mboMemberId, mboVisitId, email) => (dispatch) => {
    dispatch({ type: types.CLEAR_MEMBER_CHECK_IN });
    return ClassAPI.checkin(classUUId, mboMemberId, mboVisitId, email)
      .then((result) => {
        dispatch({
          type: types.MEMBER_CHECK_IN,
          payload: result.data
        });
      }).catch((err) => {
        dispatch(handleError(err));
        dispatch({ type: types.CLEAR_MEMBER_CHECK_IN });
      });
  },
  multiCheckIn: (classUUId, members) => (dispatch) => {
    dispatch({ type: types.CLEAR_MEMBER_CHECK_IN });
    return ClassAPI.multiCheckIn(classUUId, members)
      .then((result) => {
        dispatch({
          type: types.MEMBER_BULK_CHECK_IN,
          payload: result.data
        });
      }).catch((err)=>{
        dispatch(handleError(err));
      });
  },
  /**
   * Clear checkin data in redux stored
   */
  clearCheckIn: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_MEMBER_CHECK_IN
    });
  },
  /**
   * Add member to class
   */
  addMember: (classId, payload) => (dispatch) => {
    return ClassAPI.addMember(classId, payload)
      .then(() => {
        dispatch({
          type: types.ADD_MEMBER_SUCCESSFULLY
        });
      }).catch((err) => {
        dispatch(errorAction.handleError(err));
      });
  },
  /**
   * Reset status after add member successfully
   */
  clearStatusAddMember: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_STATUS_ADD_MEMBER
    });
  },
  /**
   * Undo checkin action
   * @param {String}  classUUId   The class uuid that member will checkin
   * @param {Number}  mboMemberId The mbo member id will be checked in
   * @param {Number}  mboVivistId The mbo visit id to be checked in
   * @param {String}  email       The member email
   */
  undoCheckin: (classUUId, mboMemberId, mboVisitId, email) => (dispatch) => {
    dispatch({
      type: types.CLEAR_MEMBER_UNDO_CHECK_IN
    });
    return ClassAPI.undoCheckin(classUUId, mboMemberId, mboVisitId, email)
      .then((result) => {
        dispatch({
          type: types.MEMBER_UNDO_CHECK_IN,
          payload: result.data
        });
      }).catch((err) => {
        dispatch(handleError(err));
        dispatch({
          type: types.CLEAR_MEMBER_UNDO_CHECK_IN
        });
      });
  },

  /**
   * Get latest vist for a specify member and studioId
   * @param {String}  studioUUid The studioUUId to be requested
   * @param {Number}  mboMemberId The mbo member id to be requested
   */
  getLatestVisitForMember: (studioUUid, mboMemberId) => (dispatch) => {
    return ClassAPI.getLatestVisitForMember(studioUUid, mboMemberId)
      .then((result) => {
        dispatch({
          type: types.GET_LATEST_VISIT_FOR_MEMBER,
          payload: result.data.data
        });
      }).catch((err) => {
        dispatch(handleError(err));
        dispatch({ type: types.CLEAR_LATEST_VISIT_FOR_MEMBER });
      });
  },

  /**
   * Clear latestVisit in redux
   */
  clearLatestVisitForMember: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_LATEST_VISIT_FOR_MEMBER
    });
  },

  /**
   * Clear seleted profile in redux
   */
  clearSelectedProfile: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_SELECTED_PROFILE
    });
  },
  /**
   * Get member profile by email
   */
  getMemberProfileByEmail: (email) => (dispatch) => {
    return MemberAPI.getMemberProfileByEmail(email)
      .then((result) => {
        dispatch({
          type: types.GET_MEMBER_PROFILE,
          payload: result.data
        });
      }).catch((err) => {
        dispatch(handleError(err));
      });
  },
  /**
   * Select member
   */
  selectMember: (member) => (dispatch) => {
    dispatch({
      type: types.SELECT_MEMBER,
      payload: member
    });
  },
  /**
   * Clear selected member
   */
  clearSelectedMember: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_SELECTED_MEMBER
    });
  },

  getMembershipTypes: (memberUUId) =>(dispatch) =>{
    return MemberAPI.getMembershipTypes(memberUUId).then((result) =>{
      if (result) {
        dispatch({
          type: types.GET_MEMBERSHIP_TYPES,
          payload: result.data
        });
      }
    }, err =>{
      dispatch({
        type: types.ERROR_GET_DATA,
        payload: err
      });
    });
  },
  clearMemberTypes: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_MEMBERSHIP_TYPES
    });
  }
};

export const reducer = createReducer({
  [types.SELECTED_CLASS]: (state, { payload }) => {
    return {
      ...state,
      selectedClass: payload,
      checkedIn: '',
      members: payload.members || []
    };
  },
  [types.CLEAR_SELECTED_CLASS]: (state) => {
    return {
      ...state,
      selectedClass: undefined,
      members: []
    };
  },
  [types.MEMBER_CHECK_IN]: (state, { payload }) => {
    const { data, code } = payload;
    return {
      ...state,
      checkedIn: code,
      selectedMember: data.visit
    };
  },
  [types.MEMBER_BULK_CHECK_IN]: (state, { payload }) => {
    const { data, code } = payload;
    let bulkCheckedIn = [];
    let selectedMember = {};
    if (data && _.isArray(data) && data.length > 0) {
      data.map(dt => {
        bulkCheckedIn.push(dt);
      });
      selectedMember = undefined;
    } else {
      selectedMember = data;
    }

    return {
      ...state,
      checkedIn: code,
      selectedMember,
      bulkCheckedInMembers: bulkCheckedIn
    };
  },
  [types.CLEAR_MEMBER_CHECK_IN]: (state) => {
    return {
      ...state,
      checkedIn: ''
    };
  },
  [types.GET_LATEST_VISIT_FOR_MEMBER]: (state, { payload }) => {
    return {
      ...state,
      latestVisit: payload
    };
  },
  [types.CLEAR_LATEST_VISIT_FOR_MEMBER]: (state) => {
    return {
      ...state,
      latestVisit: undefined
    };
  },

  [types.CLEAR_SELECTED_PROFILE]: (state) => {
    return {
      ...state,
      profile: undefined
    };
  },
  [types.GET_MEMBER_PROFILE]: (state, { payload }) => {
    return {
      ...state,
      profile: payload.data,
      isStatus: payload.code
    };
  },
  [types.MEMBER_UNDO_CHECK_IN]: (state, { payload }) => {
    return {
      ...state,
      undoCheckIn: payload.data.status,
      selectedMember: payload.data.visit
    };
  },
  [types.CLEAR_MEMBER_UNDO_CHECK_IN]: (state) => {
    return {
      ...state,
      undoCheckIn: ''
    };
  },
  [types.SELECT_MEMBER]: (state, { payload }) => {
    return {
      ...state,
      selectedMember: payload
    };
  },
  [types.CLEAR_SELECTED_MEMBER]: (state) => {
    return {
      ...state,
      selectedMember: undefined,
      bulkCheckedInMembers: undefined
    };
  },

  [types.ERROR_GET_DATA]: (state, {
    payload
  }) => {
    return {
      ...state,
      errors: payload
    };
  },
  [types.GET_TERMS_CONDITIONS_ROSTER]: (state, { payload }) => {
    return {
      ...state,
      rosterTermsConditions: payload
    };
  },
  [types.RESET_UPSERT]: (state) => {
    return {
      ...state,
      isUpsertSucessfully: false
    };
  },
  [types.UPSERT_SUCCESSFULLY]: (state) => {
    return {
      ...state,
      isUpsertSucessfully: true
    };
  },
  [types.GET_MEMBERSHIP_TYPES]: (state, { payload }) => {
    return {
      ...state,
      selectedMemberTypes: payload
    };
  },
  [types.CLEAR_MEMBERSHIP_TYPES]: (state) => {
    return {
      ...state,
      isStatus: undefined,
      selectedMemberTypes: {}
    };
  },
  [types.ADD_MEMBER_SUCCESSFULLY]: (state) => {
    return {
      ...state,
      isAddMemberSuccessfully: true
    };
  },
  [types.CLEAR_STATUS_ADD_MEMBER]: (state) => {
    return {
      ...state,
      isAddMemberSuccessfully: false
    };
  }
});

/*
 * The initial state for this part of the component tree
 */
export const initialState = {
  selectedClass: undefined,
  autoNextTime: -1,
  profile: undefined,
  undoCheckIn: '',
  checkedIn: '',
  selectedMember: undefined,
  members: [],
  rosterTermsConditions: {},
  isUpsertSucessfully: false,
  selectedMemberTypes: {},
  isStatus: undefined,
  isAddMemberSuccessfully: false
};

import { createConstants, createReducer } from 'redux-module-builder';

import { ClassAPI } from 'services';
import { Constants } from 'common';

export const types = createConstants('class')('GET_ALL_AVAILABLE_CLASS', 'ERROR_GET_DATA', 'SELECT_DATE', 'ACTIVE_ROSTER');
export const reducerKey = 'checkIn_classes';

export const actions = {
  getAllSchedule: (studioNumber, date) => (dispatch) => {
    ClassAPI.getAvailableClass(studioNumber, date).then(res => {
      if (res) {
        dispatch({
          type: types.GET_ALL_AVAILABLE_CLASS,
          payload: res.data
        });
      }
    }, err => {
      dispatch({
        type: types.ERROR_GET_DATA,
        payload: err
      });
    });
  },
  selectDate: (date) => (dispatch) => {
    localStorage.setItem(Constants.SelectedDateKey, JSON.stringify(date));
    dispatch({
      type: types.SELECT_DATE,
      payload: date
    });
  },
  updateActiveRoster: (bool) => (dispatch) => {
    dispatch({
      type: types.ACTIVE_ROSTER,
      payload: bool
    });
  },
  clearClassSchedule: () => (dispatch) => {
    dispatch({
      type: types.CLEAR_CLASS_SCHEDULE
    });
  }
};

export const reducer = createReducer({
  [types.GET_ALL_AVAILABLE_CLASS]: (state, { payload }) => {
    return {
      ...state,
      classes: payload,
      updated: false,
      errors: undefined
    };
  },
  [types.ERROR_GET_DATA]: (state, { payload }) => {
    return {
      ...state,
      classes: [],
      members: [],
      errors: payload
    };
  },
  [types.SELECT_DATE]: (state, { payload }) => {
    return {
      ...state,
      selectedDate: payload
    };
  },
  [types.ACTIVE_ROSTER]: (state, { payload }) => {
    return {
      ...state,
      isRosterActive: payload
    };
  }
});

/*
 * The initial state for this part of the component tree
 */
export const initialState = {
  loading: false,
  errors: undefined,
  classes: [],
  members: [],
  selectedDate: undefined,
  isRosterActive: false
};

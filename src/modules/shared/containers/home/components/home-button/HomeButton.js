import React from 'react';
import PropTypes from 'prop-types';

import './styles';

export default class HomeButton extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={`home-btn ${this.props.className}`}>
        <img className={`home-ic ${this.props.classNameIcon || ''}`} src={this.props.icon}/>
        <div className='orange-btn' onClick={() => this.props.handleClick()}>
          {this.props.name}
        </div>
      </div>
    );
  }
}

HomeButton.propTypes = {
  className: PropTypes.string,
  classNameIcon: PropTypes.string,
  handleClick: PropTypes.func,
  icon: PropTypes.any,
  name: PropTypes.string
}
;

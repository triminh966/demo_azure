import React from 'react';
import PropTypes from 'prop-types';
import { intlShape, injectIntl } from 'react-intl';
import { connect } from 'react-redux';

import {IconCheck, IconWearable, ReciepIcon} from 'modules/shared/images';
import {HomeButton} from './components';

import './styles.scss';

export class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.validateTimeSetup();
  }

  /**
   * Validate studio time and client computer's time setup
   */
  validateTimeSetup() {
    const { currentStudio, intl } = this.props;
    /* eslint-disable */
    const clientTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    /* eslint-enable */
    let message;
    if (currentStudio) {
      if (!currentStudio.TimeZone) {
        message = intl.formatMessage({ id: 'General.StudioTimeZoneDidNotSetup' });
      } else if (currentStudio.TimeZone !== clientTimeZone) {
        message = intl.formatMessage({ id: 'General.TimeSetupDoesNotMatch' }, { timeZone: currentStudio.TimeZone });
      }
    }


    if (message) {
      setTimeout(() => {
        this.props.actions.core_alert.showError(
          message
        );
      }, 1000);
    }
  }

  render() {
    const isCIEnabled = (this.props.currentStudio || {}).IsCIEnabled;
    return (
      <div className={`home-wrapper${isCIEnabled ? ' ci-enabled' : ''}`}>
        <HomeButton name='PRODUCT SELECTION' icon={IconWearable} handleClick={() => this.props.router.push('/product-selection/home')}
          className={'product'}/>
        <HomeButton name='CLIENT INTAKE' icon={ReciepIcon} handleClick={() => this.props.router.push('/client-take/member-search')}
          classNameIcon='client-take-ic' className={`ci${isCIEnabled ? ' enabled' : ''}`}/>
        <HomeButton name='CHECK IN' icon={IconCheck} handleClick={() => this.props.router.push('/check-in/class-time')}
          classNameIcon='check-ic' className='check-in'/>
      </div>
    );
  }
}

Home.propTypes = {
  actions: PropTypes.any,
  currentStudio: PropTypes.object,
  intl: intlShape.isRequired,
  router: PropTypes.object
};

export default injectIntl(connect(state => ({
  currentStudio: state.auth_users.currentStudio
}))(Home));

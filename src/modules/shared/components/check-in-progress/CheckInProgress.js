import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import { intlShape, injectIntl } from 'react-intl';

import { Helpers } from 'common';

// http://tauday.com/tau-manifesto
const tau = 2 * Math.PI;

/**
 * Using D3 for this customization
 * Refer: http://bl.ocks.org/mbostock/5100636
 * API documentation: https://github.com/d3/d3/blob/master/API.md
 */
export class CheckInProgress extends React.Component {
  constructor(props) {
    super(props);
    this.id = Helpers.guid();
  }

  componentDidMount() {
    this.configureChart();
  }

  /* eslint-disable react/no-deprecated */
  componentWillUpdate(nextProps) {
    const { angle, totalBooked } = this.props;
    if (angle !== nextProps.angle || totalBooked !== nextProps.totalBooked) {
      if (this.totalBookedText) {
        this.totalBookedText.text(`${parseFloat(nextProps.angle * 100).toFixed(0)}%`);
        this.foreground = this.foreground.datum({ endAngle: 0 });
      }

      // Returns a tween for a transition’s "d" attribute, transitioning any selected
      // arcs from their current angle to the specified new angle.
      this.performTransition(nextProps.angle, nextProps.totalBooked);
    }
  }

  performTransition(angle, totalBooked) {
    // Returns a tween for a transition’s "d" attribute, transitioning any selected
    // arcs from their current angle to the specified new angle.
    this.foreground.transition()
      .duration(750)
      .attrTween('d', (() => {
        return (d) => {
          let interpolate = d3.interpolate(d.endAngle, angle * tau);

          return (t) => {
            d.endAngle = interpolate(t);
            return this.arc(d);
          };
        };
      })());
    this.totalBookedText.transition()
      .duration(750)
      .tween('text', () => {
        let currentValue = this.totalBookedText.text();
        let interpolateFunc = d3.interpolateRound(currentValue, totalBooked);

        return (t) => {
          const d3RenderNumber = interpolateFunc(t);
          const totalBookedText = d3RenderNumber ? d3RenderNumber : totalBooked;
          this.totalBookedText.text(totalBookedText);
        };
      });
  }

  configureChart() {
    const { intl, innerRadius, outerRadius, angle, totalBooked } = this.props;
    const txtMemberBooked = intl.formatMessage({ id: 'ClassInfo.Chart.MemberBooked' });
    // An arc function with all values bound except the endAngle. So, to compute an
    // SVG path string for a given angle, we pass an object with an endAngle
    // property to the `arc` function, and it will return the corresponding string.
    this.arc = d3.arc()
      .cornerRadius(20)
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .startAngle(0);

    let arcInner = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .startAngle(0);


    // Get the SVG container, and apply a transform such that the origin is the
    // center of the canvas. This way, we don’t need to position arcs individually.
    let svg = d3.select(`#check-in-progress-${this.id}`);
    let width = svg.attr('width');
    let height = svg.attr('height');
    let g = svg.append('g').attr('transform', `translate(${width / 2},${height / 2})`);

    /* For the drop shadow filter... */
    let defs = svg.append('defs');

    let linearGradient = defs.append('linearGradient')
      .attr('id', 'chartGradient');
    linearGradient.append('stop')
      .attr('offset', '0%')
      .attr('stop-color', '#E06A1B');
    linearGradient.append('stop')
      .attr('offset', '100%')
      .attr('stop-color', '#EB590F');
    let bgGradient = defs.append('linearGradient')
      .attr('id', 'bgGradient');
    bgGradient.append('stop')
      .attr('offset', '0%')
      .attr('stop-color', '#40a8d8');
    bgGradient.append('stop')
      .attr('offset', '100%')
      .attr('stop-color', '#3d84cb');
    // Add the background arc, from 0 to 100% (tau).
    g.append('path')
      .datum({ endAngle: tau })
      .style('fill', 'url(#chartGradient)')
      .attr('d', arcInner);

    // Add the foreground arc in orange, currently showing 12.7%.
    this.foreground = g.append('path')
      .datum({ endAngle: 0 })
      .style('fill', 'url(#bgGradient)')
      .attr('stroke-linecap', 'round')
      .attr('d', this.arc);

    // Add percentage into circle
    this.totalBookedText = g.append('text')
      .attr('text-anchor', 'middle')
      .text('0')
      .attr('font-size', '70px')
      .attr('font-weight', 'bold')
      .attr('fill', '#FFFFFF');

    g.append('text')
      .text(txtMemberBooked.split(' ')[0])
      .attr('text-anchor', 'middle')
      .attr('font-size', '18px')
      .attr('y', '30')
      .attr('fill', '#868686')
      .append('tspan').text(txtMemberBooked.split(' ')[1]).attr('y', '50').attr('x', '0');

    // Returns a tween for a transition’s "d" attribute, transitioning any selected
    // arcs from their current angle to the specified new angle.
    this.performTransition(angle, totalBooked);
  }

  render() {
    const { height, width } = this.props;

    return (
      <div className='check-in-progress-wrapper'>
        <svg
          id={`check-in-progress-${this.id}`}
          height={height}
          width={width} />
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
CheckInProgress.propTypes = {
  angle: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  innerRadius: PropTypes.number.isRequired,
  intl: intlShape.isRequired,
  outerRadius: PropTypes.number.isRequired,
  totalBooked: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired
};

export default injectIntl(CheckInProgress);


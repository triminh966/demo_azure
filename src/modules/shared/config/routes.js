import React from 'react';
import { Route, Redirect } from 'react-router';

import { CognitoService } from 'services';
import { AppConstants } from 'common';

import { Container } from '../../check-in/containers';

import Home from '../containers/home/Home';


export const makeRoutes = (getState) => {
  const requireAuth = (nextState, replace, callback) => {
    const { auth_users } = getState();
    const { currentUser } = auth_users;

    if (!currentUser) {
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
      return callback();
    } else if (!CognitoService.getCurrentUser()) {
      CognitoService.refreshToken().then(result => {
        let currentToken = localStorage.getItem('session_token');
        if (result !== currentToken) {
          localStorage.setItem('session_token', result);
        }
        // Re-authorize the route after refresh token
        replace({
          pathname: '/login',
          state: { nextPathname: nextState.location.pathname }
        });

        return callback();
      }).catch(() => { callback(); });
    }
    return callback();
  };
  /**
   * Return route define
   */
  return (
    <Route key='home'>
      {AppConstants.defaultPagePath.homePage !== '/' ? <Redirect from='/' to={AppConstants.defaultPagePath.homePage} /> : ''}
      <Route path='/' component={Container}>
        <Route onEnter={requireAuth} key='home' path='home'
          component={Home} />
      </Route>
    </Route>
  );
};


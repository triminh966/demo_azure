// Icon
export { default as FemaleIcon } from './icon/icon_female.svg';
export { default as MaleIcon } from './icon/icon_male.svg';
export { default as Logo } from './icon/icon_generic.svg';
export { default as LogoHome } from './icon/icon_home.svg';
export { default as LogoOTF } from './icon/Logo.png';
export { default as ArrowProfileIcon } from './icon/studio-profile-arrow.svg';
export { default as BackIcon } from './icon/arrow-back-header.svg';
export { default as HomeIcon } from './icon/home-header.svg';
export { default as CoachIcon } from './icon/icon_coach.svg';
export { default as LocationIcon } from './icon/icon_location.svg';
export { default as ConfirmedIcon } from './icon/icon_confirmed.svg';
export { default as ExpandIcon } from './icon/icon_expand.svg';
export { default as CollapseIcon } from './icon/icon_collapse.svg';
export { default as IconCheck } from './icon/icon_check.svg';
export { default as IconWearable } from './icon/icon_wearable.svg';
export { default as IconOTF } from './icon/icon_otf.svg';
export { default as DeviceOtbeat } from './icon/otbeat_devices.svg';
export { default as DeviceApple } from './icon/device_apple.png';
export { default as DeviceFitbit } from './icon/device_fitbit.png';
export { default as DeviceGarmin } from './icon/device_garmin.png';
export { default as DeviceOther } from './icon/device_other.png';
export { default as DeviceBurn } from './icon/device_burn.png';
export { default as DeviceCore } from './icon/device_core.png';
export { default as DeviceAspire } from './icon/device_aspire.png';
export { default as DeviceSeed } from './icon/device_seed.png';
export { default as IconCI } from './icon/icon_ci.svg';

export { default as TrackingMetrics } from './icon/tracking_metrics.png';
export { default as PhotoArm } from './icon/photo_arm.png';
export { default as PhotoChest } from './icon/photo_chest.png';
export { default as BurnIcon } from './icon/icon_burn.svg';
export { default as CheckOrangeIcon } from './icon/icon_check_orange.svg';
export { default as BatteryIcon } from './icon/icon_rechargeable.svg';
export { default as SensorIcon } from './icon/icon_sensor.svg';
export { default as FlagIcons } from './icon/flags.png';
export { default as ReciepIcon } from './icon/recipe.svg';
export { default as GreenCreditCardIcon } from './icon/icon_credit_card_green.svg';
export { default as RedCreditCardIcon } from './icon/icon_credit_card_red.svg';
export { default as IntroMember } from './icon/icon_intro_member.png';
export { default as IntroMemberGrey } from './icon/icon_intro_member_grey.png';

// Background
export { default as MainBackground } from './background/Main_bg.svg';

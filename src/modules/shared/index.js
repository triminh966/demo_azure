import { makeRoutes } from './config/routes';


const SharedModule = {
  makeRoutes: makeRoutes,
  redux: { }
};

export default SharedModule;

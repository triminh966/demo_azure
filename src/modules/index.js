import auth from './authorization';
import core from './core';
import checkIn from './check-in';
import productSelection from './product-selection';
import shared from './shared';
import clientTake from './client-take';

let modules = {};

modules = {
  auth,
  core,
  checkIn,
  productSelection,
  shared,
  clientTake
};

export let makeRoutes = {};
export let redux = {};

Object.keys(modules).forEach(key => {
  const mod = modules[key];
  makeRoutes[key] = mod.makeRoutes || {};
  Object.keys(mod.redux || {}).forEach(rdKey => {
    const rd = mod.redux[rdKey];
    redux[`${key}_${rdKey}`] = rd || {};
  });
});

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { intlShape, injectIntl } from 'react-intl';
import * as _ from 'lodash';

import { WizardForm } from 'modules/core/components';
import MemberSection from './member-section/MemberSection';
import StaffSection from './staff-section/StaffSection';
import PolicySection from './policy-section/PolicySection';
import {Helpers, SelectOptionsConstants} from 'common';
import { postalCode, isValidPhoneNumber } from 'common/utils/validation';
import { Constants } from 'common';

import './styles.scss';

export class CIForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      memberInfo: {},
      studioInfo: {},
      policyInfo: {}
    };
    this.isPostalCode = postalCode();
    this.phone = isValidPhoneNumber();
  }

  componentDidMount() {
    const { actions, memberDetail, location, params } = this.props;

    if (!memberDetail && params.id) {
      actions.clientTake_members.getMember(params.id);
    }
    actions.clientTake_members.getMemberGoals(location.query.memberId);
  }

  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(newProps) {
    if (newProps && newProps.memberInfo && newProps.memberInfo !== this.props.memberInfo) {
      const {memberInfo} = newProps;
      this.buildMemberInfo(memberInfo);
    } else if (newProps && newProps.studioInfo && newProps.studioInfo !== this.props.studioInfo) {
      const {studioInfo} = newProps;
      this.buildStudioInfo(studioInfo);
    } else if (newProps && newProps.policyInfo && newProps.policyInfo !== this.props.policyInfo) {
      const {policyInfo} = newProps;
      this.buildPolicyInfo(policyInfo);
    }
    if (newProps && newProps.clientIntake && newProps.clientIntake.code === 'SUCCESS' && newProps.clientIntake !== this.props.clientIntake) {
      const {currentStudio, actions, intl} = this.props;
      actions.core_alert.showInfor(`${intl.formatMessage({id: 'ClientIntake.SaveSuccessfully'})} ${currentStudio.StudioName} #${currentStudio.StudioNumber}`);
      Helpers.delay(() => {
        this.props.router.push('/client-take/member-search');
      }, 3000);
    }
  }

  componentWillUnmount() {
    this.props.actions.clientTake_members.detroyMemberDetail();
    this.props.actions.clientTake_members.resetClientIntake();
    this.props.actions.core_wizardForm.resetCurrentStep();
    this.props.actions.clientTake_members.clearSignature();
    this.props.actions.clientTake_members.clearMemberGoals();
  }

  buildMemberInfo(memberInfo) {
    this.setState({
      memberInfo: {
        ...memberInfo,
        addressType: 'Home',
        addressLine1: memberInfo.address,
        weight: parseFloat(memberInfo.weight),
        height: memberInfo.weightMeasurement === Constants.WeightMeasurement.Metric ? parseFloat(memberInfo.heightCm)
          : parseFloat(this.convertToInch({feet: memberInfo.heightFeet, inch: memberInfo.heightInch})),
        heightMeasurement: memberInfo.weightMeasurement === Constants.WeightMeasurement.Metric
          ? Constants.HeightMeasurement.Metric : Constants.HeightMeasurement.Imperial,
        phoneNumber: Helpers.formatPhoneNumber(memberInfo.phoneNumber),
        emergencyNumber: Helpers.formatPhoneNumber(memberInfo.emergencyNumber),
        birthDay: moment(memberInfo.birthDay).format('YYYY-MM-DD'),
        questions: {
          doctorDiagnosis: {
            diagnosis: memberInfo.doctorDiagnosis,
            other: (memberInfo.doctorDiagnosis || []).includes('Other') ? document.getElementsByName('doctorDiagnosisTxt')[0].value : ''
          },
          performPhysicalActivity: {
            isPainChest: memberInfo.isPainChest,
            isLimitation: memberInfo.isLimitation,
            reason: memberInfo.isLimitation ? memberInfo.confirmLimitation : null
          },
          isMuscleProblem: memberInfo.isMuscleProblem,
          isPregnant: memberInfo.isPregnant,
          bloodPressure: {
            isBloodPressure: memberInfo.isBloodPressure,
            reason: memberInfo.listAnswer ? memberInfo.listAnswer : null
          }
        }
      }
    });
  }

  buildStudioInfo(studioInfo) {
    this.setState({
      studioInfo: {
        isCorporate: this.props.currentStudio.IsCorporate,
        registrationHSNo: this.props.currentStudio.RegistrationHSNo,
        studioState: this.props.currentStudio.StudioState,
        studioName: this.props.currentStudio.StudioName,
        studioUUId: this.props.currentStudio.StudioUUId,
        questions: {
          beforeWork: {
            isWorkedOTFBefore: studioInfo.isWorkedOTFBefore,
            location: studioInfo.isWorkedOTFBefore === 'Yes' ? document.getElementsByName('locationOTF')[0].value : null
          },
          receiveNews: {
            ways: studioInfo.receiveNews,
            referrer: document.getElementsByName('referrer')[0].value,
            otherWays: document.getElementsByName('otherWays')[0].value
          },
          exerciseInfo: {
            isCurrentExercising: studioInfo.isCurrentExercising,
            exercises: studioInfo.exercises,
            otherExercises: document.getElementsByName('otherExercise')[0].value,
            frequency: studioInfo.frequency
          },
          fitnessGoals: {
            personal: studioInfo.personal,
            otherFitnessGoal: document.getElementsByName('otherFitnessGoals')[0].value,
            runDistance: studioInfo.runDistance,
            reason: studioInfo.reason,
            timeAchieveGoals: {
              time: studioInfo.timeAchieveGoals,
              other: document.getElementsByName('otherTimeTxt')[0].value
            },
            passionateAchieve: studioInfo.passionateAchieve,
            barriersOfGoals: studioInfo.barriersOfGoals,
            isTrainingPast: studioInfo.isTrainingPast,
            barriersExp: studioInfo.barriersExp,
            whenInBestShape: studioInfo.whenInBestShape,
            doingInBestShape: studioInfo.doingInBestShape,
            supportSystem: studioInfo.supportSystem,
            allocatedToInvest: studioInfo.allocatedToInvest,
            classScheduleProvided: studioInfo.classScheduleProvided
          }
        }
      }
    });
  }
  buildPolicyInfo(policyInfo) {
    const {actions, memberDetail} = this.props;
    let memberGoalPayload = this.buildMemberGoalPayload();
    this.setState((prevState) => {
      delete prevState.memberInfo.doctorDiagnosis;
      delete prevState.memberInfo.isPainChest;
      delete prevState.memberInfo.isLimitation;
      delete prevState.memberInfo.isMuscleProblem;
      delete prevState.memberInfo.isPregnant;
      delete prevState.memberInfo.isBloodPressure;
      delete prevState.memberInfo.listAnswer;
      delete prevState.memberInfo.confirmLimitation;
      return {
        memberInfo: {
          ...prevState.memberInfo,
          memberSignatureDate: moment(policyInfo.memberSignatureDate).format('MM/DD/YYYY'),
          parentSignatureDate: moment(policyInfo.parentSignatureDate).format('MM/DD/YYYY')
        },
        studioInfo: {
          ...prevState.studioInfo,
          staffSignatureDate: moment(policyInfo.staffSignatureDate).format('MM/DD/YYYY')
        }
      };
    }, ()=>{
      const infoClientIntake = {
        memberInfo: {
          ...this.state.memberInfo,
          clientName: policyInfo.clientName,
          memberSignature: this.props.memberSignature ? this.props.memberSignature.toDataURL() : null,
          parentName: policyInfo.parentName,
          parentSignature: this.props.parentSignature ? this.props.parentSignature.toDataURL() : null
        },
        studioInfo: {
          ...this.state.studioInfo,
          staffName: policyInfo.staffName,
          staffSignature: this.props.staffSignature ? this.props.staffSignature.toDataURL() : null
        }
      };
      actions.clientTake_members.saveClientIntake(memberDetail.memberUUId, infoClientIntake, memberGoalPayload);
    });
  }

  /** Build member goal for POST request */
  buildMemberGoalPayload() {
    const { location, studioInfo, memberGoals = {} } = this.props;
    const otherFitnessGoal = this.state.studioInfo.questions.fitnessGoals.otherFitnessGoal;
    const memberId = location.query.memberId;
    const otherText = otherFitnessGoal.length > 0 ? otherFitnessGoal : null;
    let goalCategoryIds = []; /** list if goalcategoryId (number) -> only selected categoryId */

    /** Get goalId -> only goal CategoryId = 6 (Running distance) */
    /** {name : 'string', value : number} */
    const selectedGoal = _.find(SelectOptionsConstants.listGoalIds, function(obj) {
      return (obj.name === studioInfo.runDistance);
    });
    const goalId = (selectedGoal) ? selectedGoal.value : null;

    /** Get goal categoryId based on UI selected, find by name */
    SelectOptionsConstants.listMemberGoals.forEach(mg => {
      let goal = _.find(studioInfo.personal, function(obj) {
        return mg.name === obj;
      });

      if (goal) {
        goalCategoryIds.push(mg.value);
      }
    });

    memberGoals.forEach(gc => {
      if (gc) {
        const firstIndex = 0;
        /** Running distance */
        if (gc.goalCategoryId === 6) {
          let goals = gc.goals || [];

          goals.forEach(g => {
            if (g && g.goalId === goalId) {
              g.isSelected = true;
            } else {
              g.isSelected = false;
            }
          });
        } else {
          /** The rest */
          if (_.isArray(gc.goals)) {
            let goal = gc.goals[firstIndex];

            /** Set other text to field that had the value field */
            if (gc.goalCategoryId === 11 && goal.hasOwnProperty('value')) {
              goal.value = otherText;
            }

            if (goalCategoryIds.indexOf(gc.goalCategoryId) > -1) {
              goal.isSelected = true;
            } else {
              goal.isSelected = false;
            }
          }
        }
      }
    });

    return { memberId, data: {goalCategories: memberGoals } };
  }
  onSubmit(step) {
    const {actions} = this.props;
    switch (step) {
      case 1: {
        actions.clientTake_members.saveMemberSectionValues();
        break;
      }
      case 2: {
        actions.clientTake_members.saveStaffSectionValues();
        break;
      }
      case 3: {
        actions.clientTake_members.savePolicySectionValues();
        break;
      }
      default: {
        break;
      }
    }
  }

  renderSteps(step, next) {
    const { dispatch, memberDetail, currentStudio, actions, clientIntake, memberGoals } = this.props;
    return (
      <div className={'steps-wrapper'}>
        <MemberSection actions={actions} currentStep={step} afterValid={next}
          member={memberDetail} dispatch={dispatch} onSubmit={()=>this.onSubmit.bind(this)(step)}/>
        <StaffSection actions={actions} currentStep={step} afterValid={next}
          onSubmit={()=>this.onSubmit.bind(this)(step)} memberGoals={memberGoals}/>
        <PolicySection actions={actions} currentStep={step} afterValid={next}
          studio={currentStudio} signature={this.signature} onSubmit={()=>this.onSubmit.bind(this)(step)}
          clientIntake={clientIntake} isAbleSubmit={this.isAbleSubmit()}/>
      </div>
    );
  }

  renderWizardSteps() {
    const { intl } = this.props;
    return [
      {
        title: intl.formatMessage({ id: 'ClientIntake.MemberSecion.Title' })
      },
      {
        title: intl.formatMessage({ id: 'ClientIntake.StaffSecion.Title' })
      },
      {
        title: intl.formatMessage({ id: 'ClientIntake.PolicySecion.Title' })
      }
    ];
  }

  isAbleSubmit() {
    const {memberInfo} = this.state;
    const {memberSignature, staffSignature, parentSignature} = this.props;
    return memberInfo.questions && (memberInfo.questions.doctorDiagnosis.diagnosis
    || (memberInfo.questions.doctorDiagnosis.other))
    && (memberInfo.questions.performPhysicalActivity.reason
      || memberInfo.questions.performPhysicalActivity.isLimitation === 'No')
    && memberInfo.name && memberInfo.address && memberInfo.birthDay && memberInfo.weight && memberInfo.height
    && !this.isPostalCode(memberInfo.postalCode)
    && !this.phone(this.props.memberInfo.phoneNumber) && !this.phone(this.props.memberInfo.phoneNumber)
    && (!_.isEmpty(memberSignature) && !memberSignature.isEmpty())
    && (!_.isEmpty(staffSignature) && !staffSignature.isEmpty())
    && (Helpers.calculateAge(new Date(memberInfo.birthDay)) < 18 ?
      (!_.isEmpty(parentSignature) && !parentSignature.isEmpty()) : true);
  }
  convertToInch(height) {
    return height.cm ? Math.floor(parseFloat(height.cm || 0) * 0.393700787)
      : parseFloat(height.feet || 0) * 12 + parseFloat(height.inch || 0);
  }
  render() {
    if (!this.props.memberDetail) {
      return null;
    }
    return (
      <div className='ci-form' id='ci-form'>
        <WizardForm className={'ci-wizard'} renderSteps={this.renderSteps.bind(this)} steps={this.renderWizardSteps()}
          clientIntake={this.props.clientIntake}/>
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
CIForm.propTypes = {
  actions: PropTypes.object,
  clientInitials: PropTypes.object,
  clientIntake: PropTypes.object,
  currentStudio: PropTypes.object,
  dispatch: PropTypes.func,
  intl: intlShape.isRequired,
  memberDetail: PropTypes.object,
  memberGoals: PropTypes.array,
  memberInfo: PropTypes.object,
  memberSignature: PropTypes.object,
  params: PropTypes.object,
  parentSignature: PropTypes.object,
  policyInfo: PropTypes.object,
  router: PropTypes.object,
  staffSignature: PropTypes.object,
  studioInfo: PropTypes.object
};

const ciForm = connect(state => ({
  memberDetail: state.clientTake_members.memberDetail,
  currentStudio: state.auth_users.currentStudio,
  memberInfo: state.clientTake_members.memberInfo,
  studioInfo: state.clientTake_members.studioInfo,
  policyInfo: state.clientTake_members.policyInfo,
  clientIntake: state.clientTake_members.clientIntake,
  memberSignature: state.clientTake_members.memberSignature,
  staffSignature: state.clientTake_members.staffSignature,
  parentSignature: state.clientTake_members.parentSignature,
  clientInitials: state.clientTake_members.clientInitials,
  memberGoals: state.clientTake_members.memberGoals
}))(CIForm);

export default injectIntl(ciForm);

import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, initialize, change } from 'redux-form';
import { injectIntl } from 'react-intl';
import _ from 'lodash';
import { WizardButton } from 'modules/core/components';
import './styles.scss';
import { SelectOptionsConstants, Constants, Helpers } from 'common';
import CheckBox from '../checkbox/checkbox';
import {
  isRequired
} from 'common/utils/validation';
export class StaffSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Web: false,
      SocialMedia: false,
      Drive: false,
      Referral: false,
      Other: false,
      Cardio: false,
      WeightTraining: false,
      Classes: false,
      OtherExercise: false,
      isOtherFitnessGoal: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.OtherFitnessGoal).isChecked,

      isWeightLose: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.WeightLose),
      isIncMuscleTone: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.IncMuscleTone),
      isIncMuscleMass: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.IncMuscleMass),
      isInEnergy: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.InEnergy),
      isOverallHealth: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.OverallHealth),


      isStrength: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.Strength),
      isEndurance: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.Endurance),
      isExpFamily: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.ExpFamily),
      isDoPositive: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.DoPositive),

      weekDays: {
        Monday: false,
        Tuesday: false,
        Wednesday: false,
        Thursday: false,
        Friday: false,
        Saturday: false,
        Sunday: false
      },
      meridian: {
        AM: false,
        PM: false
      },
      Yes: '',
      No: '',
      timeAchieveGoals: '',
      fitness: [],
      frequency: '',
      isCurrentExercising: '',
      passionateAchieve: '',
      isTrainingPast: '',
      receiveNews: [],
      exercises: [],
      personal: this.initPersonalGoals(props) || [],
      doPositive: [],
      classScheduleProvided: [],
      locationOTF: '',
      otherWays: '',
      referrer: '',
      otherExercise: '',
      runDistance: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.IncreaseRunningDistance),
      otherFitnessGoals: this.getDefaultMemberGoals(props.memberGoals, Constants.GoalCategory.OtherFitnessGoal).value
    };
    this.isRequired = isRequired(
      this.props.intl.formatMessage({ id: 'Validation.RequiredField' })
    );
  }
  componentDidMount() {
    this.props.dispatch(initialize('staffSection', {
      isWorkedOTFBefore: '',
      location: '',
      isCurrentExercising: '',
      frequency: '',
      passionateAchieve: '',
      timeAchieveGoals: '',
      isTrainingPast: '',
      personal: this.initPersonalGoals(this.props) || [],
      runDistance: this.getDefaultMemberGoals(this.props.memberGoals, Constants.GoalCategory.IncreaseRunningDistance)
    }));
  }
  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(newProps) {
    if (newProps && !_.isEqual(newProps.memberGoals, []) && !_.isEqual(newProps.memberGoals, this.props.memberGoals)) {
      Object.keys(Constants.GoalCategory).forEach(gc => {
        let goal = _.find(newProps.memberGoals, function(o) {
          return o.goalCategoryId === Constants.GoalCategory[gc];
        });
        if (goal) {
          if (goal.goalCategoryId === Constants.GoalCategory.IncreaseRunningDistance) {
            let goalDistance = _.find(goal.goals, ['isSelected', true]);
            this.setState(state => {
              return {
                ...state,
                runDistance: (goalDistance || {}).localizedDisplayName
              };
            }, () => {
              this.props.dispatch(change('staffSection', 'runDistance', (goalDistance || {}).localizedDisplayName));
            });
          } else if (goal.goals[0].isSelected) {
            let array = this.state.personal;
            array.push(`is${gc}`);
            this.setState(state => {
              return {
                ...state,
                [`is${gc}`]: true,
                otherFitnessGoals: goal.goals[0].value,
                personal: array
              };
            }, () => {
              this.props.dispatch(change('staffSection', 'personal', array));
            });
          }
        }
      });
    }
  }
  initPersonalGoals(props) {
    let arrayMg = [];
    if (props.memberGoals.length > 0) {
      props.memberGoals.forEach(mg => {
        if (mg.goals[0].isSelected && mg.goalCategoryId !== Constants.GoalCategory.IncreaseRunningDistance) {
          arrayMg.push(`is${Helpers.getEnumDescription(Constants.GoalCategory, mg.goalCategoryId)}`);
        }
      });
    }
    this.props.dispatch(change('staffSection', 'personal', arrayMg));
    return arrayMg;
  }
  getDefaultMemberGoals(memberGoals, memberGoalId) {
    let mg = _.find(memberGoals, function(o) {
      return o.goalCategoryId === memberGoalId;
    });
    if (memberGoalId === Constants.GoalCategory.IncreaseRunningDistance) {
      if (mg) {
        let goalDistance = _.find(mg.goals, ['isSelected', true]);
        return (goalDistance || {}).localizedDisplayName;
      }
      return '';
    } else if (memberGoalId === Constants.GoalCategory.OtherFitnessGoal) {
      if (mg && mg.goals[0].isSelected) {
        return {isChecked: true, value: mg.goals[0].value };
      }
      return {isChecked: false, value: ''};
    }
    return mg ? mg.goals[0].isSelected : false;
  }
  validate() {
    this.props.afterValid(this.state);
  }
  handleCheckbox(key, arrName) {
    let array = this.state[arrName];
    this.setState((prevState) => ({
      [key]: !prevState[key]
    }), () => {
      if (this.state[key]) {
        array.push(key);
      } else {
        _.remove(array, function (element) {
          return element === key;
        });
        if (key === 'Referral') {
          this.setState({
            referrer: ''
          });
        } else if (key === 'Other') {
          this.setState({
            otherWays: ''
          });
        } else if (key === 'OtherExercise') {
          this.setState({
            otherExercise: ''
          });
        } else if (key === 'isOtherFitnessGoal') {
          this.setState({
            otherFitnessGoals: ''
          });
        }
      }
      this.setState({
        [arrName]: array
      }, () => {
        this.props.dispatch(change('staffSection', arrName, array));
      });
    });
  }

  handleClassScheduleProvidedCheckbox(cbName, groupName) {
    let array = this.state.classScheduleProvided;
    this.setState((prevState) => ({
      [groupName]: {
        ...prevState[groupName],
        [cbName]: !prevState[groupName][cbName]
      }
    }), () => {
      if (this.state[groupName][cbName]) {
        array.push(cbName);
      } else {
        _.remove(array, function (element) { return element === cbName; });
      }
      this.setState({
        classScheduleProvided: array
      }, () => {
        this.props.dispatch(change('staffSection', 'classScheduleProvided', array));
      });
    });
  }

  renderField({ input, type, options, meta: { touched, error }, className, disabled }) {
    return (
      <div className={`input-form${touched && error ? ' has-error' : ''}`}>
        <input
          className={`form-control ${className}`}
          {...input}
          {...options}
          disabled={disabled}
          type={type} />
        {touched && (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  handleCheckYesNo(key) {
    this.setState((prevState) => ({
      [key]: !prevState[key]
    }), () => {
      if (key === 'Yes') {
        this.setState({
          No: false,
          locationOTF: ''
        });
      } else if (key === 'No') {
        this.setState({
          Yes: false,
          locationOTF: ''
        });
      }
    });
    this.props.dispatch(change('staffSection', 'isWorkedOTFBefore', key));
  }

  changeHandler = (key, name) => {
    let newState = { [name]: key };

    if (name === 'isTrainingPast') {
      this.props.change('barriersExp', '');
    } else if (name === 'timeAchieveGoals') {
      this.props.change('otherTimeTxt', '');
    }
    this.setState(newState, () => this.props.dispatch(change('staffSection', name, newState[name])));
  }

  renderHaveBeenOTFCheckBox() {
    return <div className='have-checkbox'>
      <div className='item input-item'>
        <input
          type='checkbox'
          id='Yes'
          name='isWorkedOTFBefore'
          checked={this.state.Yes}
          onChange={() => this.handleCheckYesNo.bind(this)('Yes')} />
        <label className='checkbox-custom-label' htmlFor='Yes'>{'Yes - Location'}</label>
        <input
          type='text'
          id='otherHealthTxt'
          name='locationOTF' className='form-control txt-yeslocation '
          defaultValue={this.state.locationOTF}
          disabled={!this.state.Yes}
          onBlur={(event) => this.handleOther(event, 'locationOTF')} />
      </div>
      <div className='item input-item'>
        <input
          type='checkbox'
          id='No'
          name='isWorkedOTFBefore'
          checked={this.state.No}
          onChange={() => this.handleCheckYesNo.bind(this)('No')} />
        <label className='checkbox-custom-label' htmlFor='No'>{'No'}</label>
      </div>
    </div>;
  }

  renderHearHow() {
    return <div className='hear-checkbox'>
      <div className='row-one'>
        <div className='item'>
          <input
            type='checkbox'
            id='Website'
            name='receiveNews'
            checked={this.state.Web}
            onChange={() => this.handleCheckbox.bind(this)('Web', 'receiveNews')} />
          <label className='checkbox-custom-label' htmlFor='Website'>{'Website'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='SocialMedia'
            name='receiveNews'
            checked={this.state.SocialMedia}
            onChange={() => this.handleCheckbox.bind(this)('SocialMedia', 'receiveNews')} />
          <label className='checkbox-custom-label' htmlFor='SocialMedia'>{'Social Media'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='Drive'
            name='receiveNews'
            checked={this.state.Drive}
            onChange={() => this.handleCheckbox.bind(this)('Drive', 'receiveNews')} />
          <label className='checkbox-custom-label' htmlFor='Drive'>{'Drive/walk by'}</label>
        </div>
      </div>
      <div className='row-two'>
        <div className='item input-item'>
          <input
            type='checkbox'
            id='Referral'
            name='receiveNews'
            checked={this.state.Referral}
            onChange={() => this.handleCheckbox.bind(this)('Referral', 'receiveNews')} />
          <label className='checkbox-custom-label' htmlFor='Referral'>{'Referral'}</label>
          <input
            type='text'
            id='referrerTxt'
            name='referrer' className='form-control txt-other'
            defaultValue={this.state.referrer}
            disabled={!this.state.Referral}
            onBlur={(event) => this.handleOther.bind(this)(event, 'referrer')} />
        </div>
        <div className='item input-item'>
          <input
            type='checkbox'
            id='Other'
            name='receiveNews'
            checked={this.state.Other}
            onChange={() => this.handleCheckbox.bind(this)('Other', 'receiveNews')} />
          <label className='checkbox-custom-label' htmlFor='Other'>{'Other'}</label>
          <input
            type='text'
            id='otherWaysTxt'
            name='otherWays' className='form-control txt-other'
            defaultValue={this.state.otherWays}
            disabled={!this.state.Other}
            onBlur={(event) => this.handleOther.bind(this)(event, 'otherWays')} />
        </div>
      </div>
    </div>;
  }

  renderCurrentlyExercising() {
    return <div className='health-checkbox'>
      <CheckBox
        name='isCurrentExercising'
        selectedPosition={this.state.isCurrentExercising}
        options={SelectOptionsConstants.isYesNo}
        onSelect={this.changeHandler} />
    </div>;
  }
  renderDoingForExercise() {
    return <div className='health-checkbox'>
      <div className='item'>
        <input
          type='checkbox'
          id='Cardio'
          name='exercises'
          checked={this.state.Cardio}
          onChange={() => this.handleCheckbox.bind(this)('Cardio', 'exercises')} />
        <label className='checkbox-custom-label' htmlFor='Cardio'>{'Cardio'}</label>
      </div>
      <div className='item'>
        <input
          type='checkbox'
          id='WeightTraining'
          name='exercises'
          checked={this.state.WeightTraining}
          onChange={() => this.handleCheckbox.bind(this)('WeightTraining', 'exercises')} />
        <label className='checkbox-custom-label' htmlFor='WeightTraining'>{'Weight-training'}</label>
      </div>
      <div className='item'>
        <input
          type='checkbox'
          id='Classes'
          name='exercises'
          checked={this.state.Classes}
          onChange={() => this.handleCheckbox.bind(this)('Classes', 'exercises')} />
        <label className='checkbox-custom-label' htmlFor='Classes'>{'Classes (Pilates, Yoga, Spin, Barre)'}</label>
      </div>
      <div className='item input-item'>
        <input
          type='checkbox'
          id='OtherExercises'
          name='exercises'
          checked={this.state.OtherExercise}
          onChange={() => this.handleCheckbox.bind(this)('OtherExercise', 'exercises')} />
        <label className='checkbox-custom-label' htmlFor='OtherExercises'>{'Other'}</label>
        <input
          type='text'
          id='other'
          name='otherExercise' className='form-control txt-other'
          disabled={!this.state.OtherExercise}
          defaultValue={this.state.otherExercise}
          onBlur={(event) => this.handleOther.bind(this)(event, 'otherExercise')} />
      </div>
    </div>;
  }


  renderWeeklyExercise() {
    return <div className='health-checkbox'>
      <CheckBox
        name='frequency'
        selectedPosition={this.state.frequency}
        options={SelectOptionsConstants.weeklyExercise}
        onSelect={this.changeHandler} />
    </div>;
  }

  renderFitnessGoals({
    input,
    type,
    options,
    meta: { touched, error },
    disabled,
    className,
    autoComplete,
    autoFocus
  }) {
    return (
      <div>
        <div className='fitness-muscle-checkbox'>
          <div className='item'>
            <input
              type='checkbox'
              id='WeightLose'
              name='personal'
              checked={this.state.isWeightLose}
              onChange={() => this.handleCheckbox.bind(this)('isWeightLose', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='WeightLose'>{'Lose Weight'}</label>
          </div>
          <div className='item'>
            <input
              type='checkbox'
              id='IncMuscleTone'
              name='personal'
              checked={this.state.isIncMuscleTone}
              onChange={() => this.handleCheckbox.bind(this)('isIncMuscleTone', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='IncMuscleTone'>{' Increase Muscle Tone'}</label>
          </div>
          <div className='item'>
            <input
              type='checkbox'
              id='IncMuscleMass'
              name='personal'
              checked={this.state.isIncMuscleMass}
              onChange={() => this.handleCheckbox.bind(this)('isIncMuscleMass', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='IncMuscleMass'>{'Increase Muscle Mass'}</label>
          </div>
          <div className='item'>
            <input
              type='checkbox'
              id='InEnergy'
              name='personal'
              checked={this.state.isInEnergy}
              onChange={() => this.handleCheckbox.bind(this)('isInEnergy', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='InEnergy'>{'Increase Energy – More life'}</label>
          </div>
          <div className='item'>
            <input
              type='checkbox'
              id='OverallHealth'
              name='fitnessGoals'
              checked={this.state.isOverallHealth}
              onChange={() => this.handleCheckbox.bind(this)('isOverallHealth', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='OverallHealth'>{'Increase Heart Health – Or Increase overall health'}</label>
          </div>
        </div>
        <div className='fitness-distance-checkbox'>
          <span className='quest'>{'Increase running distance :'}</span>
          <CheckBox
            name='runDistance'
            selectedPosition={this.state.runDistance}
            options={SelectOptionsConstants.runningDistance}
            onSelect={this.changeHandler} />
        </div>
        <div className='fitness-strength-checkbox'>
          <div className='item'>
            <input
              type='checkbox'
              id='strength'
              name='fitnessGoals'
              checked={this.state.isStrength}
              onChange={() => this.handleCheckbox.bind(this)('isStrength', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='strength'>{'Increase strength'}</label>
          </div>
          <div className='item'>
            <input
              type='checkbox'
              id='endurance'
              name='fitnessGoals'
              checked={this.state.isEndurance}
              onChange={() => this.handleCheckbox.bind(this)('isEndurance', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='endurance'>{'Increase endurance'}</label>
          </div>
          <div className='item'>
            <input
              type='checkbox'
              id='expFamily'
              name='fitnessGoals'
              checked={this.state.isExpFamily}
              onChange={() => this.handleCheckbox.bind(this)('isExpFamily', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='expFamily'>{'Set a good example for my family'}</label>
          </div>
        </div>
        <div className='fitness-something-checkbox'>
          <div className='item'>
            <input
              type='checkbox'
              id='doPositive'
              name='fitnessGoals'
              checked={this.state.isDoPositive}
              onChange={() => this.handleCheckbox.bind(this)('isDoPositive', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='doPositive'>{'Do something positive for myself'}</label>
          </div>
          <div className='item input-item'>
            <input
              type='checkbox'
              id='OtherFitnessGoal'
              name='fitnessGoals'
              checked={this.state.isOtherFitnessGoal}
              onChange={() => this.handleCheckbox.bind(this)('isOtherFitnessGoal', 'personal')} />
            <label className='checkbox-custom-label' htmlFor='OtherFitnessGoal'>{'Other'}</label>
            <div
              className={`input-form${
                error ? ' has-error' : ''
              } txt-other`}>
              <input
                type='text'
                id='other'
                name='otherFitnessGoals' className='form-control'
                disabled={!this.state.isOtherFitnessGoal}
                defaultValue={this.state.otherFitnessGoals}
                onBlur={(event) => this.handleOther.bind(this)(event, 'otherFitnessGoals')} />
              {error && <span className='text-danger-tooltip'>{error}</span>}
            </div>
          </div>
        </div>
      </div>);
  }


  renderThinkAchievingGoals() {
    return <div className='health-checkbox'>
      <CheckBox
        name='timeAchieveGoals'
        selectedPosition={this.state.timeAchieveGoals}
        options={SelectOptionsConstants.timeAchieveArr}
        onSelect={this.changeHandler}
        otherName={'otherTimeTxt'}
        otherValue={this.state.otherTimeTxt}
        onChangeOther={(event, name) => this.handleOther(event, name)}
        isOtherDisabled={this.state.timeAchieveGoals !== 'Other' || !this.state.timeAchieveGoals} />
    </div>;
  }

  renderPassionateAchievingGoals() {
    return <div className='health-checkbox'>
      <CheckBox
        name='passionateAchieve'
        selectedPosition={this.state.passionateAchieve}
        options={SelectOptionsConstants.achievingGoals}
        onSelect={this.changeHandler} />
    </div>;
  }

  renderIsTrainingPast() {
    return <div className='health-checkbox'>
      <CheckBox
        name='isTrainingPast'
        selectedPosition={this.state.isTrainingPast}
        options={SelectOptionsConstants.isYesNo}
        onSelect={this.changeHandler} />
    </div>;
  }
  renderFieldAllocatedToInvest({ input, type, options, meta: { touched, error }, className }) {
    return (
      <div className={`input-form${touched && error ? ' has-error' : ''} ati-input`}>
        <span className={'prefix-ati'}>{'$'}</span>
        <input
          className={`form-control ${className}`}
          {...input}
          {...options}
          type={type} />
        {touched && (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }
  renderDayOfWeek() {
    return <div>
      <div className='week-checkbox'>
        <div className='item'>
          <input
            type='checkbox'
            id='Sunday'
            name='classScheduleProvided'
            checked={this.state.weekDays.Sunday}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('Sunday', 'weekDays')} />
          <label className='checkbox-custom-label' htmlFor='Sunday'>{'Sunday'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='Monday'
            name='classScheduleProvided'
            checked={this.state.weekDays.Monday}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('Monday', 'weekDays')} />
          <label className='checkbox-custom-label' htmlFor='Monday'>{'Monday'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='Tuesday'
            name='classScheduleProvided'
            checked={this.state.weekDays.Tuesday}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('Tuesday', 'weekDays')} />
          <label className='checkbox-custom-label' htmlFor='Tuesday'>{'Tuesday'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='Wednesday'
            name='classScheduleProvided'
            checked={this.state.weekDays.Wednesday}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('Wednesday', 'weekDays')} />
          <label className='checkbox-custom-label' htmlFor='Wednesday'>{'Wednesday'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='Thursday'
            name='classScheduleProvided'
            checked={this.state.weekDays.Thursday}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('Thursday', 'weekDays')} />
          <label className='checkbox-custom-label' htmlFor='Thursday'>{'Thursday'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='Friday'
            name='classScheduleProvided'
            checked={this.state.weekDays.Friday}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('Friday', 'weekDays')} />
          <label className='checkbox-custom-label' htmlFor='Friday'>{'Friday'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='Saturday'
            name='classScheduleProvided'
            checked={this.state.weekDays.Saturday}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('Saturday', 'weekDays')} />
          <label className='checkbox-custom-label' htmlFor='Saturday'>{'Saturday'}</label>
        </div>
      </div>
      <div className='week-checkbox'>
        <div className='item'>
          <input
            type='checkbox'
            id='AM'
            name='classScheduleProvided'
            checked={this.state.meridian.AM}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('AM', 'meridian')} />
          <label className='checkbox-custom-label' htmlFor='AM'>{'AM'}</label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='PM'
            name='classScheduleProvided'
            checked={this.state.meridian.PM}
            onChange={() => this.handleClassScheduleProvidedCheckbox.bind(this)('PM', 'meridian')} />
          <label className='checkbox-custom-label' htmlFor='PM'>{'PM'}</label>
        </div>
      </div>
    </div>;
  }

  renderFormInput() {
    const {isOtherFitnessGoal, otherFitnessGoals} = this.state;
    return (
      <div className='form-container'>
        <div className='additional-questions'>
          <div className='quest-item'>
            <span className='quest'>{'Have you been to an Orangetheory Fitness facility before?'}</span>
            <Field
              name='isWorkedOTFBefore'
              component={this.renderHaveBeenOTFCheckBox.bind(this)} />
          </div>
          <div className='quest-item'>
            <span className='quest'>{'How did you hear about us?'}</span>
            <Field
              name='hear-about-us'
              component={this.renderHearHow.bind(this)} />
          </div>
          <div className='quest-item'>
            <span className='quest'>{'Do you currently exercise?'}</span>
            <Field
              name='isCurrentExercising'
              component={this.renderCurrentlyExercising.bind(this)} />
          </div>
          <div className='quest-item'>
            <span className='quest'><b>{'If Yes:'}</b>{' What are you currently doing for exercise?'}</span>
            <Field
              name='doing-for-exercise'
              component={this.renderDoingForExercise.bind(this)} />
          </div>
          <div className='quest-item'>
            <span className='quest'>{'What is your weekly exercise frequency?'}</span>
            <Field
              name='frequency'
              component={this.renderWeeklyExercise.bind(this)} />
          </div>
          <div className='quest-item'>
            <span className='quest'>{'What are your personal fitness goals?'}</span>
            <Field
              name='fitness-goals'
              validate={isOtherFitnessGoal && otherFitnessGoals.length === 0 ? this.isRequired : undefined}
              component={this.renderFitnessGoals.bind(this)} />
          </div>
          <div className='input-item form-group'>
            <label className='title'>{'Why are these important to you?'}</label>
            <Field
              name='reason'
              component={this.renderField} />
          </div>
          <div className='quest-item'>
            <span className='quest'>{'How long have you been thinking about achieving these goals?'}</span>
            <Field
              name='timeAchieveGoals'
              component={this.renderThinkAchievingGoals.bind(this)} />
          </div>
          <div className='quest-item'>
            <span className='quest'>{'On a scale of 1-10, how passionate are you about achieving your goals?'}</span>
            <Field
              name='passionateAchieve'
              component={this.renderPassionateAchievingGoals.bind(this)} />

          </div>
          <div className='input-item form-group'>
            <label className='title'>{'What barriers have you had in reaching your fitness goals? Are any of these barriers still present?'}</label>
            <Field
              name='barriersOfGoals'
              component={this.renderField} />
          </div>
          <div className='quest-item'>
            <span className='quest'>{'Have you ever used personal training in the past?'}</span>
            <Field
              name='isTrainingPast'
              component={this.renderIsTrainingPast.bind(this)} />
          </div>
          <div className='input-item form-group'>
            <label className='title'><b>{'If Yes:'}</b>{' How was your experience?'}</label>
            <Field
              name='barriersExp'
              disabled={this.state.isTrainingPast === 'No' || !this.state.isTrainingPast}
              component={this.renderField} />
          </div>
          <div className='input-item form-group'>
            <label className='title'>{'When were you in the best shape of your life?'}</label>
            <Field
              name='whenInBestShape'
              component={this.renderField} />
          </div>
          <div className='input-item form-group'>
            <label className='title'>{'What were you doing at that time to be in the best shape of your life?'}</label>
            <Field
              name='doingInBestShape'
              component={this.renderField} />
          </div>
          <div className='input-item form-group'>
            <label className='title'>{'Who is your support system? Will they support you in achieving your goals?'}</label>
            <Field
              name='supportSystem'
              component={this.renderField} />
          </div>
          <div className='input-item form-group'>
            <label className='title'>{'Regarding fitness programs, what have you allocated to invest in your fitness goals on a per session basis?'}</label>
            <Field
              name='allocatedToInvest'
              type='number'
              className='allocated-to-invest'
              component={this.renderFieldAllocatedToInvest} />
          </div>
          <div className='input-item form-group'>
            <label className='title'>{'What days/times do you anticipate that you will be using the studio?'}</label>
            <Field
              name='classScheduleProvided'
              component={this.renderDayOfWeek.bind(this)} />
          </div>
        </div>
      </div>
    );
  }
  /**
   * Handle input change of other field
   * @param {*} value value of other field
   * @param {*} name field name
   */
  handleOther(event, name) {
    this.setState({
      [name]: event.target.value
    });
  }
  /**
   * Check if have any mandatory fields blank
   */
  isAbleNext() {
    const {otherFitnessGoals, isOtherFitnessGoal} = this.state;
    return isOtherFitnessGoal ? otherFitnessGoals.length > 0 : true;
  }
  render() {
    if (this.props.currentStep !== 2) {
      return null;
    }
    return (
      <div className='staff-section' id='staff-section'>
        <form className='staff-form' autoComplete='off' id='staff-form'>
          {this.renderFormInput()}
        </form>
        <WizardButton
          actions={this.props.actions}
          onSubmit={(step) => this.props.onSubmit(step)}
          isAbleNext={this.isAbleNext()}/>
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
StaffSection.propTypes = {
  actions: PropTypes.object,
  afterValid: PropTypes.func,
  change: PropTypes.any,
  currentStep: PropTypes.number,
  dispatch: PropTypes.func
};
let staffSection = reduxForm({
  form: 'staffSection' // A unique identifier for this form
})(StaffSection);

export default injectIntl(staffSection);

import React from 'react';
import PropTypes from 'prop-types';

class CheckBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPosition: null
    };
  }

  changeHandler = (value, id, name) => {
    if (value) {
      this.props.onSelect(id, name);
    } else {
    // handle de-select
      this.props.onSelect(this.state.isPosition, name);
    }
  };

  renderLableName(name) {
    let nameLabel = name;
    if (name === 'LB' || name === 'KG') {
      nameLabel = `${name}S`;
    }
    if (name === '4x') {
      nameLabel = `${name} or more `;
    }
    return nameLabel;
  }
  render() {
    const {options, name, selectedPosition, isOtherDisabled, otherName, otherValue} = this.props;
    return (
      <React.Fragment>
        {(options || []).map(option => {
          return (
            <div className='item' key={option}>
              <input
                id={`${name}${option}`}
                type='checkbox'
                name={name}
                value={option}
                checked={selectedPosition === option ? true : false}
                onChange={(e)=>{this.changeHandler(e.target.checked, option, name);}}/>
              <label className='checkbox-custom-label' htmlFor={`${name}${option}`}>
                {this.renderLableName(option)}
              </label>
              {option === 'Other' && <input type='text' className='form-control txt-other' name={otherName}
                disabled={isOtherDisabled}
                onBlur={(e) => this.props.onChangeOther(e, otherName)} defaultValue={isOtherDisabled ? '' : otherValue}/>}
            </div>
          );
        })}
      </React.Fragment>
    );
  }
}


CheckBox.propTypes = {
  checked: PropTypes.bool,
  handleChange: PropTypes.func,
  id: PropTypes.string,
  isOtherDisabled: PropTypes.bool,
  name: PropTypes.string,
  onChangeOther: PropTypes.func,
  onSelect: PropTypes.func,
  options: PropTypes.object,
  otherName: PropTypes.string,
  otherValue: PropTypes.string,
  selectedPosition: PropTypes.any
};

export default CheckBox;

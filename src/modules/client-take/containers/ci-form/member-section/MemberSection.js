import React from 'react';
import PropTypes from 'prop-types';
import {
  Field,
  reduxForm,
  initialize,
  change,
  formValueSelector
} from 'redux-form';
import { injectIntl, intlShape } from 'react-intl';
import { connect } from 'react-redux';
import ReactTelInput from 'react-telephone-input/lib/withStyles';
import moment from 'moment';
import _ from 'lodash';
import { handleSubmitFail } from 'common/utils/errorUtils';
import { phoneFormatter } from 'common/utils/validation';
import {
  isRequired,
  email,
  postalCode,
  isValidPhoneNumber,
  validateBirthDate
} from 'common/utils/validation';
import { FlagIcons } from 'modules/shared/images';
import { WizardButton, FormSelect } from 'modules/core/components';
import { SelectOptionsConstants } from 'common/constants/constants';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import momentLocaliser from 'react-widgets-moment';
import 'react-widgets/dist/css/react-widgets.css';
import CheckBox from '../checkbox/checkbox';
import { Constants } from 'common';
import './styles.scss';

let onSubmitFailHandler;

export class MemberSection extends React.Component {
  constructor(props) {
    super(props);
    momentLocaliser();
    const { intl, member } = this.props;
    this.state = {
      HeartCondition: false,
      SeizureDisorder: false,
      DizzinessOrFainting: false,
      None: false,
      Other: false,
      doctorDiagnosis: [],
      birthDay: member.birthDay,
      isPainChest: '',
      isLimitation: '',
      isMuscleProblem: '',
      isPregnant: '',
      isBloodPressure: '',
      weightMeasurement: '',
      otherDoctorDiagnosis: '',
      isFlag: true
    };
    // Init validation function
    this.isRequired = isRequired(
      intl.formatMessage({ id: 'Validation.RequiredField' })
    );
    this.isRequiredPhone = isRequired(
      intl.formatMessage({ id: 'Validation.RequiredField' }),
      true
    );
    this.isEmail = email(intl.formatMessage({ id: 'Validation.InvalidEmail' }));
    this.isPostalCode = postalCode();
    this.phone = isValidPhoneNumber(
      intl.formatMessage({ id: 'ClientIntake.Error.PhoneNumber' })
    );
    this.validateBirthDate = validateBirthDate(
      intl.formatMessage({ id: 'ClientIntake.Format.Date' })
    );
    this.checkIsValidate = this.checkIsValidate.bind(this);
    onSubmitFailHandler = handleSubmitFail.bind(this);
  }

  componentDidMount() {
    let { member, dispatch } = this.props;
    if (!member) { return; }
    dispatch(initialize('memberSection', {
      address: (member.addresses[0] || {}).address1,
      name: `${member.firstName} ${member.lastName}`,
      birthDay: member.birthDay ? moment(member.birthDay) : moment(new Date()),
      gender: member.gender,
      email: member.email,
      weight: member.weight,
      height: member.height,
      heightFeet: member.height && member.heightMeasure === Constants.HeightMeasurement.Imperial ? Math.floor(member.height / 12) : undefined,
      heightInch: member.height && member.heightMeasure === Constants.HeightMeasurement.Imperial ? member.height % 12 : undefined,
      heightCm: member.height && member.heightMeasure === Constants.HeightMeasurement.Metric ? member.height : '',
      city: (member.addresses[0] || {}).suburb,
      state: (member.addresses[0] || {}).territory,
      phoneNumber: member.phoneNumber || '1',
      emergencyNumber: (member.emergencyContacts[0] || {}).contactPhone || '1',
      isMuscleProblem: '',
      isPregnant: '',
      isPainChest: '',
      isBloodPressure: '',
      isLimitation: '',
      weightMeasurement: member.weightMeasure ? member.weightMeasure : '',
      maritalStatus: Constants.MaritalStatusType.Single,
      postalCode: (member.addresses[0] || {}).postalCode,
      emergencyRelationship: (member.emergencyContacts[0] || {}).contactRelationship || 'parent',
      emergencyName: (member.emergencyContacts[0] || {}).contact
    }));
  }

  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(newProps) {
    if (
      newProps &&
      newProps.birthDay &&
      newProps.birthDay !== this.props.birthDay
    ) {
      this.setState({
        birthDay: newProps.birthDay
      });
    }
  }
  validate() {
    this.props.afterValid(this.state);
  }

  /**
   * Check if have any mandatory fields blank
   */
  isAbleNext() {
    const {
      confirmLimitation,
      isLimitation,
      name,
      phoneNumber,
      emergencyNumber,
      emergencyName,
      weight,
      heightFeet,
      zip,
      city,
      state,
      address,
      isPainChest,
      heightCm,
      listAnswer,
      isBloodPressure,
      weightMeasurement,
      birthDay
    } = this.props;
    let isBirthDay = moment(birthDay).format('MM/DD/YYYY');
    return (
      ((confirmLimitation || '').length > 0 || isLimitation === 'No') &&
      (name || '').length > 0 &&
      (address || '').length > 0 &&
      (city || '').length > 0 &&
      (state || '').length > 0 &&
      (zip || '').length > 0 &&
      String(weight || '').length > 0 &&
      ((weightMeasurement === Constants.WeightMeasurement.Imperial &&
        String(heightFeet || '').length > 0) ||
        (weightMeasurement === Constants.WeightMeasurement.Metric &&
          String(heightCm || '').length > 0)) &&
      (zip || '').length > 0 &&
      !this.isPostalCode(zip) &&
      (emergencyName || '').length > 0 &&
      _.isUndefined(this.phone(phoneNumber)) &&
      _.isUndefined(this.phone(emergencyNumber)) &&
      (phoneNumber || '').length > 2 &&
      (emergencyNumber || '').length > 2 &&
      (this.state.doctorDiagnosis || []).length > 0 &&
      ((this.state.doctorDiagnosis || []).includes('Other')
        ? (this.state.otherDoctorDiagnosis || '').length > 0
        : true) &&
      (isPainChest || '').length > 0 &&
      (weightMeasurement || '').length > 0 &&
      (this.state.isMuscleProblem || '').length > 0 &&
      (this.state.isPregnant || '').length > 0 &&
      ((listAnswer || '').length > 0 || isBloodPressure === 'No') &&
      isBirthDay !== '01/01/1900'
    );
  }
  handleCheckbox(key) {
    let { doctorDiagnosis } = this.state;
    this.setState(
      prevState => ({
        [key]: !prevState[key]
      }),
      () => {
        if (this.state[key]) {
          if (key === 'None') {
            _.remove(doctorDiagnosis, function(element) {
              return element !== key;
            });
            this.setState({
              HeartCondition: false,
              SeizureDisorder: false,
              DizzinessOrFainting: false,
              Other: false,
              otherDoctorDiagnosis: ''
            });
          }
          doctorDiagnosis.push(key);
        } else {
          _.remove(doctorDiagnosis, function(element) {
            return element === key;
          });
          if (key === 'Other') {
            this.setState({
              otherDoctorDiagnosis: ''
            });
          }
        }

        this.setState(
          {
            doctorDiagnosis
          },
          () => {
            this.props.dispatch(
              change('memberSection', 'doctorDiagnosis', doctorDiagnosis)
            );
          }
        );
      }
    );
  }

  renderField({
    input,
    type,
    options,
    meta: { touched, error },
    disabled,
    className,
    autoComplete,
    autoFocus
  }) {
    return (
      <div className={`input-form${touched && error ? ' has-error' : ''}`}>
        <input
          className={`form-control ${className}`}
          {...input}
          {...options}
          type={type}
          disabled={disabled}
          autoFocus={autoFocus}
          autoComplete={autoComplete}/>
        {touched &&
          (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  getAddress(arrAddress) {
    let temp;
    if (!arrAddress) {
      return null;
    }
    let result = arrAddress.filter(function(address) {
      return address.type === 'Home';
    });
    for (let i = 0; i < result.length; i++) {
      temp = result[i].address1 ? result[i].address1 : result[i].address2;
      if (temp) {
        return temp;
      }
    }
    return null;
  }

  getTypeAddress(arrAddress, typeAddress) {
    if (!arrAddress) {
      return null;
    }
    for (let i = 0; i < arrAddress.length; i++) {
      if (arrAddress[i][typeAddress]) {
        return arrAddress[i][typeAddress];
      }
    }
    return null;
  }

  renderFieldHeight({
    input,
    type,
    options,
    meta: { touched, error },
    disabled,
    className,
    autoComplete,
    unit
  }) {
    return (
      <div className='input-form'>
        <div
          className={`input-form${
            !disabled && touched && error ? ' has-error' : ''
          } height`}>
          <input
            className={`form-control ${className}`}
            {...input}
            {...options}
            type={type}
            disabled={disabled}
            autoComplete={autoComplete}/>
          {unit && <p className={`unit ${className}`}>{unit}</p>}
        </div>
        {!disabled &&
          touched &&
          (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  /**
   * This function will be wrapped by Redux Form
   * Render gender field
   */
  renderGenderField({ input: { value, onChange } }) {
    return (
      <div className='gender-fields'>
        <input
          type='radio'
          id='male'
          name='gender'
          value={'Male'}
          checked={value ? value === 'Male' : true}
          onChange={() => {
            onChange('Male');
          }}/>
        <label className='radio-inline' htmlFor='male'>
          <span />
          {'Male'}
        </label>

        <input
          type='radio'
          id='female'
          name='gender'
          value={'Female'}
          checked={value === 'Female'}
          onChange={() => {
            onChange('Female');
          }}/>
        <label className='radio-inline' htmlFor='female'>
          <span />
          {'Female'}
        </label>
      </div>
    );
  }
  renderUnitField() {
    return (
      <div className='unit-checkbox'>
        <CheckBox
          name='weightMeasurement'
          selectedPosition={this.props.weightMeasurement}
          options={SelectOptionsConstants.unitMeasurement}
          onSelect={this.changeHandler}/>
      </div>
    );
  }

  /**
   * This function will be wrapped by Redux Form
   * Render marital field
   */
  renderMaritalField({ input: { value, onChange } }) {
    return (
      <div className='marital-fields'>
        <input
          type='radio'
          id='single'
          name='single'
          value={'Single'}
          checked={value ? value === 'Single' : true}
          onChange={() => {
            onChange('Single');
          }}/>
        <label className='radio-inline' htmlFor='single'>
          <span />
          {'Single'}
        </label>

        <input
          type='radio'
          id='married'
          name='married'
          value={'Married'}
          checked={value === 'Married'}
          onChange={() => {
            onChange('Married');
          }}/>
        <label className='radio-inline' htmlFor='married'>
          <span />
          {'Married'}
        </label>
      </div>
    );
  }

  renderIntelInput({
    input,
    input: { onChange, onBlur, value, options },
    meta: { touched, error, visited, invalid }
  }) {
    return (
      <div
        className={`input-form${
          (!visited && invalid) ||
          (touched && value.length === 1) ||
          (touched && error)
            ? ' has-error'
            : ''
        }`}>
        <ReactTelInput
          classNames='content form-control'
          {...input}
          {...options}
          defaultCountry='us'
          flagsImagePath={FlagIcons}
          onBlur={() => onBlur(value)}
          onChange={onChange}/>
        {((!visited && invalid) ||
          (touched && value.length === 1) ||
          (touched && error)) && (
          <span className='text-danger-tooltip'>{error}</span>
        )}
      </div>
    );
  }

  renderDateTimePicker({
    options,
    input: { onChange, onBlur, value },
    meta: { touched, error, visited, invalid }
  }) {
    return (
      <div
        className={`input-form${
          (!visited && invalid) || (touched && error) ? ' has-error' : ''
        }`}>
        <DateTimePicker
          {...options}
          onChange={onChange}
          onBlur={() => onBlur(value)}
          format={'MM/DD/YYYY'}
          time={false}
          value={!value ? null : new Date(moment(value))}/>
        {touched &&
          (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  handleOtherHealth(event) {
    this.setState({
      otherDoctorDiagnosis: event.target.value
    });
  }
  /**
   * Render check box
   *
   * @param {any} { input: { onChange } }
   * @returns
   * @memberof ChallengeForm
   */
  renderHealthCheckBox({
    input,
    type,
    options,
    meta: { touched, error },
    disabled,
    className,
    autoComplete,
    autoFocus
  }) {
    return (
      <div className='health-checkbox'>
        <div className='item'>
          <input
            type='checkbox'
            id='HeartCondition'
            name='doctorDiagnosis'
            checked={this.state.HeartCondition && !this.state.None}
            onChange={() => this.handleCheckbox.bind(this)('HeartCondition')}
            disabled={this.state.None}/>
          <label className='checkbox-custom-label' htmlFor='HeartCondition'>
            {'Heart Condition'}
          </label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='SeizureDisorder'
            checked={this.state.SeizureDisorder && !this.state.None}
            s={true}
            name='doctorDiagnosis'
            onChange={() => this.handleCheckbox.bind(this)('SeizureDisorder')}
            disabled={this.state.None}/>
          <label className='checkbox-custom-label' htmlFor='SeizureDisorder'>
            {'Seizure Disorder'}
          </label>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='DizzinessOrFainting'
            name='doctorDiagnosis'
            checked={this.state.DizzinessOrFainting && !this.state.None}
            onChange={() =>
              this.handleCheckbox.bind(this)('DizzinessOrFainting')}
            disabled={this.state.None}/>
          <label
            className='checkbox-custom-label'
            htmlFor='DizzinessOrFainting'>
            {'Dizziness Or Fainting (Syncopy)'}
          </label>
        </div>
        <div className='item input-item'>
          <input
            type='checkbox'
            id='OtherHealth'
            name='doctorDiagnosisOther'
            checked={this.state.Other && !this.state.None}
            onChange={() => this.handleCheckbox.bind(this)('Other')}
            disabled={this.state.None}/>
          <label className='checkbox-custom-label' htmlFor='OtherHealth'>
            {'Other'}
          </label>
          <div
            className={`input-form${
              error ? ' has-error' : ''
            } txt-other-health`}>
            <input
              type='text'
              id='otherHealthTxt'
              name='doctorDiagnosisTxt'
              className='form-control'
              defaultValue={this.state.otherDoctorDiagnosis}
              disabled={!this.state.Other}
              onBlur={this.handleOtherHealth.bind(this)}/>
            {error && <span className='text-danger-tooltip'>{error}</span>}
          </div>
        </div>
        <div className='item'>
          <input
            type='checkbox'
            id='None'
            name='doctorDiagnosis'
            checked={this.state.None}
            onChange={() => this.handleCheckbox.bind(this)('None')}/>
          <label className='checkbox-custom-label' htmlFor='None'>
            {'None of the above'}
          </label>
        </div>
      </div>
    );
  }

  /**
   * Render check box
   *
   * @param {any} { input: { onChange } }
   * @returns
   * @memberof ChallengeForm
   */
  changeHandler = (key, name) => {
    let newState = { [name]: key };
    if (name === 'isBloodPressure') {
      this.props.change('listAnswer', '');
    } else if (name === 'isLimitation') {
      this.props.change('confirmLimitation', '');
    }
    this.setState(newState, () =>
      this.props.dispatch(change('memberSection', name, newState[name]))
    );
  };

  renderChestCheckBox() {
    return (
      <div className='health-checkbox'>
        <CheckBox
          name='isPainChest'
          selectedPosition={this.state.isPainChest}
          options={SelectOptionsConstants.isYesNo}
          onSelect={this.changeHandler}/>
      </div>
    );
  }
  renderConfirmLimitationCheckBox() {
    return (
      <div className='health-checkbox'>
        <CheckBox
          name='isLimitation'
          selectedPosition={this.state.isLimitation}
          options={SelectOptionsConstants.isYesNo}
          onSelect={this.changeHandler}/>
      </div>
    );
  }
  renderMuscleCheckBox() {
    return (
      <div className='health-checkbox'>
        <CheckBox
          name='isMuscleProblem'
          selectedPosition={this.state.isMuscleProblem}
          options={SelectOptionsConstants.isYesNo}
          onSelect={this.changeHandler}/>
      </div>
    );
  }

  renderPregnantCheckBox() {
    return (
      <div className='health-checkbox'>
        <CheckBox
          name='isPregnant'
          selectedPosition={this.state.isPregnant}
          options={SelectOptionsConstants.isYesNoPregnant}
          onSelect={this.changeHandler}/>
      </div>
    );
  }

  renderSupportedHRCheckBox() {
    return (
      <div className='health-checkbox'>
        <CheckBox
          name='isBloodPressure'
          selectedPosition={this.state.isBloodPressure}
          options={SelectOptionsConstants.isYesNo}
          onSelect={this.changeHandler}/>
      </div>
    );
  }
  /**
   * Render form input (user name and password) based on login type
   */
  changeBirthDate(newDate) {
    this.setState({
      birthDay: newDate
    });
  }
  renderFormInput() {
    const {
      isDoctor,
      isPaint,
      isLimitation,
      isMuscleProblem,
      isPregnant,
      isBloodPressure,
      weightMeasurement,
      isWeightMeasurement,
      listAnswer,
      confirmLimitation,
      birthDay
    } = this.onCreateEvent();
    const { isFlag } = this.state;
    return (
      <div className='form-container'>
        <div className='basic-info'>
          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'Client Name'}
            </label>
            <Field
              name='name'
              ref={'name'}
              component={this.renderField}
              validate={[this.isRequired]}/>
          </div>
          <div className='input-item form-group'>
            <label className='title'>{'Employer'}</label>
            <Field name='employer' component={this.renderField} />
          </div>
          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'Address'}
            </label>
            <Field
              name='address'
              ref={'address'}
              validate={this.isRequired}
              component={this.renderField}/>
          </div>
          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'City'}
            </label>
            <Field
              name='city'
              ref={'city'}
              validate={this.isRequired}
              component={this.renderField}/>
          </div>
          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'State'}
            </label>
            <Field
              name='state'
              ref={'state'}
              autoFocus={true}
              validate={this.isRequired}
              options={SelectOptionsConstants.state}
              component={FormSelect}/>
          </div>
          <div className='input-item form-group'>
            <label className={'title'}>
              <span className='red-star'>{'*'}</span>
              {'Zip'}
            </label>
            <Field
              name='postalCode'
              ref={'postalCode'}
              validate={[this.isPostalCode, this.isRequired]}
              component={this.renderField}/>
          </div>
          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'Date Of Birth'}
            </label>
            <Field
              name='birthDay'
              ref='birthDay'
              autoFocus={true}
              validate={[this.validateBirthDate, this.isRequired]}
              intl={this.props.intl}
              component={this.renderDateTimePicker}
              onChange={this.changeBirthDate.bind(this)}/>
          </div>
          <div className='input-item form-group nested'>
            <div className='nested-item form-group'>
              <label className='title'>{'Gender'}</label>
              <Field
                name='gender'
                ref={'gender'}
                validate={this.isRequired}
                component={this.renderGenderField}/>
            </div>
            <div className='nested-item form-group'>
              <label className='title'>{'Marital Status'}</label>
              <Field name='maritalStatus' component={this.renderMaritalField} />
            </div>
          </div>
          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'Email'}
            </label>
            <Field
              name='email'
              type='email'
              ref='email'
              component={this.renderField}
              disabled={true}
              validate={[this.isEmail, this.isRequired]}/>
          </div>

          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'Phone'}
            </label>
            <Field
              name='phoneNumber'
              ref={'phoneNumber'}
              format={phoneFormatter}
              validate={[this.phone, this.isRequiredPhone]}
              intl={this.props.intl}
              component={this.renderIntelInput}/>
          </div>
          <div className='input-item form-group'>
            <div className='input-item form-group nested'>
              <div className='nested-item form-group'>
                <label className='title'>
                  <span className='red-star'>{'*'}</span>
                  {'Emergency Contact Name'}
                </label>
                <Field
                  name='emergencyName'
                  type='text'
                  ref={'emergencyName'}
                  validate={this.isRequired}
                  component={this.renderField}/>
              </div>
              <div className='nested-item form-group'>
                <label className='title'>
                  {'Emergency Contact Relationship'}
                </label>
                <Field
                  name='emergencyRelationship'
                  validate={this.isRequired}
                  autoFocus={true}
                  options={SelectOptionsConstants.relationship}
                  searchable={false}
                  component={FormSelect}/>
              </div>
            </div>
          </div>
          <div className='input-item form-group'>
            <label className='title'>
              <span className='red-star'>{'*'}</span>
              {'Emergency Contact Number'}
            </label>
            <Field
              name='emergencyNumber'
              ref={'emergencyNumber'}
              autoFocus={true}
              format={phoneFormatter}
              validate={[this.phone, this.isRequiredPhone]}
              intl={this.props.intl}
              component={this.renderIntelInput}/>
          </div>
          <div className='input-item form-group nested'>
            <div className='nested-item form-group'>
              <label
                className={`title${
                  isFlag || isWeightMeasurement ? '' : '-none'
                }`}>
                <span className='red-star'>{'*'}</span>
                {'Unit of measurement'}
              </label>
              <Field
                name='weightMeasurement'
                ref='weightMeasurement'
                autoFocus={true}
                component={this.renderUnitField.bind(this)}/>
            </div>
            <div className='nested-item form-group'>
              <label className='title'>
                <span className='red-star'>{'*'}</span>
                {'Weight'}
              </label>
              <Field
                name='weight'
                ref='weight'
                type='number'
                component={this.renderField}
                validate={this.isRequired}/>
            </div>
          </div>
          <div className='input-item form-group nested'>
            <div className='nested-item form-group'>
              <label className='title'>
                <span className='red-star'>{'*'}</span>
                {'Height'}
              </label>
              <Field
                name={this.props.weightMeasurement === 'KG'
                  ? 'heightCm'
                  : 'heightFeet'}
                type='number'
                className='height-feet'
                autoFocus={true}
                unit={this.props.weightMeasurement === 'KG' ? 'centimeters' : 'feet'}
                component={this.renderFieldHeight}
                validate={this.isRequired}/>
            </div>
            {this.props.weightMeasurement !== 'KG' && (
              <div className='nested-item form-group'>
                <label className='title' />
                <Field
                  name='heightInch'
                  type='number'
                  autoFocus={true}
                  validate={this.isRequired}
                  unit='inches'
                  className='height-inch'
                  component={this.renderFieldHeight}/>
              </div>
            )}
          </div>
        </div>
        <div className='additional-questions'>
          <div className='quest-item'>
            <span className={`quest${isFlag || isDoctor ? '' : '-none'}`}>
              <span className='red-star'>{'*'}</span>
              {'Has your doctor ever told you that you have a:'}
            </span>
            <Field
              name='healthCheckbox'
              validate={this.state.Other && this.state.otherDoctorDiagnosis.length === 0
                ? this.isRequired
                : undefined}
              component={this.renderHealthCheckBox.bind(this)}/>
          </div>
          <div className='quest-item long-quest'>
            <span className={`quest${isFlag || isPaint ? '' : '-none'}`}>
              <span className='red-star'>{'*'}</span>
              {
                'Do you feel pain in your chest during physical activity or at any other time?'
              }
            </span>
            <Field
              name='isPainChest'
              component={this.renderChestCheckBox.bind(this)}/>
          </div>
          <div className='quest-item long-quest'>
            <span className={`quest${isFlag || isLimitation ? '' : '-none'}`}>
              <span className='red-star'>{'*'}</span>
              {
                'Are there any other reasons that you should not perform, or limitations that could prevent your from performing physical activity?'
              }
            </span>
            <Field
              name='isLimitation'
              component={this.renderConfirmLimitationCheckBox.bind(this)}/>
          </div>
          <div>
            <span
              className={`quest${
                this.props.isLimitation === 'Yes' && !confirmLimitation
                  ? '-red'
                  : ''
              }`}>
              <span className='red-star'>{'*'}</span>
              {'If yes please explain below'}
            </span>
            <Field
              className='quest-answer'
              name='confirmLimitation'
              autoFocus={true}
              disabled={this.props.isLimitation !== 'Yes'}
              component={this.renderField}
              autoComplete='off'/>
          </div>
          <div className='quest-item long-quest'>
            <span
              className={`quest${isFlag || isMuscleProblem ? '' : '-none'}`}>
              <span className='red-star'>{'*'}</span>
              {
                'Do you have any muscle, tendon, ligament, bone or joint problems that will be exacerbated by increase in activity?'
              }
            </span>
            <Field
              name='isMuscleProblem'
              component={this.renderMuscleCheckBox.bind(this)}/>
          </div>
          <div className='quest-item'>
            <span className={`quest${isFlag || isPregnant ? '' : '-none'}`}>
              <span className='red-star'>{'*'}</span>
              {'Are you currently pregnant?'}
            </span>
            <Field
              name='isPregnant'
              component={this.renderPregnantCheckBox.bind(this)}/>
          </div>
          <div className='quest-item long-quest'>
            <span
              className={`quest${isFlag || isBloodPressure ? '' : '-none'}`}>
              <span className='red-star'>{'*'}</span>
              {
                'Are you currently taking any medications or supplements that affect your heart rate or blood pressure? '
              }
            </span>
            <Field
              name='isBloodPressure'
              component={this.renderSupportedHRCheckBox.bind(this)}/>
          </div>
          <div>
            <span
              className={`quest${
                isBloodPressure === 'Yes' && !listAnswer ? '-red' : ''
              }`}>
              <span className='red-star'>{'*'}</span>
              {'If yes to the above question, please list:'}
            </span>
            <Field
              className='quest-answer'
              name='listAnswer'
              autoFocus={true}
              disabled={this.state.isBloodPressure !== 'Yes'}
              component={this.renderField}
              autoComplete='off'/>
          </div>
          <div>
            <b className='notes'>
              {
                'I acknowledge that if I answered yes to any of the above questions that are noted with an asterisk (*), a written physician approval must be obtained.'
              }
            </b>
          </div>
        </div>
      </div>
    );
  }

  checkIsValidate() {
    this.setState({
      isFlag: false
    });
  }
  onCreateEvent() {
    let isDoctor =
      (this.state.doctorDiagnosis || []).length > 0 ||
      ((this.state.doctorDiagnosis || []).includes('Other') &&
        (this.state.otherDoctorDiagnosis || '').length > 0);
    let isPaint = (this.state.isPainChest || '').length > 0;
    let isLimitation = this.state.isLimitation;
    let isPregnant = (this.state.isPregnant || '').length > 0;
    let isMuscleProblem = (this.state.isMuscleProblem || '').length > 0;
    let isBloodPressure = this.state.isBloodPressure;
    let isWeightMeasurement =
      (this.state.weightMeasurement || '').length > 0 ||
      this.props.weightMeasurement;
    let listAnswer = (this.props.listAnswer || '').length > 0;
    let confirmLimitation = (this.props.confirmLimitation || '').length > 0;
    let birthDay =
      moment(this.props.birthDay).format('MM/DD/YYYY') === '01/01/1990';
    return {
      isDoctor,
      isPaint,
      isLimitation,
      isMuscleProblem,
      isPregnant,
      isBloodPressure,
      isWeightMeasurement,
      listAnswer,
      confirmLimitation,
      birthDay
    };
  }
  render() {
    if (this.props.currentStep !== 1) {
      return null;
    }
    const { handleSubmit } = this.props;
    return (
      <div className='member-section' id='member-section'>
        <form className='member-form' autoComplete='off'>
          {this.renderFormInput()}
        </form>
        <WizardButton
          actions={this.props.actions}
          onSubmit={step => this.props.onSubmit(step)}
          isAbleNext={this.isAbleNext()}
          checkIsValidate={this.checkIsValidate}
          onCreateEvent={handleSubmit(this.onCreateEvent)}/>
      </div>
    );
  }
}

const isEmpty = value => value === undefined || value === null || value === '';

const myValidators = values => {
  const errors = {};
  const { address, birthDay, weight, state } = values;
  if (isEmpty(address)) {
    errors.address = 'This field is required!';
  }
  if (!birthDay || moment(birthDay).format('MM/DD/YYYY') === '01/01/1900') {
    errors.birthDay = 'This field is required!';
  }
  if (isEmpty(weight) || weight === '0' || weight === 0) {
    errors.weight = 'This field is required!';
  }
  if (isEmpty(state) || state === '0') {
    errors.state = 'This field is required!';
  }

  return errors;
};
/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
MemberSection.propTypes = {
  actions: PropTypes.object,
  afterValid: PropTypes.func,
  change: PropTypes.func,
  currentStep: PropTypes.number,
  dispatch: PropTypes.func,
  handleSubmit: PropTypes.any,
  intl: intlShape.isRequired,
  invalid: PropTypes.bool,
  member: PropTypes.object,
  touch: PropTypes.func,
  weightMeasurement: PropTypes.string // Marks the given fields as "touched" to show errors
};

let memberSection = reduxForm({
  form: 'memberSection',
  validate: myValidators,
  onSubmitFail: errors => {
    if (onSubmitFailHandler) {
      onSubmitFailHandler(errors);
    }
  }
})(MemberSection);
const selector = formValueSelector('memberSection');

memberSection = connect(state => ({
  member: state.clientTake_members.memberDetail,
  confirmLimitation: selector(state, 'confirmLimitation'),
  listAnswer: selector(state, 'listAnswer'),
  weightMeasurement: selector(state, 'weightMeasurement'),
  isLimitation: selector(state, 'isLimitation'),
  isPainChest: selector(state, 'isPainChest'),
  isBloodPressure: selector(state, 'isBloodPressure'),
  addressLine1: selector(state, 'addressLine1'),
  address: selector(state, 'address'),
  city: selector(state, 'city'),
  name: selector(state, 'name'),
  healthCheckbox: selector(state, 'healthCheckbox'),
  phoneNumber: selector(state, 'phoneNumber'),
  emergencyNumber: selector(state, 'emergencyNumber'),
  emergencyName: selector(state, 'emergencyName'),
  weight: selector(state, 'weight'),
  height: selector(state, 'height'),
  zip: selector(state, 'postalCode'),
  email: selector(state, 'email'),
  state: selector(state, 'state'),
  heightFeet: selector(state, 'heightFeet'),
  heightInch: selector(state, 'heightInch'),
  heightCm: selector(state, 'heightCm'),
  birthDay: selector(state, 'birthDay')
}))(memberSection);

export default injectIntl(memberSection);

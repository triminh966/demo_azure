import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { injectIntl, intlShape } from 'react-intl';
import { connect } from 'react-redux';
import { isRequired } from 'common/utils/validation';
import { WizardButton } from 'modules/core/components';
import { Calendar, AppSignaturePad } from 'modules/core/components';
import { Helpers } from 'common';
import './styles.scss';

export class PolicySection extends React.Component {
  constructor(props) {
    super(props);
    this.signature = React.createRef();
    this.state = {
      memberSignature: '',
      staffSignature: '',
      parentSignature: '',
      isChecked: false
    };
    this.isRequired = isRequired(this.props.intl.formatMessage({ id: 'Validation.RequiredField' }));
  }

  validate() {
    this.props.afterValid(this.state);
  }
  renderSignature(name) {
    return (
      <div className={'form-control sig'}>
        <AppSignaturePad onEnd={(signaturePad) => this.onEnd.bind(this)(signaturePad, name)}
          onClear={(signaturePad) => this.onEnd.bind(this)(signaturePad, name)}
          data={this.state[name]} />
      </div>
    );
  }
  onEnd(signaturePad, name) {
    this.setState({
      [name]: signaturePad.toDataURL()
    }, () => {
      this.props.actions.clientTake_members.saveSignature(signaturePad, name);
    });
  }
  renderField({ input, type, options, meta: { touched, error }, className, id }) {
    return (
      <div className={`input-form${touched && error ? ' has-error' : ''}`}>
        <input
          name={id}
          className={`form-control ${className}`}
          {...input}
          {...options}
          type={type} />
        {touched && (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  handleCheckBox() {
    this.setState({
      isChecked: !this.state.isChecked
    });
  }

  renderAgreeCheckbox() {
    return (<div className='control'>
      <input
        type='checkbox'
        id='agreeTo'
        checked={this.state.isChecked === true}
        onChange={this.handleCheckBox.bind(this)} />
      <label className='checkbox-custom-label' htmlFor='agreeTo'>{'I agree to'}</label>
    </div>);
  }

  render() {
    const { studio, invalid, memberInfo } = this.props;

    const { isChecked } = this.state;
    if (this.props.currentStep !== 3) {
      return null;
    }
    const { studio: { StudioState, RegistrationHSNo } } = this.props;
    let renderMessages = <b>{'You acknowledge and agree that neither UFG, nor any of UFG’s past, present, or future subsidiaries, successors, assigns, or affiliates, or their respective officers, directors, incorporators, members, partners, owners, agents, management, controlling parties, entities under common control, vendors, service providers, investors, attorneys, employees, or representatives (together with UFG, collectively the “UFG Parties”), is a party to this agreement.  You understand and agree that the UFG Parties are not responsible for any acts or omissions related in any way to this agreement, the goods or services provided to you hereunder, or for any act or omission by us or any other OTF studio or any employee, agent or representative of any OTF studio.'}</b>;
    if (studio.IsCorporate) {
      renderMessages = <b>{'You acknowledge and agree that neither UFG, nor any of UFG’s past, present, or future subsidiaries, successors, assigns, or affiliates other than OTF Studios, LLC, or their respective officers, directors, incorporators, members, partners, owners, agents, management, controlling parties, entities under common control, vendors, service providers, investors, attorneys, employees, or representatives (together with UFG, collectively the “UFG Parties”), is a party to this agreement.  You understand and agree that the UFG Parties are not responsible for any acts or omissions related in any way to this agreement, the goods or services provided to you hereunder, or for any act or omission by us or any other OTF studio or any employee, agent or representative of any OTF studio'}</b>;
    }
    return (
      <div className='policy-section' id={'policy-section'}>
        {/* <p className='label-page'>
          {'Studio Policies'}
        </p> */}
        <form className='policy-wrapper'>
          <div className='policy-content'>
            <div className='item item-control'>
              <Field
                name='agreeTo'
                component={this.renderAgreeCheckbox.bind(this)} />
              <div className='btn-collapse' data-toggle='collapse'
                data-target='#policy'>{'Studio Policy'} <span className='red-star'>{'*'}</span></div>
            </div>
            <div id='policy' className='collapse'>
              <div className='title-text'>
                <h1>{'TERMS AND CONDITIONS:'}</h1>
              </div>
              <div className='policy'>
                <p className={((StudioState === 'Texas') || (StudioState === 'TX')) ? 'title1' : ''}><u><b>{'Assumption of Risk, Release, Waiver of Liability, and Indemnification:'}</b></u></p>
                <div>
                  {`We urge you and all clients to obtain a physical examination from a physician prior to initiating any exercise program. Orangetheory® Fitness (“OTF”) classes are not designed for individuals with known heart disease with or without functional impairment.`}{((StudioState === 'Texas') || (StudioState === 'TX')) ? <b>{` You understand and agree that there is a risk of injury associated with participation in any exercise program and that there exists the possibility for certain conditions occurring during or following training and/or exercise.  In recognition of the possible dangers connected with any physical activity, by signing below, you understand, acknowledge, agree, and hereby voluntarily accept all risk and responsibility associated with the services provided and use of any of the facilities at any OTF studio.`}</b> : ' You understand and agree that there is a risk of injury associated with participation in any exercise program and that there exists the possibility for certain conditions occurring during or following training and/or exercise.  In recognition of the possible dangers connected with any physical activity, by signing below, you understand, acknowledge, agree, and hereby voluntarily accept all risk and responsibility associated with the services provided and use of any of the facilities at any OTF studio.'}{` You acknowledge that it is your responsibility to disclose any medical condition or medication that could limit or prevent you from performing physical activity.  You acknowledge that we may require you to provide written physician approval before you may use or participate in any physical activity at the Studio.  You acknowledge that you may decrease or stop at any time any physical activity you perform at the Studio and that it is your obligation to inform the Studio’s staff of any medical symptoms or issues that arise while at the Studio.`}{((StudioState === 'Texas') || (StudioState === 'TX')) ? <b>{` You hereby waive all claims, assume all liability, and release, hold harmless, indemnify, and agree to defend us, Ultimate Fitness Group, LLC, the franchisor of OTF ${studio.IsCorporate ? '' : 'and the entity who granted us contractual authority to independently own and operate our franchised location'} (“UFG”), the UFG Parties (defined below), any other OTF studio, any owner of any other OTF studio you may visit, and any of our or their respective affiliates, successors, assigns, agents, representatives, and employees, from liability for any injury, claim, cause of action, suit, demand, and damages (including, without limitation, personal, bodily, or mental injury, property damage, economic loss, consequential damages, and punitive damages), arising from or related to (1) your failure to disclose any pre-existing conditions, limitations, or sensitivities; (2) your presence on the premises of any OTF studio; (3) your participation in any OTF class or use of any equipment at any OTF studio; and/or (4) any negligence on our part (including our employees) or on the part of any employee at any other OTF studio.`}</b> : ` You hereby waive all claims, assume all liability, and release, hold harmless, indemnify, and agree to defend us, Ultimate Fitness Group, LLC, the franchisor of OTF ${studio.IsCorporate ? '' : 'and the entity who granted us contractual authority to independently own and operate our franchised location'} (“UFG”), the UFG Parties (defined below), any other OTF studio, any owner of any other OTF studio you may visit, and any of our or their respective affiliates, successors, assigns, agents, representatives, and employees, from liability for any injury, claim, cause of action, suit, demand, and damages (including, without limitation, personal, bodily, or mental injury, property damage, economic loss, consequential damages, and punitive damages), arising from or related to (1) your failure to disclose any pre-existing conditions, limitations, or sensitivities; (2) your presence on the premises of any OTF studio; (3) your participation in any OTF class or use of any equipment at any OTF studio; and/or (4) any negligence on our part (including our employees) or on the part of any employee at any other OTF studio.`}{` You further expressly agree that this Assumption of Risk, Release, Waiver of Liability, and Indemnification is intended to be as broad and inclusive as permitted by law, that you are aware of Section 1542 of the California Civil Code, and that you expressly agree to waive the protections, rights, and benefits you may have under Section 1542 relating to this Assumption of Risk, Release, Waiver of Liability, and Indemnification.  Section 1542 states: “A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS OR HER FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM OR HER MUST HAVE MATERIALLY AFFECTED HIS OR HER SETTLEMENT WITH THE DEBTOR.”  You expressly agree that if any portion of this Assumption of Risk, Release, Waiver of Liability, and Indemnification is held invalid, the balance shall be valid and continue in full legal force and effect. These provisions are binding on you, your estate, family, heirs, administrators, personal representatives, and assigns.`}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Use of Personal Information, Image, Likeness, and/or Voice:'}</b></u></p>
                <div>
                  {'We may photograph, record on audio or video, or otherwise record OTF classes or client use of the Studio.  In exchange for your use of the Studio or your participation in any OTF class, you understand, acknowledge, and agree that you may be photographed, recorded on audio or video, or otherwise recorded while at the Studio, and hereby agree and consent for all purposes to the sale, reproduction, and/or use in any manner of any such photograph, audio, video, or other recording or depiction of your likeness and/or voice whatsoever by us, any OTF studio, UFG, the UFG Parties (defined below), and any nominee or designee of us or them, including without limitation any agency, client, periodical or other publication, in all forms of media, whether now or hereafter devised, throughout the world and in perpetuity, and in all manners, including without limitation advertising, trade, display, editorial, art and exhibition. You further understand and agree that any such photograph, audio, video, or other recording or depiction of your likeness and/or voice may be modified, altered, cropped and combined with other content such as images, video, audio, text and graphics, and hereby waive any right that you may have to inspect or approve any finished image, video, or audio containing a depiction of your likeness or voice.  You further agree that the Studio, any other OTF studio, UFG, and/or the UFG Parties (defined below), may use any information gathered in this form or through your use of the Studio or participation in any OTF class, provided the information does not personally identify you or provide facts that could lead to your identification, for any purpose, including without limitation research, product and program improvements, and statistical purposes.  You agree to hold harmless and indemnify the Studio, any other OTF studio, UFG, and the UFG Parties (defined below), from and against any and all liability, damage, loss and/or claims of any kind or nature whatsoever, including, without limitation, any and all claims and demands relating to libel, invasion of privacy, and violation of publicity rights.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Authorization for Phone Calls and Text Messages:'}</b></u></p>
                <div>
                  {'By providing your phone number above and signing below, you consent to and authorize the Studio, UFG, the UFG Parties (defined below), and/or their agents to call or send you text messages to the number you provide regarding (1) class reminders, waitlists, account balances, and/or transactions, and (2) OTF promotions or advertising.  You acknowledge that these calls or text messages may be sent via autodialer, and that standard message and data rates may apply.  You are not required to authorize calls or text messages to become an OTF member, and you may opt-out at any time by request if called or by replying “STOP” in response to a message.'}
                </div>
              </div>
              <div className='title-text'>
                <h1>{'RULES & POLICIES:'}</h1>
              </div>
              <div className='policy'>
                <div>
                  {'These Rules & Policies, as modified and amended from time-to-time (the “'}
                  <span className='underline'>{'Rules & Policies'}</span>
                  {'”), are incorporated into and a part of the Client Intake Form, Membership Agreement and/or Package Holder Agreement with us (collectively referred to as the “'}
                  <span className='underline'>{'Studio User Documents'}</span>
                  {'”), as applicable. Neither Ultimate Fitness Group, LLC, the franchisor of the Orangetheory® Fitness system and the entity who granted us contractual authority to independently own and operate our franchised location, nor any of its past, present, or future subsidiaries, successors, assigns, or affiliates, or their respective officers, directors, incorporators, members, partners, owners, agents, management, controlling parties, entities under common control, vendors, service providers, investors, attorneys, employees, or representatives is a party to the Studio User Documents or the Rules & Policies.  Capitalized terms used but not defined in the Rules & Policies have the meanings given to them in the applicable Studio User Document.'}
                </div>
              </div>
              {
                ((StudioState === 'Florida') || (StudioState === 'FL')) &&
                <div className='policy'>
                  <div>
                    {`${studio.StudioName} is registered with the State of Florida as a Health Studio.  Registration No. HS ${RegistrationHSNo || ''}`}
                  </div>
                </div>
              }
              <div className='policy'>
                <p><u><b>{'8 Hour Class Cancellation Policy:'}</b></u></p>
                <div>
                  {'We enforce a strict 8-hour class cancellation policy.  If you reserve a place in a class but fail to cancel 8 hours in advance and fail to show up for the class, you may forfeit a membership or package session or, if you have used your monthly membership or package sessions, you are not a member or package  holder  (i.e.,  you  are  a  drop  in  user  at  the  studio)  or  you  are  a  Premier  member,  as  applicable,  your  credit  card  on  file  may  be  charged  the  amount  for  an additional session consistent with the terms of the applicable Studio User Document.  You may also be subject to an additional surcharge if such a surcharge is in place at the studio where you registered for a class.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Late Arrival Policy:'}</b></u></p>
                <div>
                  {'We enforce a strict late arrival policy.  If you are more than 5 minutes late for your class, you may not be permitted to enter the class and may forfeit a membership or package session or be charged for the class session as set forth above.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Early Entry into Studio:'}</b></u></p>
                <div>
                  {'For the safety of all members and guests, you will not be permitted in the workout area unless a coach is present.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Equipment Reservation Cards: '}</b></u></p>
                <div>
                  {'Are available on a first-come, first-served basis and can be picked up no more than 30 minutes prior to the start of class. You may only pick up one card. No multiple card pickups for family/friends.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Waitlist  Procedure: '}</b></u></p>
                <div>
                  {'If  you  are  a  member  or  class  package  holder,  you  may  add  yourself,  or  request  that  a  sales  associate  add  you,  to  the  waitlist  of  any  normally scheduled class session.  Being added to a waitlist, however, does not guarantee your entrance into a class.  If you are on a waitlist for a class and a spot opens up at least 8 hours in advance of the class, you will be automatically added into the class.  If you fail to show up for the class, you may forfeit a membership or package session or be charged for the session as set forth above.  Please see a studio sales associate for more details regarding the waitlist sign-up process.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Dress Code Policy: '}</b></u></p>
                <div>
                  {'We want you to come to class fully prepared for strenuous activity: enclosed running shoes, athletic shorts/pants, athletic top, towel, and water bottle. A sports bra is considered an appropriate athletic top for women. Franchisees reserve the right to refuse entrance to any client who is not properly attired or is wearing clothing that may be considered offensive to other members such as a t-shirt with vulgar, sexually suggestive, or racially offensive language or graphics. If you are refused entrance, you may forfeit a membership or package session or be charged for the session as set forth above.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Children in the Studio: '}</b></u></p>
                <div>
                  {'Children  under  the  age  of  14  are  not  allowed  in  the  studio  at  any  time.  Children  ages  14-15  are  permitted  in  the  studio  only  if  they  are accompanied by a parent or legal guardian at all times, and the parent or legal guardian has first signed any required studio paperwork pertaining to such use.  Children ages 16-17 are permitted in the studio and may work out without parental or legal guardian supervision or accompaniment, provided the parent or legal guardian has first signed any required studio paperwork pertaining to such use.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Rules: '}</b></u></p>
                <div>
                  {'All rules and modifications to rules as provided and as posted in the studio or at www.orangetheoryfitness.com are part of your Studio User Documents, and you agree to comply with the same.  If you fail to comply, your Studio User Documents may be terminated.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Membership Freeze Policy:  '}</b></u></p>
                <div>
                  {'You have the option to freeze your membership for a minimum of 30 days and a maximum of 60 days.  You may freeze your membership up to 2 times per year (for a maximum of 120 days per year).  You may not freeze your membership for more than 60 consecutive days.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Suspend Fee:'}</b></u></p>
                <div>
                  {'During any freeze period, you will be charged a monthly suspend fee on the regular autopay (EFT) date based on the fee in place at the studio where you purchased your membership.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'National Reciprocity:'}</b></u></p>
                <div>
                  {'Unless  otherwise  noted,  you  may  use  your  membership  and  package  sessions  or  purchase  additional  sessions  at  any  Orangetheory®  Fitness studio in the United States; however, prices and sessions offered at each studio may vary and may require additional payments (i.e., a surcharge).'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'Session Capacity:'}</b></u></p>
                <div>
                  {'Orangetheory® Fitness class sessions are limited in size and may reach capacity.   Nothing in your Studio User Document guarantees your ability to attend class sessions that have reached capacity and for which you did not register in advance.'}
                </div>
              </div>
              <div className='policy'>
                <p><u><b>{'OTbeat Heart Rate Monitor:'}</b></u></p>
                <div>
                  {'Orangetheory® Fitness system is a heart rate based, interval training fitness program.  We strongly encourage you to purchase an OTbeat Heart Rate Monitor. Otherwise, we rent loaner monitors for a daily fee, which are available on a first come first serve basis, with first time guests of a studio having priority usage.  If you borrow an OTbeat Heart Rate Monitor from a studio and you do not return it, your credit card on file may be charged the then current retail price of such monitor.  Please see a studio sales associate for more details regarding OTbeat Heart Rate Monitors for sale and rent.'}
                </div>
              </div>
            </div>
            <div className='policy'>
              <div className={'red-txt'}>
                {renderMessages}
              </div>
            </div>
            <div className='policy'>
              <b>{'I have read and understand and agree to comply in full with the terms and conditions stated above and in the Rules and Policies document.  I acknowledge that I have received a copy of the terms and conditions stated above and the Rules and Polices document and have had sufficient time to read them.'}</b>
            </div>
            <div className='content-signature'>
              <div className='signature'>
                <h3>
                  <b>{'Individual Member/Package Holder/Studio User (“'}<u>{'Client'}</u>{'”)'}</b>
                </h3>
                <div className='sig-item'>
                  <label className='title'>{'Name: '}</label>
                  <Field
                    id={'clientName'}
                    validate={this.isRequired}
                    name='clientName' component={this.renderField} className={'text sig'} />
                </div>
                <div className='sig-item'>
                  <label className='title'>{'Signature'}</label>
                  <Field
                    name='memberSignature'
                    component={() => this.renderSignature.bind(this)('memberSignature')} />
                  <label className='title'>{'Date'}</label>
                  <Field
                    name='memberSignatureDate'
                    options={{ format: 'MM/DD/YYYY' }}
                    component={Calendar} />
                </div>
                <h3>
                  <b>{'Parent/Guardian Signature (if Client is under 18)'}</b>
                </h3>
                <div className='sig-item'>
                  <label className='title'>{'Name: '}</label>
                  <Field
                    id={'parentName'}
                    validate={Helpers.calculateAge(new Date(memberInfo.birthDay)) < 18 && this.isRequired}
                    name='parentName' component={this.renderField} className={'text sig'} />
                </div>
                <div className='sig-item parent-sig'>
                  <label className='title'>{'Signature:'}
                  </label>
                  <Field
                    name='parentSignature'
                    component={() => this.renderSignature.bind(this)('parentSignature')} />
                  <label className='title'>{'Date'}</label>
                  <Field
                    name='parentSignatureDate'
                    options={{ format: 'MM/DD/YYYY' }}
                    component={Calendar} />
                </div>
                <h3>
                  <b>{'Studio Staff Member'}</b>
                </h3>
                <div className='sig-item'>
                  <label className='title'>{'Name: '}</label>
                  <Field
                    id='staffName'
                    validate={this.isRequired}
                    name='staffName' component={this.renderField} className={'text sig'} />
                </div>
                <div className='sig-item'>
                  <label className='title'>{'Signature'}</label>
                  <Field
                    name='staffSignature'
                    component={() => this.renderSignature.bind(this)('staffSignature')} />
                  <label className='title'>{'Date'}</label>
                  <Field
                    name='staffSignatureDate'
                    options={{ format: 'MM/DD/YYYY' }}
                    component={Calendar} />
                </div>
              </div>
            </div>
          </div>
        </form>
        <WizardButton actions={this.props.actions} onSubmit={(step) => this.props.onSubmit(step)}
          clientIntake={this.props.clientIntake} isAbleSubmit={this.props.isAbleSubmit && !invalid && isChecked} />
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
PolicySection.propTypes = {
  actions: PropTypes.object,
  afterValid: PropTypes.func,
  clientIntake: PropTypes.object,
  currentStep: PropTypes.number,
  intl: intlShape.isRequired,
  invalid: PropTypes.bool,
  isAbleSubmit: PropTypes.bool,
  onEnd: PropTypes.func,
  onSubmit: PropTypes.func,
  signature: PropTypes.object,
  studio: PropTypes.object
};

let policySection = reduxForm({
  form: 'policySection' // A unique identifier for this form
})(PolicySection);

policySection = connect(state => ({
  memberInfo: state.clientTake_members.memberInfo
}))(policySection);

export default injectIntl(policySection);

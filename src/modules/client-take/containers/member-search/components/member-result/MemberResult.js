import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';
import * as Helpers from 'common/utils/helpers';
import MaleImage from './images/icon_male.svg';
import FemaleImage from './images/icon_female.svg';
import { ModalType, ModalResult } from 'modules/core/components';
import { intlShape, injectIntl } from 'react-intl';

export class MemberResult extends React.Component {
  constructor(props) {
    super(props);
  }

  openCI() {
    const { intl, member } = this.props;
    if (member.MemberStudioAgreement === null) {
      return this.props.actions.routing.navigateTo(`/client-take/ci-form/${member.Email}?memberId=${member.MemberId}`);
    }
    return this.props.actions.core_modal.show({
      message: intl.formatHTMLMessage({id: 'MemberSearch.Confirmation.OpenCI.Message'}),
      modalType: ModalType.Approve,
      className: 'openCI-confirmation',
      onClose: this.confirmOpenCI.bind(this)
    });
  }

  confirmOpenCI(modalResult) {
    const { member } = this.props;
    if (modalResult === ModalResult.Ok) {
      this.props.actions.routing.navigateTo(`/client-take/ci-form/${member.Email}?memberId=${member.MemberId}`);
    }
  }
  formatMemberName(firstName, lastName) {
    if (!firstName) {
      return undefined;
    }

    // Trim name before concatination
    if (lastName) {
      lastName = `${lastName}`.trim();
    }
    return `${firstName.trim()} ${`${lastName || ''}`.charAt(0).toUpperCase()}.`;
  }
  render() {
    const { member } = this.props;
    const firstName = member.FirstName;
    const lastName = member.LastName;
    return (
      <div className={`member-result-item${member.MemberStudioAgreement ? ' frost-item' : ''}`} onClick={this.openCI.bind(this)}>
        <div className='container-member'>
          <div className='avatar-icon'>
            <img className='icon-member' src={member.GenderId === 1 ? MaleImage : FemaleImage} />
          </div>
          <div className='wrap-name'>
            <p className='name-member'>{this.formatMemberName(firstName, lastName)}</p>
            <p className='email'>{Helpers.renderEmail(member.Email)}</p>
          </div>
        </div>
      </div>
    );
  }
}

MemberResult.propTypes = {
  actions: PropTypes.any,
  intl: intlShape.isRequired,
  member: PropTypes.object
};
export default injectIntl(MemberResult);


import { makeRoutes } from './config/routes';

import * as MemberRedux from './config/redux/member';

const ClientTakeModule = {
  makeRoutes: makeRoutes,
  redux: {
    members: MemberRedux
  }
};

export default ClientTakeModule;

import { createConstants, createReducer } from 'redux-module-builder';

import { MemberAPI } from 'services';
import { errorHandlerConstants } from 'modules/core/config/redux/error-handler';

export const types = createConstants('members')(
  'FETCHING_SUGGESTION',
  'FETCHED_SUGGESTION',
  'UPDATING_INPUT',
  'CLEAR_SUGGESTION',
  'GET_MEMBER_SUCCESS',
  'CLEAR_MEMBER_DETAIL',
  'ADD_IMAGE',
  'SAVE_MEMBER_SECTION_VALUES',
  'SAVE_STAFF_SECTION_VALUES',
  'SAVE_POLICY_SECTION_VALUES',
  'SAVE_CLIENT_INTAKE',
  'RESET_CLIENT_INTAKE',
  'SAVE_SIGNATURE',
  'CLEAR_SIGNATURE',
  'CLEAR_MEMBER_GOALS',
  'GET_MEMBER_GOALS');

export const actions = {
  fetchMembersSuggestion: () => (dispatch) => {
    dispatch({ type: types.FETCHING_SUGGESTION });
  },
  searchMembersSuggestion: (keyword, loggedInStudioId, filterStudioId, isSuggestion) => (dispatch) => {
    if (!keyword || keyword.length < 3) {
      if (!isSuggestion) {
        dispatch({
          type: errorHandlerConstants.HANDLE_ERROR,
          payload: { error: new Error('At least 3 characters to search.') }
        });
      }
    } else {
      dispatch({ type: types.FETCHING_SUGGESTION });
      let searchPayload = {
        SearchQuery: encodeURIComponent(keyword).trim(),
        PageSize: 30,
        StudioId: loggedInStudioId,
        FilterByStudioId: filterStudioId
      };
      MemberAPI.searchMember(searchPayload, isSuggestion)
        .then((res) => {
          dispatch({ type: types.FETCHED_SUGGESTION, payload: res.data });
        })
        .catch(() => {
          dispatch({ type: types.FETCHED_SUGGESTION, payload: [] });
        });
    }
  },
  updateCurrentMember: (newValue) => (dispatch) => {
    dispatch({ type: types.UPDATING_INPUT, payload: newValue });
  },
  clearMemberSuggestion: () => (dispatch) => {
    dispatch({ type: types.CLEAR_SUGGESTION });
  },
  getMember: (memberId) => (dispatch) => {
    MemberAPI.getMemberProfileByEmail(memberId).then((res) => {
      dispatch({ type: types.GET_MEMBER_SUCCESS, payload: res.data.data });
    }).catch((error) => {
      dispatch({
        type: errorHandlerConstants.HANDLE_ERROR,
        payload: {
          error
        }
      });
    });
  },
  detroyMemberDetail: () => (dispatch) => {
    dispatch({ type: types.CLEAR_MEMBER_DETAIL });
  },
  saveMemberSectionValues: () => (dispatch, getState) => {
    const memberInfo = getState().form.memberSection.values;
    dispatch({ type: types.SAVE_MEMBER_SECTION_VALUES, payload: memberInfo });
  },
  saveStaffSectionValues: () => (dispatch, getState) => {
    const studioInfo = getState().form.staffSection.values;
    dispatch({ type: types.SAVE_STAFF_SECTION_VALUES, payload: studioInfo });
  },
  savePolicySectionValues: () => (dispatch, getState) => {
    const policyInfo = getState().form.policySection.values;
    dispatch({ type: types.SAVE_POLICY_SECTION_VALUES, payload: policyInfo });
  },
  saveClientIntake: (memberUUId, ci, payload) => (dispatch) => {
    MemberAPI.saveClientIntake(memberUUId, ci).then(async (res) => {
      dispatch({ type: types.SAVE_CLIENT_INTAKE, payload: res.data });
      MemberAPI.submitMemberGoals(payload);
    }).catch((error) => {
      dispatch({
        type: errorHandlerConstants.HANDLE_ERROR,
        payload: {
          error
        }
      });
    });
  },
  resetClientIntake: () => (dispatch) => {
    dispatch({ type: types.RESET_CLIENT_INTAKE });
  },
  saveSignature: (signature, name) => (dispatch) => {
    dispatch({ type: types.SAVE_SIGNATURE, payload: { signature, name }});
  },
  clearSignature: () => (dispatch) => {
    dispatch({ type: types.CLEAR_SIGNATURE });
  },
  clearMemberGoals: () => (dispatch) => {
    dispatch({ type: types.CLEAR_MEMBER_GOALS });
  },
  getMemberGoals: (memberUUId) => (dispatch) => {
    MemberAPI.getMemberGoals(memberUUId).then((res) => {
      dispatch({ type: types.GET_MEMBER_GOALS, payload: res.data.goalCategories });
    }).catch((error) => {
      dispatch({
        type: errorHandlerConstants.HANDLE_ERROR,
        payload: {
          error
        }
      });
    });
  }
};

export const reducer = createReducer({
  [types.FETCHING_SUGGESTION]: (state) => {
    return {
      ...state,
      fetching: true,
      suggestions: [],
      errors: undefined
    };
  },
  [types.FETCHED_SUGGESTION]: (state, { payload }) => {
    return {
      ...state,
      fetching: false,
      members: payload,
      suggestions: payload,
      errors: undefined
    };
  },
  [types.UPDATING_INPUT]: (state, { payload }) => {
    return {
      ...state,
      member: payload || ''
    };
  },
  [types.CLEAR_SUGGESTION]: (state) => {
    return {
      ...state,
      fetching: false,
      suggestions: [],
      members: [],
      member: ''
    };
  },
  [types.GET_MEMBER_SUCCESS]: (state, { payload }) => {
    return {
      ...state,
      memberDetail: payload
    };
  },
  [types.CLEAR_MEMBER_DETAIL]: (state) => {
    return {
      ...state,
      memberDetail: undefined
    };
  },
  [types.SAVE_MEMBER_SECTION_VALUES]: (state, { payload }) => {
    return {
      ...state,
      memberInfo: payload
    };
  },
  [types.SAVE_STAFF_SECTION_VALUES]: (state, { payload }) => {
    return {
      ...state,
      studioInfo: payload
    };
  },
  [types.SAVE_POLICY_SECTION_VALUES]: (state, { payload }) => {
    return {
      ...state,
      policyInfo: payload
    };
  },
  [types.SAVE_CLIENT_INTAKE]: (state, { payload }) => {
    return {
      ...state,
      clientIntake: payload
    };
  },
  [types.RESET_CLIENT_INTAKE]: (state) => {
    return {
      ...state,
      clientIntake: {}
    };
  },
  [types.SAVE_SIGNATURE]: (state, { payload }) => {
    return {
      ...state,
      [payload.name]: payload.signature
    };
  },
  [types.CLEAR_SIGNATURE]: (state) => {
    return {
      ...state,
      memberSignature: '',
      staffSignature: '',
      parentSignature: '',
      clientInitials: ''
    };
  },
  [types.GET_MEMBER_GOALS]: (state, {payload}) => {
    return {
      ...state,
      memberGoals: payload
    };
  },
  [types.CLEAR_MEMBER_GOALS]: (state) => {
    return {
      ...state,
      memberGoals: []
    };
  }
});

/*
 * The initial state for this part of the component tree
 */
export const initialState = {
  errors: undefined,
  fetching: false,
  member: '',
  members: [],
  suggestions: [],
  memberDetail: undefined,
  memberInfo: {},
  studioInfo: {},
  policyInfo: {},
  clientIntake: {},
  memberSignature: '',
  staffSignature: '',
  parentSignature: '',
  clientInitials: '',
  memberGoals: []
};

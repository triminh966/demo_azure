import React from 'react';
import { Route } from 'react-router';

import { Container } from '../../check-in/containers';
import MemberSearch from '../containers/member-search/MemberSearch';
import CIForm from '../containers/ci-form/CIForm';

import { CognitoService } from 'services';

export const makeRoutes = (getState) => {
  const requireAuth = (nextState, replace, callback) => {
    const { auth_users } = getState();
    const { currentUser } = auth_users;

    if (!currentUser) {
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
      return callback();
    } else if (!CognitoService.getCurrentUser()) {
      CognitoService.refreshToken().then(result => {
        let currentToken = localStorage.getItem('session_token');
        if (result !== currentToken) {
          localStorage.setItem('session_token', result);
        }
        // Re-authorize the route after refresh token
        replace({
          pathname: '/login',
          state: { nextPathname: nextState.location.pathname }
        });

        return callback();
      }).catch(() => { callback(); });
    }
    return callback();
  };
  /**
   * Return route define
   */
  return (
    <Route key='client-take' path='/client-take' component={Container}>
      <Route onEnter={requireAuth} path='member-search'
        component={MemberSearch} />
      <Route onEnter={requireAuth} path='ci-form/:id'
        component={CIForm} />
    </Route>
  );
};

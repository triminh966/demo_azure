import { makeRoutes } from './config/routes';


const ProductSelectionModule = {
  makeRoutes: makeRoutes,
  redux: { }
};

export default ProductSelectionModule;

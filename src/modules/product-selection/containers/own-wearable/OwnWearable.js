import React from 'react';
import PropTypes from 'prop-types';

import { DeviceApple, DeviceFitbit, DeviceGarmin } from 'modules/shared/images';
import './styles.scss';

export class OwnWearable extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='own-wearable-wrapper'>
        <div className='device-wrapper'>
          <img className='device' src={DeviceFitbit} />
          <img className='device middle'src={DeviceApple} />
          <img className='device' src={DeviceGarmin} />
        </div>
        <h2 className='question'>{'Do you currently own an all day wearable?'}</h2>
        <div className='btn-wrapper'>
          <div className='no-btn' onClick={() => this.props.router.push('/product-selection/prefer-to-wear')}>{'NO'}</div>
          <div className='yes-btn' onClick={() => this.props.router.push('/product-selection/product-page')}>{'YES'}</div>
        </div>
      </div>
    );
  }
}
OwnWearable.propTypes = {
  router: PropTypes.object
};

export default OwnWearable;

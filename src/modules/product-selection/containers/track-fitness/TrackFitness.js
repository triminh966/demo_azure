import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

export class TrackFitness extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='track-fitness-wrapper'>
        <h2 className='question'>{'Do you use it to track your fitness?'}</h2>
        <div className='btn-wrapper'>
          <div className='no-btn' onClick={() => this.props.router.push('/product-selection/prefer-to-wear')}>{'NO'}</div>
          <div className='yes-btn' onClick={() => this.props.router.push('/product-selection/product-page')}>{'YES'}</div>
        </div>
      </div>
    );
  }
}
TrackFitness.propTypes = {
  router: PropTypes.object
};
export default TrackFitness;

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';
import { TrackingMetrics } from 'modules/shared/images';

export class LifestyleMetrics extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='lifestyle-metrics-wearable-wrapper'>
        <div className='lms'>
          <img src={TrackingMetrics} className='tracking-metrics'/>
        </div>
        <h2 className='question'>{'Are you interested in tracking your lifestyle metrics with your OTbeat wearable (calories, steps, distance)?'}</h2>
        <div className='btn-wrapper'>
          <div className='no-btn' onClick={() => this.props.router.push('/product-selection/product-page')}>{'NO THANKS'}</div>
          <div className='yes-btn' onClick={() => this.props.router.push('/product-selection/prefer-to-wear')}>{'YES!'}</div>
        </div>
      </div>
    );
  }
}
LifestyleMetrics.propTypes = {
  router: PropTypes.object
};
export default LifestyleMetrics;

import React from 'react';
import PropTypes from 'prop-types';

import { DeviceApple, DeviceFitbit, DeviceGarmin, DeviceOther } from 'modules/shared/images';
import './styles.scss';

export class WhichWearable extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='which-wearable-wrapper'>
        <h2 className='question'>{'Which all day wearable do you have?'}</h2>
        <div className='wearable-opt'>
          <div className='col-md-2' />
          <div className='wearable-wrapper col-md-2' onClick={() => this.props.router.push('/product-selection/track-fitness')}>
            <img className='wearable' src={DeviceFitbit} />
            <h1 className='wearable-name'>{'fitbit'}</h1>
          </div>
          <div className='wearable-wrapper col-md-2' onClick={() => this.props.router.push('/product-selection/track-fitness')}>
            <img className='wearable' src={DeviceApple} />
            <h1 className='wearable-name'>{'apple'}</h1>
          </div>
          <div className='wearable-wrapper col-md-2' onClick={() => this.props.router.push('/product-selection/track-fitness')}>
            <img className='wearable' src={DeviceGarmin} />
            <h1 className='wearable-name'>{'garmin'}</h1>
          </div>
          <div className='wearable-wrapper col-md-2' onClick={() => this.props.router.push('/product-selection/track-fitness')}>
            <img className='wearable other' src={DeviceOther} />
            <h1 className='wearable-name'>{'other'}</h1>
          </div>
          <div className='col-md-2' />
        </div>
      </div>
    );
  }
}

WhichWearable.propTypes = {
  router: PropTypes.object
};
export default WhichWearable;

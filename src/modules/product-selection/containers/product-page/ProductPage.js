import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import PropTypes from 'prop-types';

import { PanelContainer } from '../../../check-in/containers';
import WearableInfo from './components/WearableInfo';

import { DeviceBurn, DeviceCore, IconWearable } from 'modules/shared/images';

import { Constants } from 'common';

import './styles.scss';

export class ProductPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productIndex: this.props.location.query.wearableType
        ? Constants.WearableType[this.props.location.query.wearableType] : 0
    };
  }

  renderProductContent(wearables) {
    return wearables.map((wearable, index)=>{
      if (wearable.type === 'text') {
        return <div key={index} className='instruction'>
          <div className='header-ins'>
            <img className='instruction-ic' src={IconWearable} />
            <p className='title'>{'how to pair'}</p>
          </div>
          <p className='content-ins'> <b>{'1. '}</b> {'Make sure that you OTbeat Burn is ON and discoverable.'}
            <br/>
            <b>{'2. '}</b>{'Launch the settings app on the apple Watch.'}
            <br/>
            <b>{'3. '}</b>{'Select Bluetooth and your OTbeat Burn should appear under health devices.'}
            <br/>
            <b>{'4. '}</b>{'Tap OTbeat Burn and wait until you see the “Connected” confirmation.'}
            <br/>
            <b>{'5. '}</b>{'Your OTbeat Burn is now connected.'}</p>
        </div>;
      }
      return <div key={index}>
        <img src={wearable.content} />
      </div>;
    });
  }
  renderProductArr() {
    if (this.state.productIndex === Constants.WearableType.Arm) {
      return [{ type: 'image', content: DeviceBurn}, { type: 'text', content: 'Burn'}];
    }
    return [{ type: 'image', content: DeviceCore}];
  }
  render() {
    const wearables = this.renderProductArr();
    const productInfos = [
      {title: 'burn', description: 'We’re proud to introduce our latest wearable, the OTbeat Burn! It’s smart and sleek — it tracks your steps and stores your unique heart rate data so you can get the most out of your workout.',
        activityTracking: 'Steps, distance and calories', battery: 'Up to 24 hours', warranty: '1-year', sensor: 'Optical PPG', price: 109},
      {title: 'core', description: 'We’re proud to introduce center of our products, the OTbeat Core! It’s a chest strap and pod combo that uses ECG technology to track your unique heart rate data so you can get the most out of your workout.',
        activityTracking: 'Heart Rate', battery: 'CR 2032 battery up to 1-year battery life', warranty: '1-year', sensor: 'ECG', price: 74}
    ];
    return (
      <div className='product-page'>
        <PanelContainer
          leftPanel={(
            <WearableInfo info={productInfos[this.state.productIndex]} startOver={() => this.props.router.push('/product-selection/home')}/>
          )}
          rightPanel={(
            <div className='carousel slide'>
              <Carousel showThumbs={false} showStatus={false}>
                {this.renderProductContent(wearables)}
              </Carousel>
            </div>
          )}/>
      </div>
    );
  }
}

ProductPage.propTypes = {
  location: PropTypes.any,
  router: PropTypes.object
};
export default ProductPage;

import React from 'react';
import PropTypes from 'prop-types';

import { BurnIcon, BatteryIcon, CheckOrangeIcon, SensorIcon } from 'modules/shared/images';
import './styles.scss';

export class WearableInfo extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='left-wrapper'>
        <div className='devide-info'>
          <p className='title'>{this.props.info.title}</p>
          <p className='introduce'>{this.props.info.description}</p>
          <div className='row'>
            <div className='col-md-2'>
              <img src={BurnIcon} className='icon-info'/>
            </div>
            <div className='col-md-5'>
              <h3 className='wearable-prop'>{'Activity Tracking'}</h3>
            </div>
            <div className='col-md-5'>
              <p className='wearable-content'>{this.props.info.activityTracking}</p>
            </div>
            <div className='col-md-2'>
              <img src={BatteryIcon} className='icon-info'/>
            </div>
            <div className='col-md-5'>
              <h3 className='wearable-prop'>{'Battery'}</h3>
            </div>
            <div className='col-md-5'>
              <p className='wearable-content single-ln'>{this.props.info.battery}</p>
            </div>
            <div className='col-md-2'>
              <img src={CheckOrangeIcon} className='icon-info'/>
            </div>
            <div className='col-md-5'>
              <h3 className='wearable-prop'>{'Warranty'}</h3>
            </div>
            <div className='col-md-5'>
              <p className='wearable-content single-ln'>{this.props.info.warranty}</p>
            </div>
            <div className='col-md-2'>
              <img src={SensorIcon} className='icon-info'/>
            </div>
            <div className='col-md-5'>
              <h3 className='wearable-prop'>{'Sensor'}</h3>
            </div>
            <div className='col-md-5'>
              <p className='wearable-content single-ln'>{this.props.info.sensor}</p>
            </div>
          </div>
          <div className='price-wrapper row'>
            <p className='price'>{`$${this.props.info.price}`}<span className='plus-tax'>{'plus tax'}</span></p>
          </div>
        </div>
        <div className='start-over row' onClick={this.props.startOver}>{'start over'}</div>
      </div>
    );
  }
}

WearableInfo.propTypes = {
  info: PropTypes.object,
  router: PropTypes.object,
  startOver: PropTypes.func
};

export default WearableInfo;

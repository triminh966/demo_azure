import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';
import { PhotoArm, PhotoChest } from 'modules/shared/images';

export class PreferToWear extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='prefer-to-wear-wrapper'>
        <h2 className='question'>{'Where do you prefer to wear your OTbeat wearable?'}</h2>
        <div className='place-wrapper'>
          <div className='body-part col-md-5' onClick={() => this.props.router.push('/product-selection/product-page?wearableType=Arm')}>
            <img className='img-part' src={PhotoArm}/>
            <h1 className='name-part'>{'ARM'}</h1>
          </div>
          <div className='body-part col-md-5' onClick={() => this.props.router.push('/product-selection/product-page?wearableType=Chest')}>
            <img className='img-part' src={PhotoChest}/>
            <h1 className='name-part'>{'CHEST'}</h1>
          </div>
        </div>
      </div>
    );
  }
}
PreferToWear.propTypes = {
  router: PropTypes.object
};

export default PreferToWear;

import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';
import { DeviceOtbeat } from 'modules/shared/images';

export class HomeProduct extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='home-product'>
        <div>
          <div className='otbeat-txt-wrapper'>
            <img className='device-otbeat' src={DeviceOtbeat}/>
          </div>
        </div>
        <div className='right-wrapper'>
          <h1 className='title-home'>{'tracking towards'}<b>{' more life'}</b></h1>
          <h3 className='content-home'>{'This isn’t just a heart rate monitor. It does more then that. It tells you when to push yourself. It tells you when to rest. And when your workout is working. This is a life-Changing Monitor. Connect your heart to your workout, and begin your journey to more life.'}</h3>
          <div className='orange-btn' onClick={() => this.props.router.push('/product-selection/own-wearable')}>
            {'GET STARTED'}
          </div>
        </div>
      </div>
    );
  }
}
HomeProduct.propTypes = {
  router: PropTypes.object
};
export default HomeProduct;

import React from 'react';
import { Route } from 'react-router';

import { CognitoService } from 'services';

import { Container } from '../../check-in/containers';

import HomeProduct from '../containers/home-product/HomeProduct';
import OwnWearable from '../containers/own-wearable/OwnWearable';
import WhichWearable from '../containers/which-wearable/WhichWearable';
import TrackFitness from '../containers/track-fitness/TrackFitness';
import LifestyleMetrics from '../containers/lifestyle-metrics/LifestyleMetrics';
import PreferToWear from '../containers/prefer-to-wear/PreferToWear';
import ProductPage from '../containers/product-page/ProductPage';

export const makeRoutes = (getState) => {
  const requireAuth = (nextState, replace, callback) => {
    const { auth_users } = getState();
    const { currentUser } = auth_users;

    if (!currentUser) {
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
      return callback();
    } else if (!CognitoService.getCurrentUser()) {
      CognitoService.refreshToken().then(result => {
        let currentToken = localStorage.getItem('session_token');
        if (result !== currentToken) {
          localStorage.setItem('session_token', result);
        }
        // Re-authorize the route after refresh token
        replace({
          pathname: '/login',
          state: { nextPathname: nextState.location.pathname }
        });

        return callback();
      }).catch(() => { callback(); });
    }
    return callback();
  };
  /**
   * Return route define
   */
  return (
    <Route key='product' path='/product-selection' component={Container}>
      <Route onEnter={requireAuth} key='product-selection' path='home'
        component={HomeProduct}/>
      <Route onEnter={requireAuth} key='own-wearable' path='own-wearable'
        component={OwnWearable}/>
      <Route onEnter={requireAuth} key='which-wearable' path='which-wearable'
        component={WhichWearable}/>
      <Route onEnter={requireAuth} key='track-fitness' path='track-fitness'
        component={TrackFitness}/>
      <Route onEnter={requireAuth} key='lifestyle-metrics' path='lifestyle-metrics'
        component={LifestyleMetrics}/>
      <Route onEnter={requireAuth} key='prefer-to-wear' path='prefer-to-wear'
        component={PreferToWear}/>
      <Route onEnter={requireAuth} key='product-page' path='product-page'
        component={ProductPage}/>
    </Route>
  );
};


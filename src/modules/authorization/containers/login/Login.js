/**
 * React / Redux dependencies
 */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape, injectIntl } from 'react-intl';

/**
 * The application library, component, etc...
 */
import { LoginForm } from './components/login-form';
import { ForgotPassword } from './components/forgot-password';
import { ChangePassword } from './components/change-password';
import { AppConstants } from 'common';
import { LanguageSetting } from 'modules/check-in/containers';
import LogoChallenge from './images/logo-challenge-header.png';
/**
 * The assets such as css, images
 */
import Logo from './images/Logo.png';
import './login.scss';

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isForgotPassword: false,
      newPasswordRequired: false,
      forgotPasswordSuccess: false,
      forgotPasswordConfirmSuccess: false
    };
  }

  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(newProps) {
    const { actions, intl } = this.props;

    if (newProps.loginSuccess) {
      actions.routing.navigateTo(AppConstants.defaultPagePath.homePage);
    }

    if (newProps.newPasswordRequired) {
      this.setState({
        newPasswordRequired: true
      });
    }

    this.setState({
      forgotPasswordSuccess: newProps.forgotPasswordSuccess
    });

    if (newProps.forgotPasswordConfirmSuccess) {
      actions.core_alert.showInfor(intl.formatMessage({id: 'Login.ChangePasswordSuccess'}));
      setTimeout(() => {
        actions.auth_users.logout();
        this.setState({
          isForgotPassword: false
        });
      }, 1000);
    }

    if (newProps.forceChangePasswordSuccess) {
      actions.core_alert.showInfor(intl.formatMessage({id: 'Login.ChangePasswordSuccess'}));

      setTimeout(() => {
        actions.auth_users.logout();
        this.setState({
          newPasswordRequired: false
        });
      }, 1000);
    }

    this.setState({
      selectedStudio: newProps.selectedStudio
    });

    this.props.actions.core_loading.hide();
  }

  authorize(formValues) {
    const { selectedStudio } = this.props;
    let password = formValues.cognitoPassword;
    this.props.actions.auth_users.loginCognito(selectedStudio, password);
  }

  switchViewState() {
    this.setState({ isForgotPassword: !this.state.isForgotPassword });
  }

  render() {
    let content;
    const { actions, isSwitchLocale, selectedStudio, email } = this.props;

    if (!isSwitchLocale) {
      $('.wrapper-center-login').addClass('wrapper-language');
      content = (
        <div>
          <div className='header-title'>
            <img src={LogoChallenge} />
            <span className='title'>
              <FormattedMessage id='General.AppName' />
            </span>
          </div>
          <LanguageSetting actions={actions} />
        </div>
      );
    }

    if (this.state.newPasswordRequired) {
      content = (<ChangePassword
        actions={this.props.actions}
        username={email} />);
    }

    if (!content) {
      content = this.state.isForgotPassword ?
        (<ForgotPassword
          actions={this.props.actions}
          switchToLogin={this.switchViewState.bind(this)}
          forgotPasswordSuccess={this.state.forgotPasswordSuccess} />) :
        (<LoginForm
          actions={this.props.actions}
          authorize={this.authorize.bind(this)}
          switchToForgotPassword={this.switchViewState.bind(this)}
          selectedStudio={selectedStudio} />);
    }

    return (
      <div className={'wrapper-center-login wrapper-language'}>
         {isSwitchLocale ? (
          <div className='header-logo'>
            <img src={Logo} className='logo-web'/>
            <div className='inclined-line'/>
            <div className='fitness-challenge-application'>
              <FormattedMessage id='General.AppName'/>
            </div>
          </div>
        ) : null}
        {content}
      </div>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
Login.propTypes = {
  actions: PropTypes.any,
  dispatch: PropTypes.func,
  email: PropTypes.string,
  intl: intlShape.isRequired,
  isSwitchLocale: PropTypes.bool,
  selectedStudio: PropTypes.object
};

let login = connect(state => ({
  error: state.core_errorHandler.error,
  selectedStudio: state.auth_users.selectedStudio,
  currentUser: state.auth_users.currentUser,
  loginSuccess: state.auth_users.loginSuccess,
  newPasswordRequired: state.auth_users.newPasswordRequired,
  forgotPasswordSuccess: state.auth_users.forgotPasswordSuccess,
  forgotPasswordConfirmSuccess: state.auth_users.forgotPasswordConfirmSuccess,
  errors: state.auth_users.errors,
  forceChangePasswordSuccess: state.auth_users.forceChangePasswordSuccess,
  isSwitchLocale: state.core_language.isSwitched,
  email: state.auth_users.email
}))(Login);
export default injectIntl(login);

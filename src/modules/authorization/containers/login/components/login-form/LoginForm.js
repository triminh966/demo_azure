/**
 * React / Redux dependencies
 */
import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape, injectIntl } from 'react-intl';

import StudioAutoSuggestion from '../suggestions/StudioAutoSuggestion';
import { isRequired, email } from 'common/utils/validation';

/**
 * The assets such as css, images
 */
import './styles.scss';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'password'
    };
    const { intl } = this.props;

    // Init validation function
    this.isRequired = isRequired(
      intl.formatMessage({ id: 'Validation.RequiredField' })
    );
    this.isEmail = email(intl.formatMessage({ id: 'Validation.InvalidEmail' }));
    this.showHide = this.showHide.bind(this);
  }

  handleKeyPress(e) {
    if (e.key === 'Enter') {
      const submitter = this.props.handleSubmit(this.props.authorize);
      submitter(); // submits
    }
  }

  showHide(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    });
  }
  renderField({ input, type, options, meta: { touched, error } }) {
    return (
      <div className={`input-form${touched && error ? ' has-error' : ''}`}>
        <input {...input} {...options} type={type} />
        {touched &&
          (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  /**
   * Select other language
   */
  selectLanguage() {
    const { actions } = this.props;

    actions.core_language.clearLocale();
  }

  /**
   * On studio name input touch
   */
  onStudioNameInputBlur() {
    const { touch } = this.props;

    touch(...['studioUsername']);
  }

  /**
   * Render form input (user name and password) based on login type
   */
  renderFormInput() {
    return (
      <div className='form-container'>
        <Field
          name='studioUsername'
          actions={this.props.actions.auth_users}
          component={StudioAutoSuggestion}
          onInputBlur={this.onStudioNameInputBlur.bind(this)}
          validate={this.isRequired}/>
        <div className='input-password'>
          <FormattedMessage id='Login.PlaceholderStudioPassword'>
            {ph => (
              <Field
                name='cognitoPassword'
                options={{
                  id: 'cognitoPassword',
                  className: 'input-password-studio i-alKeyboard i-password',
                  placeholder: ph,
                  autoComplete: 'off'
                }}
                type={this.state.type}
                component={this.renderField}
                validate={this.isRequired}/>
            )}
          </FormattedMessage>
          <span
            toggle='#password-field'
            className={`fa fa-fw fa-eye field-icon ${this.state.type === 'input' ? 'fa-eye-slash' : ''}`}
            onClick={this.showHide}/>
        </div>

      </div>
    );
  }

  /**
   * Validate Studio login
   */
  validateStudioLogin() {
    const { selectedStudio, studio } = this.props;
    return (
      !selectedStudio ||
      (selectedStudio &&
        studio !==
          `${selectedStudio.StudioName} #${selectedStudio.StudioNumber}`)
    );
  }

  render() {
    const { handleSubmit, invalid } = this.props;
    const checkClass = $('.wrapper-center-login').hasClass('wrapper-language');
    if (checkClass) {
      $('.wrapper-center-login').removeClass('wrapper-language');
    }
    console.log(process.env);
    return (
      <form
        className='login-form'
        onSubmit={handleSubmit(this.props.authorize)}
        autoComplete='off'>
        <div className='sign-in'>
          <h1>
            {' '}
            <FormattedMessage id='Login.Title' />
          </h1>
        </div>
        <input className='fake-input' type='text' name='fakename' />
        <input className='fake-input' type='password' name='fakepass' />
        {this.renderFormInput()}
        <div className='col-2'>
          <button
            disabled={invalid || this.validateStudioLogin()}
            className='btn button-login-studio'>
            <FormattedMessage id='Login.SignInBtn' />
          </button>
        </div>
        <a
          className='forgot-password'
          onClick={this.props.switchToForgotPassword}>
          <FormattedMessage id='Login.ForgotPassword' />
        </a>
        {'/'}
        <a
          className='select-other-language'
          onClick={this.selectLanguage.bind(this)}>
          <FormattedMessage id='Login.SelectLanguage' />
        </a>
      </form>
    );
  }
}

/**
 * Typechecking With PropTypes
 * Reference https://facebook.github.io/react/docs/typechecking-with-proptypes.html
 * Proptypes: https://github.com/facebook/prop-types
 */
LoginForm.propTypes = {
  actions: PropTypes.any,
  authorize: PropTypes.func,
  change: PropTypes.func,
  handleSubmit: PropTypes.func,
  intl: intlShape.isRequired,
  invalid: PropTypes.bool,
  reset: PropTypes.func,
  selectedStudio: PropTypes.object,
  studio: PropTypes.string,
  switchToForgotPassword: PropTypes.func,
  touch: PropTypes.func // Marks the given fields as "touched" to show errors
};

let loginForm = reduxForm({
  form: 'loginForm' // A unique identifier for this form
})(LoginForm);

loginForm = connect(state => ({
  selectedStudio: state.auth_users.selectedStudio,
  studio: state.auth_users.studio
}))(loginForm);

export default injectIntl(loginForm);

/**
 * React / Redux dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import EmailConfirmImage from './images/EmailConfirm.svg';

/**
 * The assets such as css, images
 */
import './styles.scss';

class DialogPasswordReset extends React.Component {
  constructor(props) {
    super(props);
  }

  onConfirm() {
    this.props.ok();
  }

  render() {
    return (
      <div>
        <img src={EmailConfirmImage} className='icon-email-confirm' />
        <p className='notification'><FormattedMessage id={'Login.ForgotPassword.Dialog.Notification'} /></p>
        <button className='back-login-btn' onClick={this.onConfirm.bind(this)}><FormattedMessage id={'General.OkBtn'} /></button>
      </div>
    );
  }
}

DialogPasswordReset.propTypes = {
  actions: PropTypes.any,
  backToLogin: PropTypes.func,
  forgotPassword: PropTypes.func,
  ok: PropTypes.func
};

export default DialogPasswordReset;

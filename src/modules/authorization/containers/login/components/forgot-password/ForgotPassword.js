/**
 * React / Redux dependencies
 */
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape, injectIntl } from 'react-intl';

import ForgotPasswordImage from './images/ForgotPassword.svg';
import { isRequired, match, strongPassword } from 'common/utils/validation';

/**
 * The assets such as css, images
 */
import './styles.scss';

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSendSuccessful: false,
      isValidRequiredFields: true
    };
    const { intl } = this.props;
    // Init validation function
    this.isRequired = isRequired(intl.formatMessage({ id: 'Validation.RequiredField' }));
    this.matchConfirmPassword = match('newPassword', intl.formatMessage({ id: 'Validation.MatchPassword' }));
    this.isValidPassword = strongPassword(intl.formatMessage({ id: 'Validation.StrongPassword' }));
  }

  /* eslint-disable react/no-deprecated */
  componentWillMount() {
    this.setState({
      isSendSuccessful: false
    });
    this.props.actions.auth_users.resetForgotPasswordData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.forgotPasswordSuccess
      && this.state.isSendSuccessful !== nextProps.forgotPasswordSuccess) {
      this.setState({
        isSendSuccessful: true,
        isValidRequiredFields: false
      });
    }
  }

  updateInputValue(evt) {
    this.setState((prevState) => {
      return { ...prevState, username: evt.target.value };
    });
  }

  submitForm(formValues) {
    if (this.state.isSendSuccessful) {
      this.props.actions.auth_users.forgotPasswordConfirm(formValues.email, formValues.pin, formValues.newPassword);
      return true;
    }

    this.props.actions.auth_users.forgotPassword(this.state.username);
    return false;
  }

  renderField({ input, type, options, meta: { touched, error } }) {
    return (
      <div className={`input-form${touched && error ? ' has-error' : ''}`}>
        <input
          {...input}
          {...options}
          type={type} />
        {touched && (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  renderFormInput() {
    const { invalid } = this.props;
    return (
      <div>
        <div className='row form-container'>
          <FormattedMessage id='General.EmailAddress'>
            {ph =>
              <Field
                name='email'
                options={{
                  className: `input-email-address i-alKeyboard${(this.state.isSendSuccessful) ? ' hidden' : ''}`,
                  id: 'email',
                  placeholder: ph
                }}
                onChange={this.updateInputValue.bind(this)}
                component={this.renderField}
                validate={this.isRequired} />
            }
          </FormattedMessage>
        </div>
        {
          this.state.isSendSuccessful && (
            <div>
              <div className='row form-container'>
                <FormattedMessage id='Login.ForgotPassword.PlaceholherVerifyCode'>
                  {ph =>
                    <Field
                      name='pin'
                      options={{
                        className: 'input-forgot-password i-alKeyboard',
                        id: 'pin',
                        placeholder: ph
                      }}
                      component={this.renderField}
                      validate={this.isRequired} />
                  }
                </FormattedMessage>
              </div>
              <div className='row form-container'>
                <FormattedMessage id='Login.ForgotPassword.PlaceholherNewPassword'>
                  {ph =>
                    <Field
                      name='newPassword'
                      options={{
                        className: 'input-forgot-password i-alKeyboard',
                        id: 'newPassword',
                        placeholder: ph
                      }}
                      type='password'
                      component={this.renderField}
                      validate={[this.isRequired, this.isValidPassword]} />
                  }
                </FormattedMessage>
              </div>
              <div className='row form-container'>
                <FormattedMessage id='Login.ForgotPassword.PlaceholherConfirmPassword'>
                  {ph =>
                    <Field
                      name='confirmPassword'
                      options={{
                        className: 'input-forgot-password i-alKeyboard',
                        id: 'confirmPassword',
                        placeholder: ph
                      }}
                      type='password'
                      component={this.renderField}
                      validate={[this.isRequired, this.matchConfirmPassword]} />
                  }
                </FormattedMessage>
              </div>
            </div>
          )
        }
        <button className='send-btn' disabled={invalid && !this.state.isValidRequiredFields}>
          <FormattedMessage id={this.state.isSendSuccessful ? 'Login.ForgotPassword.ResetPasswordBtn' : 'General.SendBtn'} />
        </button>
      </div>
    );
  }

  render() {
    const { handleSubmit } = this.props;
    let instruction = 'Login.ForgotPassword.Instruction';

    if (this.state.isSendSuccessful) {
      instruction = 'Login.ForgotPassword.Alert.SentNotification';
    }

    return (
      <form className='forgot-password-form' onSubmit={handleSubmit(this.submitForm.bind(this))}>
        <div className='row forgot-image'><img src={ForgotPasswordImage} className='fp-icon' /></div>
        <div className='row forgot-text'>
          <h3 className='title'><FormattedMessage id={'Login.ForgotPassword'} /></h3>
          <p className='instruction'><FormattedMessage id={instruction} /></p>
        </div>
        {this.renderFormInput()}
        <div className='row'><a className='back-to-login' onClick={this.props.switchToLogin}><FormattedMessage id={'Login.ForgotPassword.BackToLogin'} /></a></div>
      </form>
    );
  }
}

ForgotPassword.propTypes = {
  actions: PropTypes.any,
  forgotPasswordSuccess: PropTypes.bool,
  handleSubmit: PropTypes.func,
  intl: intlShape.isRequired,
  invalid: PropTypes.bool,
  switchToLogin: PropTypes.func,
  touch: PropTypes.func // Marks the given fields as "touched" to show errors
};

let forgotPassword = reduxForm({
  form: 'forgotPassword' // A unique identifier for this form
})(ForgotPassword);

export default injectIntl(forgotPassword);

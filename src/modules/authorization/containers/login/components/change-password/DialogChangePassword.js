/**
 * React / Redux dependencies
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { messages } from 'localization';

import ChangePassword from './ChangePasswordForm';

/**
 * The assets such as css, images
 */
import './styles.scss';

class DialogChangePassword extends React.Component {
  constructor(props) {
    super(props);
  }

  /* eslint-disable react/no-deprecated */
  componentWillReceiveProps(newProps) {
    const { actions } = this.props;

    if (newProps.changePasswordSuccess) {
      this.props.ok();

      actions.core_alert.showInfor(messages['Login.ChangePasswordSuccess']);

      setTimeout(() => {
        actions.auth_users.logout();
        actions.routing.navigateTo('/login');
      }, 1000);
    }
  }

  changePassword(formValues) {
    const { actions } = this.props;

    actions.auth_users.changePassword(
      formValues.currentPassword, formValues.newPassword
    );
  }

  render() {
    return (
      <ChangePassword changePassword={this.changePassword.bind(this)} />
    );
  }
}

DialogChangePassword.propTypes = {
  actions: PropTypes.any,
  ok: PropTypes.func
};

export default connect(state => ({
  error: state.core_errorHandler.error,
  changePasswordSuccess: state.auth_users.changePasswordSuccess
}))(DialogChangePassword);

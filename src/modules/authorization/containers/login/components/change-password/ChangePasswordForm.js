/**
 * React / Redux dependencies
 */
import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape, injectIntl } from 'react-intl';

import { isRequired, match, strongPassword } from 'common/utils/validation';
import './styles.scss';

/**
 * The assets such as css, images
 */
import './styles.scss';

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    const { intl } = this.props;
    // Init validation function
    this.isRequired = isRequired(intl.formatMessage({ id: 'Validation.RequiredField' }));
    this.matchConfirmPassword = match('newPassword', intl.formatMessage({ id: 'Validation.MatchPassword' }));
    this.isValidPassword = strongPassword(intl.formatMessage({ id: 'Validation.StrongPassword' }));
  }

  renderField({ input, type, options, meta: { touched, error } }) {
    return (
      <div className={`input-form${touched && error ? ' has-error' : ''}`}>
        <input
          {...input}
          {...options}
          type={type} />
        {touched && (error && <span className='text-danger-tooltip'>{error}</span>)}
      </div>
    );
  }

  renderFormInput() {
    const { invalid } = this.props;
    return (
      <div>
        <FormattedMessage id='Login.PlaceholderCurrentPassword'>
          {ph =>
            <Field
              name='currentPassword'
              options={{
                className: 'input-password current-password',
                id: 'currentPassword',
                placeholder: ph
              }}
              type='password'
              component={this.renderField}
              validate={[this.isRequired, this.isValidPassword]} />
          }
        </FormattedMessage>
        <FormattedMessage id='Login.PlaceholderNewPassword'>
          {ph =>
            <Field
              name='newPassword'
              options={{
                className: 'input-password new-password',
                id: 'newPassword',
                placeholder: ph
              }}
              type='password'
              component={this.renderField}
              validate={[this.isRequired, this.isValidPassword]} />
          }
        </FormattedMessage>
        <FormattedMessage id='Login.PlaceholderConfirmPassword'>
          {ph =>
            <Field
              name='confirmPassword'
              options={{
                className: 'input-password confirm-password',
                id: 'confirmPassword',
                placeholder: ph
              }}
              type='password'
              component={this.renderField}
              validate={[this.isRequired, this.matchConfirmPassword]} />
          }
        </FormattedMessage>

        <button className='col-xs-12 btn btn-primary btn-load btn-lg' disabled={invalid}><FormattedMessage id={'Login.ChangePasswordBtn'} /></button>
      </div>
    );
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div>
        <div className='row'>
          <div className='col-sm-10 col-sm-offset-1'>
            <form method='post' onSubmit={handleSubmit(this.props.changePassword)}>
              {this.renderFormInput()}
            </form>
          </div>
        </div>
      </div>
    );
  }
}

ChangePassword.propTypes = {
  actions: PropTypes.any,
  changePassword: PropTypes.func,
  handleSubmit: PropTypes.func,
  intl: intlShape.isRequired,
  invalid: PropTypes.bool
};

let changePassword = reduxForm({
  form: 'changePassword' // A unique identifier for this form
})(ChangePassword);

export default injectIntl(changePassword);

import React from 'react';
import { Route } from 'react-router';
import { CognitoService } from 'services';

import { rootActions } from 'common';

import Login from '../containers/login/Login';

export const makeRoutes = (getState) => {
  const requireNoAuth = (nextState, replace, callback) => {
    const { auth_users } = getState();
    const { currentUser } = auth_users;

    if (currentUser && CognitoService.getCurrentUser()) {
      replace({
        pathname: '/home',
        state: {}
      });
      return callback();
    }

    return CognitoService.refreshToken().then((result) => {
      let currentToken = localStorage.getItem('session_token');

      if (result !== currentToken) {
        localStorage.setItem('session_token', result);
      }

      // Re-authorize the route after refresh token
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
      return callback();
    }).catch(() => {
      callback();
      rootActions.auth_users.clearCurrentStudio();
    });
  };
  /**
   * Return route define
  */
  return (
    <Route key='app'>
      <Route path='login' onEnter={requireNoAuth} component={Login} />
    </Route>
  );
};


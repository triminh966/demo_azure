import * as _ from 'lodash';
import moment from 'moment';
import { createConstants, createReducer } from 'redux-module-builder';
import { Constants, AppConstants } from 'common';
import { CognitoService, UserAPI } from 'services';
import { errorHandlerConstants } from 'modules/core/config/redux/error-handler';
import { loadingIndicatorConstants } from 'modules/core/config/redux/loading-indicator';
import { messages } from 'localization';
import { rootActions } from 'common';
import { LocaleSessionConstant } from 'common/constants/constants';
import jwt_decode from 'jwt-decode';

export const types = createConstants('users')(
  'LOADING',
  'LOGIN',
  'LOGGED_IN_COGNITO',
  'HANDLE_ERROR',
  'LOGOUT',
  'CLEAR_ERRORS',
  'FORCE_CHANGE_PASSWORD',
  'FORCE_CHANGE_PASSWORD_SUCCESS',
  'CLEAR_CURRENT_STUDIO',
  'CLEAR_SUGGESTION',
  'UPDATING_INPUT',
  'FETCHED_SUGGESTION',
  'FETCHING_SUGGESTION',
  'FORGOT_PASSWORD_SUCCESS',
  'FORGOT_PASSWORD_CONFIRM_SUCCESS',
  'RESET_FORGOT_PASSWORD_DATA',
  'CHANGE_PASSWORD_SUCCESS',
  'SET_SELECTED_STUDIO',
  'CLEAR_SELECTED_STUDIO',
  'DESTROY_STUDIO'
);

export function loading(show = true) {
  return {
    type: show
      ? loadingIndicatorConstants.SHOW_LOADING
      : loadingIndicatorConstants.HIDE_LOADING
  };
}

export function handleError(err) {
  return {
    type: errorHandlerConstants.HANDLE_ERROR,
    payload: { error: err }
  };
}

export const actions = {
  loginCognito: (studio, password) => (dispatch, getState) => {
    dispatch(loading());

    const email = (studio || {}).StudioAccount || '';
    const locale = localStorage.getItem(LocaleSessionConstant);
    return CognitoService.login(email, password).then(
      result => {
        // Login success
        if (result && result.status === Constants.CognitoStatus.Success) {
          localStorage.setItem(
            'session_token',
            result.data.getIdToken().getJwtToken()
          );

          let tokenId = jwt_decode(result.data.idToken.jwtToken);
          const userRole = tokenId['cognito:groups'] || [];
          const allowAccess = userRole.filter(role => AppConstants.allowedGroups.includes(role));
          if (allowAccess.length > 0) {
            let saveData = _.cloneDeep(getState().auth_users);
            saveData.loginSuccess = false;
            localStorage.setItem('current_login', JSON.stringify(saveData));
            dispatch({
              type: types.LOGGED_IN_COGNITO,
              payload: {
                selectedStudio: studio,
                userdata: tokenId.email
              }
            });
          } else {
            dispatch(handleError(new Error(messages[moment.locale()]['Login.NotAllowed'])));
            localStorage.clear();
            rootActions.core_language.changeLocale(locale);
            return;
          }
        } else if (result && result.status === Constants.CognitoStatus.NewPasswordRequired) {
          // We need force change password at the first time login
          dispatch({ type: types.FORCE_CHANGE_PASSWORD, payload: email });
        }
      }, err => dispatch(handleError(new Error(messages[moment.locale()]['Login.Password.Error']))));
  },
  refreshToken: () => () => {
    return CognitoService.refreshToken().then(
      result => {
        localStorage.setItem('session_token', result);
        return null;
      },
      err => {
        throw err;
      }
    );
  },
  completeNewPassword: (username, password, newPassword) => dispatch => {
    dispatch(loading());

    return CognitoService.completeNewPassword(
      username,
      password,
      newPassword
    ).then(
      result => {
        // Force change password success
        if (
          result &&
          result.status === Constants.CognitoStatus.SetNewPasswordSuccess
        ) {
          // Just logout and redirect to login page
          dispatch({ type: types.FORCE_CHANGE_PASSWORD_SUCCESS });
        }
      },
      err => dispatch(handleError(err))
    );
  },
  changePassword: (oldPassword, newPassword) => dispatch => {
    dispatch(loading());

    return CognitoService.changePassword(oldPassword, newPassword).then(
      result => {
        // Change password success
        if (result) {
          dispatch({ type: types.CHANGE_PASSWORD_SUCCESS });
        }
      },
      err => {
        dispatch(loading(false));
        dispatch(handleError(err));
      }
    );
  },
  forgotPassword: username => dispatch => {
    dispatch(loading());

    return CognitoService.forgotPassword(username).then(
      result => {
        if (result) {
          dispatch({
            type: types.FORGOT_PASSWORD_SUCCESS
          });
        }
      },
      err => dispatch(handleError(err))
    );
  },
  forgotPasswordConfirm: (
    username,
    verificationCode,
    newPassword
  ) => dispatch => {
    dispatch(loading());

    return CognitoService.confirmPassword(
      username,
      verificationCode,
      newPassword
    ).then(
      result => {
        if (result) {
          dispatch({
            type: types.FORGOT_PASSWORD_CONFIRM_SUCCESS
          });
        }
      },
      err => dispatch(handleError(err))
    );
  },
  resetForgotPasswordData: () => dispatch => {
    dispatch({
      type: types.RESET_FORGOT_PASSWORD_DATA
    });
  },
  logout: () => dispatch => {
    const locale = localStorage.getItem(LocaleSessionConstant);
    localStorage.clear();
    dispatch({ type: types.LOGOUT });
    rootActions.core_language.changeLocale(locale);
    rootActions.core_screen.clearFirstScreenLoad();
  },
  fetchUsersSuggestion: value => dispatch => {
    dispatch({ type: types.FETCHING_SUGGESTION });
    if (!value || (value && !value.trim())) {
      return dispatch({
        type: types.FETCHED_SUGGESTION,
        payload: []
      });
    }
    return UserAPI.getStudioSuggestions(value, { isUndergroundReq: true })
      .then(res => {
        if (res) {
          dispatch({ type: types.FETCHED_SUGGESTION, payload: res.data });
        }
      })
      .catch(err => {
        if (err.statusCode && err.statusCode !== 403) {
          dispatch(handleError(err));
        }
      });
  },
  updateCurrentUser: newValue => dispatch => {
    dispatch({ type: types.UPDATING_INPUT, payload: newValue });
  },
  setSelectedStudio: value => dispatch => {
    dispatch({ type: types.SET_SELECTED_STUDIO, payload: value });
  },
  clearUserSuggestion: () => dispatch => {
    dispatch({ type: types.CLEAR_SUGGESTION });
  },
  clearCurrentStudio: () => dispatch => {
    dispatch({ type: types.CLEAR_CURRENT_STUDIO });
  },
  clearSelectedStudio: () => dispatch => {
    dispatch({ type: types.CLEAR_SELECTED_STUDIO });
  },
  destroyStudio: () => dispatch => {
    dispatch({ type: types.DESTROY_STUDIO });
  }
};

export const reducer = createReducer({
  [types.LOADING]: state => {
    return {
      ...state,
      loading: true,
      errors: undefined
    };
  },
  [types.LOGGED_IN_COGNITO]: (state, { payload }) => {
    return {
      ...state,
      loading: false,
      currentUser: payload.userdata,
      loginSuccess: true,
      currentStudio: payload.selectedStudio
    };
  },
  [types.FORCE_CHANGE_PASSWORD]: (state, { payload }) => {
    return {
      ...state,
      loading: false,
      errors: undefined,
      newPasswordRequired: true,
      email: payload
    };
  },
  [types.FORCE_CHANGE_PASSWORD_SUCCESS]: state => {
    return {
      ...state,
      loading: false,
      errors: undefined,
      forceChangePasswordSuccess: true
    };
  },
  [types.FORGOT_PASSWORD_SUCCESS]: state => {
    return {
      ...state,
      loading: false,
      errors: undefined,
      forgotPasswordSuccess: true
    };
  },
  [types.FORGOT_PASSWORD_CONFIRM_SUCCESS]: state => {
    return {
      ...state,
      loading: false,
      errors: undefined,
      forgotPasswordSuccess: false,
      forgotPasswordConfirmSuccess: true
    };
  },
  [types.RESET_FORGOT_PASSWORD_DATA]: state => {
    return {
      ...state,
      loading: false,
      errors: undefined,
      forgotPasswordSuccess: false,
      forgotPasswordConfirmSuccess: false
    };
  },
  [types.CHANGE_PASSWORD_SUCCESS]: state => {
    return {
      ...state,
      loading: false,
      errors: undefined,
      changePasswordSuccess: true
    };
  },
  [types.LOGOUT]: state => {
    return {
      ...state,
      currentUser: undefined,
      loginSuccess: false,
      selectedStudio: undefined,
      currentStudio: undefined,
      studio: '',
      studios: [],
      suggestions: []
    };
  },
  [types.CLEAR_CURRENT_STUDIO]: state => {
    return {
      ...state,
      currentStudio: undefined
    };
  },
  [types.CLEAR_SUGGESTION]: state => {
    return {
      ...state,
      suggestions: []
    };
  },
  [types.FETCHING_SUGGESTION]: state => {
    return {
      ...state,
      fetching: true,
      suggestions: [],
      errors: undefined
    };
  },
  [types.FETCHED_SUGGESTION]: (state, { payload }) => {
    return {
      ...state,
      studios: payload,
      suggestions: payload,
      errors: undefined,
      fetching: false
    };
  },
  [types.UPDATING_INPUT]: (state, { payload }) => {
    return {
      ...state,
      studio: payload
    };
  },
  [types.SET_SELECTED_STUDIO]: (state, { payload }) => {
    return {
      ...state,
      selectedStudio: payload
    };
  },
  [types.CLEAR_SELECTED_STUDIO]: state => {
    return {
      ...state,
      selectedStudio: undefined
    };
  },
  [types.DESTROY_STUDIO]: state => {
    return {
      ...state,
      changePasswordSuccess: false,
      fetching: false,
      forceChangePasswordSuccess: false,
      forgotPasswordSuccess: false,
      loading: false,
      loginSuccess: false,
      newPasswordRequired: false,
      studio: '',
      studios: [],
      suggestions: []
    };
  }
});

const loginSession = JSON.parse(localStorage.getItem('current_login'));

/*
 * The initial state for this part of the component tree
 */
export const initialState = loginSession
  ? loginSession
  : {
    loading: false,
    errors: undefined,
    loginSuccess: false,
    currentUser: undefined,
    currentStudio: undefined,
    newPasswordRequired: false,
    forceChangePasswordSuccess: false,
    changePasswordSuccess: false,
    forgotPasswordSuccess: false,
    studio: '',
    studios: [],
    suggestions: [],
    selectedStudio: undefined,
    fetching: false,
    email: undefined
  };

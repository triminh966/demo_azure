import axios from 'axios';
import { CognitoService } from 'services';
import * as _ from 'lodash';
import { default as AppConfig } from 'common/config/app-config';
import { LocaleSessionConstant } from 'common/constants/constants';
let actions = {};

export const setApiActions = function(a) {
  actions = a;
};

class ApiService {
  constructor() {
    let root = AppConfig.getOtumFranchiseApiRootUrl();
    this.session_token = undefined;

    this.getRootLink = function (options) {
      // Default value isAuthorized config will be true
      if (options.isAuthorized === undefined && options.isBearerAuthorization === undefined) {
        return root;
      }
      if (options.isAuthorized) {
        return AppConfig.getAuthorizeEndpoint();
      } else if (options.isBearerAuthorization) {
        return AppConfig.getEdenApiRootUrl();
      }
      return AppConfig.getUnauthorizeEndpoint();
    };

    this.getFullAPILink = function(link, options) {
      options = options || {};
      if (root) {
        return this.getRootLink(options) + link;
      }
      return link;
    };


    this.clearStorage = function() {
      const locale = localStorage.getItem(LocaleSessionConstant);
      localStorage.clear();
      actions.core_language.changeLocale(locale);
    };


    ['handleSuccess', 'handleError'].forEach(method => {
      if (this[method]) {
        this[method] = this[method].bind(this);
      }
    });

    let service = axios.create();

    service.interceptors.response.use(this.handleSuccess, this.handleError);
    this.service = service;
  }

  async getRequestHeaders(options) {
    let apiService = this;
    let isUndergroundReq =
      options && options.isUndergroundReq ? options.isUndergroundReq : false;

    let currentUser = await CognitoService.getCurrentUser();
    if (!currentUser) {
      await CognitoService.refreshToken()
        .then(result => {
          localStorage.setItem('session_token', result);

          return null;
        })
        .catch(() => this.clearStorage());
    }
    let timezoneoffset = new Date().getTimezoneOffset();
    apiService.session_token = localStorage.getItem('session_token');

    if (!apiService.session_token) {
      this.clearStorage();
      actions.routing.navigateTo('/login');
      return undefined;
    }
    if (!isUndergroundReq) {
      actions.core_loading.show();
    }
    if (options && options.isAuthorized) {
      return {
        'Authorization-Cognito': apiService.session_token,
        TimezoneOffset: timezoneoffset
      };
    } else if (options && options.isBearerAuthorization) {
      return {
        Authorization: `Bearer ${apiService.session_token}`
      };
    }
    return {
      Authorization: apiService.session_token
    };
  }

  handleSuccess(response) {
    actions.core_loading.hide();
    return response;
  }

  handleError(error) {
    actions.core_loading.hide();
    if (error && error.response && error.response.status === 401) {
      actions.auth_users.logout();
      actions.routing.navigateTo('/login');
      localStorage.clear();
    }
    return Promise.reject(error);
  }

  static ProcessResponse(response) {
    let messages = [];

    switch (response.status) {
      case 400:
        if (typeof response.data === 'string') {
          messages.push(response.data);
        } else if (response.data['0']) {
          for (let key in response.data['0']) {
            messages.push(response.data['0'][key]);
          }
        } else if (response.data.ModelState) {
          for (let key in response.data.ModelState) {
            response.data.ModelState[key].forEach(function(message) {
              messages.push(message);
            });
          }
        }
        break;
      case 401:
        actions.auth_users.logout();
        actions.routing.navigateTo('/login');
        localStorage.clear();
        break;
      case 500:
        messages.push('Server Error.');
        break;
      default:
        messages.push('An error has occured.');
    }

    return messages;
  }

  async get(path, options) {
    let requestHeaders = await this.getRequestHeaders(options);
    path = this.getFullAPILink(path, options);
    return this.service.get(path, {
      headers: requestHeaders
    });
  }

  async post(path, payload, options) {
    let requestHeaders = await this.getRequestHeaders(options);
    path = this.getFullAPILink(path, options);
    return this.service.request({
      method: 'POST',
      url: path,
      responseType: 'json',
      data: payload,
      headers: requestHeaders
    });
  }

  async put(path, payload, options) {
    let requestHeaders = await this.getRequestHeaders(options);
    path = this.getFullAPILink(path, options);
    return this.service.request({
      method: 'PUT',
      url: path,
      responseType: 'json',
      data: payload,
      headers: requestHeaders
    });
  }

  async delete(path, payload, options) {
    let requestHeaders = await this.getRequestHeaders(options);
    path = this.getFullAPILink(path, options);
    return this.service.request({
      method: 'DELETE',
      url: path,
      responseType: 'json',
      data: payload,
      headers: requestHeaders
    });
  }
}

export default new ApiService();

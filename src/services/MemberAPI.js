import {
  Helpers
} from 'common';
import BaseAPI from './BaseAPI';
// import { ApiFake } from 'services';

class MemberAPI extends BaseAPI {
  constructor() {
    super();

    this.baseUrl = '/members/';
  }

  /**
   * Get member profile from OTF system by email
   * @param {string} email
   */
  getMemberProfileByEmail(email) {
    this.requestUrl = `${
      this.baseUrl
    }search/email/${email}?include=memberIntakeArgeement,memberAddresses,memberEmergencyContact`;
    return this.get({});
  }
  getMembershipTypes(memberUUId) {
    this.requestUrl = `/v3/agenda/class-milestones/member/${memberUUId}`;
    return this.get({
      isAuthorized: true,
      isUndergroundReq: true
    });
  }

  /**
   * Search member information
   * @param {string} keyword Specify the search keyword
   */
  searchMember(searchPayload, isSuggestion) {
    this.requestUrl = isSuggestion ? `/members${Helpers.buildUrlQuery(searchPayload)}` :
      `/v3/studios/member/suggestion/${searchPayload.SearchQuery}${searchPayload.StudioId ? `?studioId=${searchPayload.StudioId}` : ''}`;

    return this.get({
      isAuthorized: !isSuggestion,
      isBearerAuthorization: isSuggestion,
      isUndergroundReq: true
    });
  }

  /**
   * Get member information
   * @param {string} memberId Specify the member id
   */
  getMember(memberId) {
    this.requestUrl = `/v3/member/${memberId}`;

    return this.get({
      isAuthorized: true
    });
  }
  saveClientIntake(memberUUId, ci) {
    this.requestUrl = `${this.baseUrl}${memberUUId}/agreements/client-intake`;
    return this.post(ci);
  }
  getMemberGoals(memberId) {
    this.requestUrl = `${this.baseUrl}${memberId}/goals`;
    return this.get({
      isBearerAuthorization: true
    });
  }
  submitMemberGoals(payload) {
    this.requestUrl = `${this.baseUrl}${payload.memberId}/goals`;
    return this.post(payload.data, {
      isBearerAuthorization: true
    });
  }
}

export default new MemberAPI();
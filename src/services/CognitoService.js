import AWS from 'aws-sdk';
import apigClientFactory from 'aws-api-gateway-client';
import { AuthenticationDetails, CognitoUserPool, CognitoUser } from 'amazon-cognito-identity-js';

import { Constants } from 'common';
import { default as AppConfig } from 'common/config/app-config';

class CognitoService {
  /**
   * Get cognito user pool
   * @returns {CognitoUserPool} Congito user pool object
   */
  getCognitoUserPool() {
    return new CognitoUserPool({
      UserPoolId: AppConfig.getCognitoUserPoolId(),
      ClientId: AppConfig.getCognitoAppClientId()
    });
  }

  /**
   * Get current user from local storage
   * @returns {CognitoUser} Congito user
   */
  getCurrentUser() {
    const userPool = this.getCognitoUserPool();
    const cognitoUser = userPool.getCurrentUser();

    if (cognitoUser !== null) {
      return cognitoUser.getSession((err) => {
        if (err) {
          return null;
        }
        return cognitoUser;
      });
    }

    return cognitoUser;
  }

  /**
   * Refresh access token
   * @returns {CognitoUser} Congito user
   */
  refreshToken() {
    return new Promise((resolve, reject) => {
      const cognitoUser = this.getCognitoUserPool().getCurrentUser();
      if (cognitoUser !== null) {
        cognitoUser.getSession((err, session) => {
          if (err) {
            reject(err);

            return;
          }
          resolve(session.getIdToken().getJwtToken());
        });
      } else {
        reject('Failed to retrieve user from localStorage');
      }
    });
  }

  /**
   * Get current user attributes from local storage
   * @returns {CognitoUser} Congito user
   */
  getCurrentUserAttributes() {
    return new Promise((resolve, reject) => {
      this.getCurrentUser().getUserAttributes((err, result) => {
        if (err) {
          reject(err);

          return;
        }

        let userProfileObject = {};

        for (let item of result) {
          if (item.getName().indexOf('custom:') >= 0) {
            let name = item.getName().slice(7, item.getName().length);
            userProfileObject[name] = item.getValue();
          } else {
            userProfileObject[item.getName()] = item.getValue();
          }
        }

        resolve(userProfileObject);
      });
    });
  }

  /**
   * Login using Cognito
   * @param  {String} username The cognito username (user email)
   * @param  {String} password The cognito login password
   * @returns {Promise<any>} The token generate response
   */
  login(username, password) {
    const userPool = this.getCognitoUserPool();
    const authenticationData = {
      Username: username,
      Password: password
    };
    let user;

    try {
      user = new CognitoUser({ Username: username, Pool: userPool });
    } catch (error) {
      return Promise.reject(error);
    }

    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) => (
      user.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          resolve({
            status: Constants.CognitoStatus.Success,
            data: result
          });
        },
        onFailure: (err) => reject(err),
        newPasswordRequired: (userAttributes) => {
          resolve({
            status: Constants.CognitoStatus.NewPasswordRequired,
            userAttributes
          });
        }
      })
    ));
  }

  /**
   * Set new password at the first time login
   * @param  {String} username The cognito username (user email)
   * @param  {String} password The cognito login password
   * @param  {String} newPassword New password input by user
   * @returns {Promise<any>} The token generate response
   */
  completeNewPassword(username, password, newPassword) {
    const userPool = this.getCognitoUserPool();
    const authenticationData = {
      Username: username,
      Password: password
    };

    const user = new CognitoUser({ Username: username, Pool: userPool });
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) => (
      user.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          resolve({
            status: Constants.CognitoStatus.Success,
            token: result.getIdToken().getJwtToken()
          });
        },
        onFailure: (err) => reject(err),
        newPasswordRequired: (userAttributes) => {
          delete userAttributes.email_verified;
          delete userAttributes.phone_number_verified;

          user.completeNewPasswordChallenge(newPassword, userAttributes, {
            onSuccess: (result) => {
              resolve({
                status: Constants.CognitoStatus.SetNewPasswordSuccess,
                data: result
              });
            },
            onFailure: (err) => reject(err)
          });
        }
      })
    ));
  }

  /**
   * Changes the current password of an authenticated user
   * @param  {string} oldPassword The current password
   * @param  {string} newPassword The new password
   * @returns {Promise<any>} Promise with thenable
   */
  changePassword(oldPassword, newPassword) {
    return new Promise((resolve, reject) => {
      this.getCurrentUser().changePassword(oldPassword, newPassword, (err, result) => {
        if (err) {
          return reject(err);
        }

        return resolve(result);
      });
    });
  }

  /**
   * Forgotten password for an unauthenticated user.
   * @param  {string} username The username
   * @returns {Promise<any>} Promise with thenable
   */
  forgotPassword(username) {
    const cognitoUser = new CognitoUser({ Username: username, Pool: this.getCognitoUserPool() });

    return new Promise((resolve, reject) => {
      cognitoUser.forgotPassword({
        onSuccess: (result) => {
          return resolve({
            status: Constants.CognitoStatus.VerifyCodeSendSuccess,
            data: result
          });
        },
        onFailure: (err) => {
          reject(err);
        }
      });
    });
  }

  /**
   * Forgotten password for an unauthenticated user.
   * @param  {string} username            The username
   * @param  {string} verificationCode    The verification code
   * @param  {string} newPassword         The new password input by user
   * @returns {Promise<any>} Promise with thenable
   */
  confirmPassword(username, verificationCode, newPassword) {
    const cognitoUser = new CognitoUser({ Username: username, Pool: this.getCognitoUserPool() });

    return new Promise((resolve, reject) => {
      cognitoUser.confirmPassword(verificationCode, newPassword, {
        onSuccess: () => {
          return resolve({
            status: Constants.CognitoStatus.ForgotPasswordSuccess
          });
        },
        onFailure: (err) => {
          reject(err);
        }
      });
    });
  }

  /**
   * Changes the current password of an authenticated user
   * @param  {string} username    The current username
   * @param  {string} oldPassword The current password
   * @param  {string} newPassword The new password
   * @returns {Promise<any>} Promise with thenable
   */
  changeAndResetPassword(username, oldPassword, newPassword) {
    const userPool = this.getCognitoUserPool();
    const authenticationData = {
      Username: username,
      Password: oldPassword
    };

    const user = new CognitoUser({ Username: username, Pool: userPool });
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) => (
      user.authenticateUser(authenticationDetails, {
        onSuccess: () => {
          return user.changePassword(oldPassword, newPassword, (err, changePasswordResult) => {
            if (err) {
              return reject(err);
            }

            return resolve(changePasswordResult);
          });
        },
        onFailure: (err) => reject(err),
        newPasswordRequired: (userAttributes) => {
          delete userAttributes.email_verified;
          delete userAttributes.phone_number_verified;

          return user.completeNewPasswordChallenge(newPassword, userAttributes, {
            onSuccess: (forceChangePasswordResult) => {
              resolve(forceChangePasswordResult);
            },
            onFailure: (err) => reject(err)
          });
        }
      })
    ));
  }

  getAwsCredentials() {
    if (AWS.config.credentials && AWS.config.credentials.expireTime
      && (Date.now() < AWS.config.credentials.expireTime.getTime() - 60000)) {
      return AWS.config.credentials.getPromise();
    }

    AWS.config.update({ region: AppConfig.getAwsRegion() });

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: AppConfig.getCognitoIdentityPoolId()
    });

    AWS.config.credentials.refresh((error) => {
      if (error) {
        return Promise.reject(error);
      }

      return Promise.resolve();
    });

    return AWS.config.credentials.getPromise();
  }

  invokeLambdaFunction({ path, method = 'GET', headers = {}, params = {}, queryParams = {}, body }) {
    let options = {};

    return this.getAwsCredentials().then(() => {
      options = {
        accessKey: AWS.config.credentials.accessKeyId,
        secretKey: AWS.config.credentials.secretAccessKey,
        sessionToken: AWS.config.credentials.sessionToken,
        region: AppConfig.getAwsRegion(),
        invokeUrl: AppConfig.getOtumFranchiseApiRootUrl()
      };

      const apigClient = apigClientFactory.newClient(options);

      headers = apigClient.headers;

      const additionalParams = {
        headers,
        queryParams
      };

      return apigClient.invokeApi(params, path, method, additionalParams, body).then((results) => {
        return Promise.resolve(results.data);
      });
    });
  }

  /**
   * Signs the current user out from the application
   */
  logout() {
    if (this.getCurrentUser()) {
      this.getCurrentUser().signOut();
    }
  }
}

export default new CognitoService();

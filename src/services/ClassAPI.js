import moment from 'moment';
import BaseAPI from './BaseAPI';
import {Helpers} from 'common';

class ClassAPI extends BaseAPI {
  constructor() {
    super();

    this.baseUrl = '/studios/';
  }

  /**
   * Search function get list of target entities
   * @param {string} url Specify the url
   */
  getAvailableClass(studioId, date) {
    let endDate = new Date(date);
    this.requestUrl = `/v3/studios/${studioId}/classes?date=${moment(endDate).format('MM/DD/YYYY')}`;

    return this.get({isAuthorized: true});
  }


  /**
   * Get list of class roster
   * @param {object} classPayload The selected class info
   */
  getClassRoster(classPayload) {
    let payload = {
      MboStudioId: classPayload.mboStudioId,
      MboClassId: classPayload.mboClassId,
      PageSize: classPayload.pageSize,
      CurrentPageIndex: classPayload.currentPageIndex
    };
    this.requestUrl = `/schedules/${classPayload.classId}/rosters${Helpers.buildUrlQuery(payload)}`;
    return this.get({ isBearerAuthorization: true });
  }

  /**
   *
   * @param {String} classUUId The class UUId to be checked
   * @param {Number} mboMemberId The MBO Member Id
   * @param {Number} mboVivistId The MBO Vivist Id to be checkin
   * @param {String}  email       The member email
   */
  checkin(classUUId, mboMemberId, mboVivistId, email) {
    this.requestUrl = `/classes/${classUUId}/checkin/${mboVivistId}`;

    return this.post({
      mboMemberId,
      email
    });
  }
  multiCheckIn(classUUId, members) {
    this.requestUrl = `/classes/${classUUId}/checkin`;
    return this.post(members);
  }
  /**
   * @param {Number} mboMemberId The MBO Member Id
   * @param {String} classUUId The class UUId to be checked
   * @param {Number} mboVivistId The MBO Vivist Id to be checkin
   * @param {String}  email       The member email
   */
  undoCheckin(classUUId, mboMemberId, mboVivistId, email) {
    this.requestUrl = `/classes/${classUUId}/checkin/cancel/${mboVivistId}`;
    return this.post({
      mboMemberId,
      email
    });
  }

  getLatestVisitForMember(studioUUId, mboMemberId) {
    this.requestUrl = `/studios/${studioUUId}/last-checkedin?mboMemberId=${mboMemberId}`;
    return this.get({});
  }

  addMember(classId, payload) {
    this.requestUrl = `/schedules/${classId}/addMemberToClass`;
    return this.post(payload, { isBearerAuthorization: true });
  }
}

export default new ClassAPI();

import BaseAPI from './BaseAPI';

class UserAPI extends BaseAPI {
  constructor() {
    super();
    this.baseUrl = '/franchises/';
  }

  /**
   * Login function
   * @param {string} url Specify the url
   */
  getProfile(franchiseId, options = {}) {
    this.requestUrl = this.baseUrl + franchiseId;

    return this.get(options);
  }

  getStudioSuggestions(value, options) {
    let defaultOptions = { isAuthorized: false };
    value = value.trim();
    this.requestUrl = `/v3/studios/suggestion/${encodeURIComponent(value)}`;
    return this.get(Object.assign(defaultOptions, options));
  }
}

export default new UserAPI();

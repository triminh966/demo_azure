import BaseAPI from './BaseAPI';

class TableAPI extends BaseAPI {
  constructor() {
    super();

    this.baseUrl = '/v2';
  }

  /**
   * Search function get list of target entities
   * @param {string} url Specify the url
   */
  search(url) {
    this.requestUrl = `${this.baseUrl}/${url}`;

    return this.get({ isUndergroundReq: true });
  }
}

export default new TableAPI();

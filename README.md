# OT Class Check In Application

# Installing Dependencies #

Make sure you have Node version >= 6.9.1 and NPM >=3 installed. Then run the folowing commands to install all needed dependencies
```sh
# Install npm dependencies
npm install
```

## CLI

### Running locally
```sh
# Start webpack dev server (you will need to make sure the OTUM LOCAL FRANCHISE API also running at http://localhost:8888/franchise)
$ npm start

# You also can start webpack to connec to OTUM DEV FRANCHISE API on AWS with below command
$ npm run start:dev
```

### Deploy

```bash
    $ npm run -s deploy # for prod stage
    $ npm run -s deploy:dev # for dev stage
    $ npm run -s deploy:sit # for sit stage
    $ npm run -s deploy:uat # for uat stage
```

Then you can access to the app deployed in AWS using the following links below:

* DEV - [https://occi.dev.orangetheoryfitness.net/](https://occi.sit.orangetheoryfitness.net/)
* SIT - [https://occi.sit.orangetheoryfitness.net/](https://occi.sit.orangetheoryfitness.net/)
* UAT - [https://occi.uat.orangetheory.co/](https://occi.uat.orangetheory.co/)
* PROD - [https://occi.orangetheory.co/](https://occi.orangetheory.co/)

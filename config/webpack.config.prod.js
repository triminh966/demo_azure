const webpack = require('webpack');
const optimize = webpack.optimize;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const helpers = require('./helpers');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

// Common config
let config = require('./webpack.config.js');

delete config.devtool;
delete config.devServer;
config.output.path = helpers.root('dist');
// Resolve production plugin
Array.prototype.push.apply(config.plugins, [
  // new BundleAnalyzerPlugin(),
  new optimize.UglifyJsPlugin({
    minimize: true,
    compress: {
      drop_console: false,
      warnings: false,
      conditionals: true,
      unused: true,
      comparisons: true,
      sequences: true,
      dead_code: true,
      evaluate: true,
      if_return: true,
      join_vars: true,
      negate_iife: false
    },
    mangle: {
      except: [
        'exports',
        'require'
      ]
    }
  }),
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  new webpack.NoEmitOnErrorsPlugin(),
  new CopyWebpackPlugin(
    [{
      from: 'public/assets',
      to: 'assets'
    }], {
      // Doesn't copy any files with a txt extension
      ignore: ['*.txt'],
      copyUnmodified: true
    }),
  new optimize.CommonsChunkPlugin({
    name: ['bundle'],
    minChunks: function (module) {
      return helpers.isExternal(module);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['bundle.common.1'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['jquery', 'babel-polyfill',
        'eonasdan-bootstrap-datetimepicker'];
      return helpers.checkChunk(module, targets)
                    || helpers.checkChunkByKeywords(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['bundle.common.2'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['d3', 'axios'];
      return helpers.checkChunk(module, targets)
                    || helpers.checkChunkByKeywords(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['react.bundle.1'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['react', 'react-intl', 'react-overlays', 'react-s-alert'];
      return helpers.checkChunk(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['react.bundle.2'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['react-dom', 'react-router-redux', 'react-router'];
      return helpers.checkChunk(module, targets) || helpers.checkChunkByKeywords(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['react.bootstrap.bundle'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['bootstrap', 'react-bootstrap'];
      return helpers.checkChunk(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['redux.bundle'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['redux', 'redux-module-builder', 'redux-thunk', 'redux-logger',
        'redux-debounce', 'react-redux', 'redux-form'];
      return helpers.checkChunk(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['aws.sdk'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['aws-sdk'];
      return helpers.checkChunk(module, targets)
                    || helpers.checkChunkByKeywords(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['aws.cognito'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['amazon-cognito-identity-js'];
      return helpers.checkChunk(module, targets)
                    || helpers.checkChunkByKeywords(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['moment'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['moment'];
      return helpers.checkChunkByKeywords(module, targets);
    }
  }),
  new optimize.CommonsChunkPlugin({
    name: ['lodash'],
    chunks: ['bundle'],
    minChunks: function (module) {
      let targets = ['lodash'];
      return helpers.checkChunkByKeywords(module, targets);
    }
  })
]);

module.exports = config;

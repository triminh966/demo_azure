/*!
 * imports.
 */
let env = process.env.APP_ENV;
let config = require(`./src/common/config/env/${env}.json`);

function load() {
  try {
    //check null or undefined App Name
    if (config.newrelicAppName && env !== 'local') {
      process.env.NEW_RELIC_APP_NAME = config.newrelicAppName;
      console.log(process.env.NEW_RELIC_APP_NAME);
      return require('newrelic');
    }
  } catch (e) {
    process.exit(1);
  }
}
/*!
 * exports.
 */ 
module.exports = load;
